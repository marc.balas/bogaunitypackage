using UnityEngine;

namespace BogakitUnity
{
    [RequireComponent(typeof(Renderer))]
    public class ApplyInstanceTextureShaderGraph : MonoBehaviour
    {
        public Texture2D instanceTexture; // Assign in Inspector

        void Start()
        {
            MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
            Renderer renderer = GetComponent<Renderer>();

            // Fetch the current values of the property block attached to this renderer
            renderer.GetPropertyBlock(propBlock);

            // Set the texture on the property block. Use the reference name set in Shader Graph
            propBlock.SetTexture("_Emission", instanceTexture);

            // Apply the updated property block to the renderer
            renderer.SetPropertyBlock(propBlock);
        }
    }
}