using System;
using System.Collections;

namespace BogakitUnity.Managers
{
    internal class AnimationClip 
    {
        public readonly float timeInQueue;
        public readonly string description;
        public readonly bool HasJoined;

        public IEnumerator Animation { get; }
        public float Duration => timeInQueue;
        public Action onComplete { get; }
        public Func<bool> YieldCondition { get; set; } = ()=> true;

        public AnimationClip(IEnumerator animation, float duration, string description, Action onComplete, bool joined = false)
        {
            Animation = animation;
            timeInQueue = duration;
            this.description = description;
            this.onComplete = onComplete;
            HasJoined = joined;
        }

        public override string ToString()
        {
            return description;
        }
    }
}