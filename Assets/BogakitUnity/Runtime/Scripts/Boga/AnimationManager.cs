﻿using System;
using System.Collections;
using System.Collections.Generic;
using Boga.Solver;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

namespace BogakitUnity.Managers
{
    #region Yield methods

    public class WaitForAnimationQueueIsOver : CustomYieldInstruction
    {
        public override bool keepWaiting => !AnimationManager.Instance.AnimationQueueIsOver;
    }

    #endregion


    public class AnimationManager : MonoBehaviour
    {
        #region FIELDS

        [SerializeField] [Range(.1f, 5f)] private float animationsSpeed = 1f;

        
        [Header("speed increase step ")]
        [Range(.1f, 5f)]  [SerializeField]  private float speedIncreaseEachStep = 1f;
        [Header("durations values when anim speed increase by 1 step ")]
        [SerializeField] 
        private  List<float> speedIncreaseStepThresholds;
        [Tooltip("how much animations overlap  > 0 => delay between anim, <0 => overlapping")] [Range(-.5f, .5f)] [SerializeField]
        private float transitionFactor;
        [SerializeField] [Required] private Image animPlaying;
        [SerializeField] private bool stepByStep;
        [SerializeField] private bool debug;
        [SerializeField] private KeyCode keyNextAnim;

        private bool _animationQueueRunning;
        private bool _paused;
        private readonly Queue<AnimationClip> _animationQueue = new Queue<AnimationClip>();

        [Header("debug")]        
        [SerializeField] private string currentAnimDescription;
        
        #endregion

        #region PROPERTIES

        public static AnimationManager Instance { get; private set; }
        public bool AnimationQueueIsOver => !_animationQueueRunning && _animationQueue.Count == 0;
        public string CurrentAnimDescription => currentAnimDescription;

        
        public static float GetTotalDuration()
        {
            float duration = 0;
            foreach (var clip in Instance._animationQueue)
            {
                if (duration <= 0.01f || !clip.HasJoined)
                {
                    duration += clip.Duration;
                }
            }

            return duration;
        }

        #endregion

        #region EVENTS/ACTIONS

        private static Action<AnimationClip, string> OnJoinClip;
        private static Action<AnimationClip, string> OnAppendClip;
        private static SolverNode lastAnimationNode = null;

        public static event Action<string, float> OnAnimationClipStarted;
        public static event Action OnAnimationQueueCompleted;
        public static event Action OnAnimationQueueStarted;

        [SerializeField] private bool nextStep;

        #endregion
     

        private void Update()
        {
            if (!_animationQueueRunning && _animationQueue.Count > 0)
                StartCoroutine(ProcessAnimationQueueCoroutine());

            animPlaying.enabled = _animationQueueRunning;
        }

        private void PrintAll()
        {
            if(!debug)
                return;
            string des = "";
            foreach (var clip in _animationQueue)
            {
                if (clip.HasJoined)
                    des += $"├   {clip}\n";
                else
                    des += $"├─> {clip.timeInQueue} {clip}\n";
            }

            Debug.Log(des);
        }

        private static IEnumerator FuncToIEnumerator(Action func)
        {
            func?.Invoke();
            yield return null;
        }

        #region LOGIC

        private IEnumerator ProcessAnimationQueueCoroutine()
        {
            Time.timeScale = animationsSpeed;
            //animationsQueueSize = 0;
            _animationQueueRunning = true;

            OnAnimationQueueStarted?.Invoke();
            while (_animationQueue.Count > 0)
            {
                if (debug) PrintAll();
                float waitingTime = 0;
                string animName = "NA";
                Queue<Action> onCompleteActions = new Queue<Action>();
                int nbAnimStacked = 0;
                do
                {
                    nbAnimStacked++;
                    //animationsQueueSize++;
                    var animationToPlay = _animationQueue.Dequeue();
                    if (nbAnimStacked == 1) animName = animationToPlay.description; //get 1st name
                    if (animationToPlay.onComplete != null)
                        onCompleteActions.Enqueue(animationToPlay.onComplete);
                    if (!animationToPlay.HasJoined) waitingTime = animationToPlay.timeInQueue;
                    OnAnimationClipStarted?.Invoke(animationToPlay.description, animationToPlay.timeInQueue);
                    
                    StartCoroutine(animationToPlay.Animation);
                    yield return new WaitUntil( animationToPlay.YieldCondition);
                } while (_animationQueue.Count > 0 && _animationQueue.Peek().HasJoined);
                
                currentAnimDescription = $"{animName} ({nbAnimStacked}), {waitingTime}s";

                if (stepByStep)
                {
                    yield return new WaitUntil(() => Input.GetKeyDown(keyNextAnim) || nextStep);
                }

                yield return new WaitForSeconds(waitingTime * (1 + transitionFactor));

                nextStep = false;

                UpdateAnimationSpeed();
                

                //process on complete actions queued earlier
                while (onCompleteActions.Count > 0)
                {
                    var onComplete = onCompleteActions.Dequeue();
                    onComplete?.Invoke();
                }

                currentAnimDescription = "OVER";
            }

            _animationQueueRunning = false;
            OnAnimationQueueCompleted?.Invoke();
            if (debug) Debug.Log("<color=green>Animation queue complete</color>");
        }

        private void UpdateAnimationSpeed()
        {
            float speed = 1;
            foreach (var threshold in speedIncreaseStepThresholds)
            {
                if (GetTotalDuration()> threshold)
                {
                    speed += speedIncreaseEachStep;

                }
            }

            animationsSpeed = speed;
        }

        #endregion

        #region API

        public static void AppendAnimation(string description, float timeInQueue, Action func = null, Action onComplete = null)
        {
            AppendAnimation(description, timeInQueue, FuncToIEnumerator(func), onComplete);
        }
        
        public static void AppendYieldAnimation(string description, Func<bool> yieldCondition , Action func = null, Action onComplete = null)
        {
            //wait .1f seconds after yield
            AppendAnimation(description, 0.1f, FuncToIEnumerator(func), onComplete,yieldCondition);
        }
        //will append (same effect node) or join animation (≠ effect node)   
        public static void AddAnimation(string description, float timeInQueue, Action func = null, Action onComplete = null)
        {
            if (Instance == null)
                throw new ArgumentException("missing animation manager");
            //use last added node
            if (lastAnimationNode != EventSolver.Instance.currentNode || lastAnimationNode == null)
            {
                if(Instance.debug)Debug.Log($"Append --- anim '{description}' : node {lastAnimationNode}");
                lastAnimationNode = EventSolver.Instance.currentNode;
                AppendAnimation(description +  (lastAnimationNode != null ? lastAnimationNode.name: "none"), timeInQueue, func,onComplete);
            }
            else
            {
               if(Instance.debug)Debug.Log($"Join --- anim '{description}' : node {lastAnimationNode}");
                JoinAnimation(description + lastAnimationNode.name, func,onComplete);
            }
        }


        private static void AppendAnimation(string description, float timeInQueue, IEnumerator animationRoutine, Action onComplete = null,Func<bool> yieldCondition =null)
        {
            AnimationClip anim = new AnimationClip(animationRoutine, timeInQueue, description, onComplete);
            OnAppendClip?.Invoke(anim, description);
            if (yieldCondition != null) anim.YieldCondition = yieldCondition;
            if (Instance == null)
                throw new ArgumentException("missing animation manager");
            Instance._animationQueue.Enqueue(anim);
        }

        public static void JoinAnimation(string description, Action func,Action onComplete = null)
        {
            JoinAnimation(description, FuncToIEnumerator(func),onComplete);
        }

        // public static void JoinAnimation(Action func,Action onComplete = null)
        // {
        //     JoinAnimation("NA", FuncToIEnumerator(func),onComplete);
        // }

        private static void JoinAnimation(string description, IEnumerator animationRoutine, Action onComplete = null)
        {
            AnimationClip anim = new AnimationClip(animationRoutine, 0.33f, description, onComplete, true);
            OnJoinClip?.Invoke(Instance._animationQueue.Peek(), description);
            if (Instance == null)
                throw new ArgumentException("missing animation manager");
            Instance._animationQueue.Enqueue(anim);
        }

        #endregion

        #region LIFECYCLE

        protected void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
                return;
            }

            Instance = this;
        }


        [Button("play/pause")]
        public void TogglePause()
        {
            if (_paused)
                Resume();
            else
                Pause();
        }

        [Button("Log anim tree")]
        public void LogMe()
        {
            PrintAll();
        }

        private void Pause()
        {
            Debug.Log(" animation queue:paused");
            _paused = true;
            Time.timeScale = 0;
        }

        [Button("next")]
        public void Next()
        {
            stepByStep = true; //force step by step
            nextStep = true;
        }
        
        public void Clear()
        {
            _animationQueue.Clear();
            
        }

        private void Resume()
        {
            Time.timeScale = 1;
            _paused = false;
        }

        #endregion

        public static void Wait(float pauseDuration)
        {
            AppendAnimation("pause animation", pauseDuration);
        }

     
    }
}