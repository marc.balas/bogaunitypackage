﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using Boga.Kit;
using Boga.Solver;
using BogakitUnity.Extensions;
using BogakitUnity.Selections;
using BogakitUnity.UI.Popups;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using Phase = Boga.Solver.Phase;

namespace BogakitUnity.Managers
{
    /// <summary>
    /// base class for any game.
    /// Provide
    /// - utils functions: show,
    /// - boga callbacks : event, sequence
    /// - interaction logic: drag drop validation, 
    /// </summary>
    public class BaseUIManager : DragDropManager
    {
        [Header("Base UI")] [SerializeField] protected GameObject messagePanel;
        [SerializeField] private GameObject messagePrefab;


        [SerializeField] protected List<string> startArguments;

        [SerializeField] private List<string> commands;
        [SerializeField] protected PopupManager popupManager;
        [SerializeField] [ReadOnly]
        protected List<string> validCommands;

        private float messageLifetime => Mathf.Clamp(10.0f / (messages.Count + 1), .5f, 3);
        private readonly Queue<GameObject> messages = new Queue<GameObject>();


        #region LIFECYCLE
        

        protected virtual void OnEnable()
        {
            //BogaLoader.GameReady += SetupUI;
            // RuntimeMilestone.MilestoneCompleted += OnMilestoneCompleted;
            // RuntimeMilestone.MilestoneProgressed += OnMilestoneProgressed;
            EventSolver.OnOpenSequence += OnOpenSequence;
            //EventSolver.OnSequenceStarted += OnSequenceStarted;
            EventSolver.OnSequenceProcessed += OnSequenceProcessed;
            EventSolver.OnEventRaised += OnRuntimeEventProcessed;
            EventSolver.OnPhaseStart += OnSolverPhaseStart;
            EventSolver.OnPhaseCompleted += OnSolverPhaseComplete;
            EventSolver.OnWaitingForParams += OnSolverWaitingForParams;
            AnimationManager.OnAnimationQueueCompleted += OnAnimationQueueCompleted;
            AnimationManager.OnAnimationQueueStarted += OnAnimationQueueStarted;
            DragDropAction.BeginDragDropEvent += BeginDrag;
            DragDropAction.DidDropOnEvent += DidDropOn;
            DragDropAction.EndDragDropOnEvent += EndDrag;
            Zone.OnElementAdded += OnElementAddedToZone;
            Selection.OnSelect += OnSelect;
            Selection.OnDeselect += OnDeSelect;
            Selection.OnFocusChanged += OnFocusChanged;
            // if(BogaGame.shared != null)BogaGame.shared.fsm.OnEnterPhase += OnEnterPhase;
            // if(BogaGame.shared != null)BogaGame.shared.fsm.OnExitPhase += OnExitPhase;

            if (messagePanel) messagePanel?.transform.Clear();
        }

        // protected virtual void OnExitPhase(Boga.Core.Phase obj)
        // {
        //    
        // }
        //
        // protected virtual void OnEnterPhase(Boga.Core.Phase phase)
        // {
        //   
        // }

        protected virtual void OnDisable()
        {
            // RuntimeMilestone.MilestoneCompleted -= OnMilestoneCompleted;
            // RuntimeMilestone.MilestoneProgressed -= OnMilestoneProgressed;
            EventSolver.OnOpenSequence -= OnOpenSequence;
            EventSolver.OnSequenceProcessed -= OnSequenceProcessed;
            EventSolver.OnEventRaised -= OnRuntimeEventProcessed;
            EventSolver.OnPhaseCompleted -= OnSolverPhaseComplete;
            DragDropAction.DidDropOnEvent -= DidDropOn;
            DragDropAction.BeginDragDropEvent -= BeginDrag;
            EventSolver.OnWaitingForParams -= OnSolverWaitingForParams;
            if (BogaGame.shared == null) return;
                
            
            // BogaGame.shared.fsm.OnExitPhase -= OnExitPhase;
            // BogaGame.shared.fsm.OnEnterPhase -= OnEnterPhase;
        }

        #endregion

        #region Event sequence

        protected virtual void OnSolverWaitingForParams(string paramName, List<int> validValues)
        {
            QueueMessage($"Waiting for user choice: {paramName}", Color.green);
        }

        protected virtual void OnAnimationQueueStarted()
        {
            Debug.Log("animation queue started");
        }

        #endregion

        protected virtual void OnAnimationQueueCompleted()
        {
            Debug.Log("animation queue completed");
        }

        private void Awake()
        {
            popupManager = FindObjectOfType<PopupManager>();
        }

        protected virtual void Start()
        {
            Assert.IsNotNull(BogaGame.shared);
            SetupUI();
            BogaGame.shared?.Start(args: startArguments.ToArray());

            foreach (var command in commands)
            {
                BogaGame.shared?.ExecuteCommandString(command);
            }
        }

        protected virtual BogaGame InstantiateGame(MatchInfo info, GameSettings settings)
        {
            throw new NotImplementedException("not implemented");
        }


        #region Selection callbacks

        protected virtual void OnFocusChanged(GameObject newObj, GameObject oldObj)
        {
        }

        protected virtual void OnDeSelect(GameObject o)
        {
        }

        protected virtual void OnSelect(GameObject obj)
        {
        }

        #endregion

        #region abstract

        protected virtual void OnRuntimeEventProcessed(RuntimeEvent runtimeEvent)
        {
            QueueMessage($"{runtimeEvent.TypeName} <b>{runtimeEvent.SourceElement.Name}</b>", Color.green);
        }

        protected virtual void OnOpenSequence(Sequence sequence)
        {
            QueueMessage($"{sequence.Name}", Color.blue);
        }

        protected virtual void OnSequenceProcessed(Sequence sequence)
        {
            //QueueMessage($"{sequence.Name} processed");
            validCommands = BogaGame.shared.commandManager.validCommands.Select(c => c.commandString).ToList();
        }

        protected virtual void OnSolverPhaseComplete(Phase phase)
        {
            QueueMessage($"<i>END {phase}</i> <<<", Color.cyan);
        }
        
        protected virtual void OnSolverPhaseStart(Phase phase)
        {
            QueueMessage($">>><i> START  {phase}</i>",  Color.yellow);
        }
        // protected abstract void OnMilestoneProgressed(RuntimeMilestone milestone);
        // protected abstract void OnMilestoneCompleted(RuntimeMilestone milestone);

        #endregion

        #region Utils

        public IEnumerator ShowText(TextMeshProUGUI textUI, string message, Func<bool> predicate)
        {
            textUI.text = message;
            textUI.gameObject.SetActive(true);
            yield return new WaitForEndOfFrame();
            yield return new WaitUntil(predicate);
            textUI.gameObject.SetActive(false);
        }

        #endregion

        protected virtual void Show(GameObject go, TextMeshProUGUI textUI, string message, float duration = -1)
        {
            textUI.text = message;
            go.SetActive(true);
            if (duration > 0) StartCoroutine(HideCoroutine(go, duration));
        }

        protected virtual void QueueShow(GameObject go, TextMeshProUGUI textUI, string message, float duration = -1)
        {
            AnimationManager.AppendAnimation("start", duration, () => Show(go, textUI, message, duration));


        }

        private IEnumerator HideCoroutine(GameObject go, float duration = 1)
        {
            yield return new WaitForSeconds(duration);
            go.SetActive(false);
        }

        #region Messages

        public void QueueMessageAnimation(TextMeshProUGUI textUI, string message, float duration)
        {
            AnimationManager.AppendAnimation(message, duration, () => Show(textUI.gameObject, textUI, message),
                () => textUI.gameObject.SetActive(false));
        }

        [Button("queue message")]
        public void QueueMessage()
        {
            QueueMessage("test message");
        }

        public void QueueMessage(string message)
        {
            if (!messagePrefab || !messagePanel)
                return;
            GameObject messageGo = Instantiate(messagePrefab, messagePanel.transform);
            messageGo.SetActive(true);
            messageGo.GetComponentInChildren<TextMeshProUGUI>().text = message;
            messages.Enqueue(messageGo);
            CancelInvoke();
            InvokeRepeating(nameof(DestroyMessages), 1, messageLifetime);
        }

        protected void QueueMessage(string message, Color color)
        {
            QueueMessage(message);
            GameObject lastMessage = messages.LastOrDefault();
            if (lastMessage && lastMessage.GetComponentInChildren<TextMeshProUGUI>())
                lastMessage.GetComponentInChildren<TextMeshProUGUI>().color = color;
        }

        private void DestroyMessages()
        {
            if (messages.Count <= 0) return;
            GameObject message = messages.Dequeue();
            Destroy(message);
        }

        #endregion

        protected virtual void OnElementAddedToZone(IZoneEntity element, Zone inZone, Zone fromZone)
        {
            QueueMessage($"{element.Name} added to {inZone.Name}");
        }

        public virtual void QuiGame()
        {
     //       BogaGame.shared.fsm.OnEnterPhase -= OnEnterPhase;
        }

        protected virtual void SetupUI()
        {
            Debug.Log("Game ready");
        }

        public override bool CanDropOn(GameObject dragDropAction, GameObject go)
        {
            return true;
        }

        public override bool CanDrag(GameObject elementElement)
        {
            return true;
        }
    }
}