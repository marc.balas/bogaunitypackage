using System;
using System.IO;
using Boga.Assets;
using Boga.Core;
using Boga.Kit;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity
{
    /// <summary>
    /// create the game with asset files
    /// do not start the game. UIManager will do it
    /// </summary>
    ///
    public class BogaLoader : MonoBehaviour
    {
        [Required] [SerializeField] protected TextAsset gameSettings;
        [Required] [SerializeField] protected TextAsset gameInfo;
        [SerializeField] private TextAsset accountFile;
        //[SerializeField] private TextAsset assetFile;
        [SerializeField] private TextAsset poolFile;
        [SerializeField] private TextAsset glossaryFile;

        [SerializeField] private TextAsset triggerFile;
        [SerializeField] private TextAsset scopeFile;
        public static BogaLoader Shared { get; private set; }

        //public static event Action GameReady;

        //protected MatchInfo info;
        //protected GameSettings settings;
        [SerializeField] private bool createGame;

        protected MatchInfo info;
        protected GameSettings settings;
        //[SerializeField] private bool startGame;

        //  [SerializeField] private bool useCustomAssets;

        private void Awake()
        {
            //     DontDestroyOnLoad(gameObject);
        
            BogaGame.shared?.Dispose();
            BogaGame.shared = null;
        
            if (Shared != null)
            {
                Debug.Log("Loader already created. Ignore this loader");
                return;
            }

            Shared = this;
            AccountInfo.path = Path.Combine(Application.persistentDataPath, "account.json");
            LoadSettings();
            RegisterAssets();

            if (createGame && BogaGame.shared == null)
            {
                InstantiateGame();
            }
        }

        [Button("start")]
        public void StartGame()
        {
            BogaGame.shared?.Start();
        }

 

        public void LoadSettings()
        {
            info = MatchInfo.LoadFromText(gameInfo.text);

            Assert.IsNotNull(info, "Missing Game info");
            settings = GameSettings.LoadFromText(gameSettings.text);
            Assert.IsNotNull(settings, "Missing Game settings");
        }

        protected virtual void RegisterCustomAssets()
        {
            Debug.LogWarning("you need to subclass the loader RegisterCustomAssets() if you use custom asset templates");
        }

        public virtual BogaGame InstantiateGame()
        {
            throw new NotImplementedException("");
        }

        [Button]
        private void CheckAssetsConsistency()
        {
            string report = ResourceManager.Shared.ValidateAllAssets();
            if (report.Exist()) Debug.LogWarning(report);
            else Debug.Log("Consistency check successful : No errors found");
        }

        [Button]
        private void RegisterAssets()
        {
            ResourceManager.Shared.Clear();
            RegisterCustomAssets();
            RegisterCustomScopes();
            RegisterCustomTriggers();
            if (triggerFile) ResourceManager.RegisterTriggers(triggerFile.text);
            if (glossaryFile) ResourceManager.RegisterGlossary(glossaryFile.text);
            if (poolFile) ResourceManager.RegisterPoolsWithData(poolFile.text);

            ResourceManager.resourcesLoaded = true;
            //do not reload account
            if (accountFile && AccountInfo.shared == null) AccountInfo.CreateFromData(accountFile.text);
            Debug.Log(ResourceManager.Shared.ToString());
        }

        protected virtual void RegisterCustomScopes()
        {
        }

        protected virtual void RegisterCustomTriggers()
        {
        }
    }
}