using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Boga.Core;
using Boga.Kit;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BogakitUnity
{
    public class BogaSceneLoader : MonoBehaviour
    {
        [SerializeField] private GameObject loaderCanvas;
        [SerializeField] private Image progressBar;

        [NaughtyAttributes.Scene] [SerializeField] private string homeScene;

        [NaughtyAttributes.ReadOnly] [SerializeField] private string phase;

        [SerializeField] private List<ScenePhaseType> sceneDico = new List<ScenePhaseType>();
      
        [SerializeField] private float LoadingDelay = 4f;
        private void OnEnable()
        {
            if (BogaGame.shared != null) BogaGame.shared.fsm.OnEnterPhase += OnEnterPhase;
        }

        private void OnDisable()
        {
            if (BogaGame.shared != null) BogaGame.shared.fsm.OnEnterPhase -= OnEnterPhase;
        }

        private void OnExitPhase(Phase obj)
        {
        }

        private void OnEnterPhase(Phase obj)
        {
            phase = obj.name;
            Debug.Log("enter phase " + obj.name);
            ScenePhaseType sceneName = sceneDico.FirstOrDefault(s => s.phaseName.Equals(obj.name));
            LoadScene(sceneName?.sceneName, sceneName?.additive ?? false);
        }

        public async void LoadScene(string sceneName, bool additive)
        {
            if (!sceneName.Exist()) return;
            loaderCanvas.SetActive(true);

            var scene = SceneManager.LoadSceneAsync(sceneName, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            scene.allowSceneActivation = false;

            while (scene.progress < 0.9f)
            {
                progressBar.fillAmount = scene.progress;
                await Task.Delay(100);
            }

            // The scene is almost loaded, update the progress bar to full
            progressBar.fillAmount = 1f;

            // Wait for a short moment to display the full progress bar
            int delay = (int)(1000 * LoadingDelay);
            await Task.Delay(delay);

            // Activate the scene
            scene.allowSceneActivation = true;

            // Hide the loading screen
            loaderCanvas.SetActive(false);
        }

        // Start is called before the first frame update
        void Start()
        {
            loaderCanvas.SetActive(false);
            progressBar.fillAmount = 0;
        }

        [NaughtyAttributes.Button("Exit Game")]
        public void ExitGame()
        {
            BogaGame.shared.Dispose();
            LoadScene(homeScene, false);
        }

        [NaughtyAttributes.Button("start Game")]
        public void StartGame()
        {
            if (BogaGame.shared?.phase != null)
            {
                Debug.LogWarning("game already started");
                return;
            }

            if (BogaGame.shared == null)
            {
                BogaLoader.Shared.InstantiateGame();
                BogaGame.shared!.fsm.OnEnterPhase += OnEnterPhase;
            }

            BogaGame.shared!.Start();
        }

        [Serializable]
        public class ScenePhaseType
        {
            public string phaseName;
            public bool additive;
            [NaughtyAttributes.Scene] [SerializeField] public string sceneName;
        }

        public void LoadHomeScene()
        {
            if (BogaGame.shared != null) Debug.LogError("game already started");
            LoadScene(homeScene, false);
        }
    }
}