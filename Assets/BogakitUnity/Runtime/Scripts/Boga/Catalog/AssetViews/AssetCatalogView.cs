using Boga.Assets;
using Boga.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    public class AssetCatalogView : MonoBehaviour
    {
        public int nbOwned { get; private set; }
        public int nbConsumed { get; private set; }
        public int nbLeft => nbOwned - nbConsumed;

        [SerializeField] private TextMeshProUGUI nameLabel;

        // public override AssetTemplate asset { get; set; }
        public TextMeshProUGUI nbAssets;

        [SerializeField] private Image noLeftImage;

        [SerializeField] private RawImage art;

        [SerializeField] private string artAttribute;
        [SerializeField] private string resource_path;
        [SerializeField] private Color noneOwnedColor = Color.black;

        public AssetTemplate asset { get; set; }

        protected virtual void Awake()
        {
            if (noLeftImage) noLeftImage.enabled = false;
        }

        public void PopulateWithAsset(AssetTemplate _asset)
        {
            if (_asset == null)
            {
                nameLabel.text = "missing asset";
                return;
            }

            asset = _asset;
            nameLabel.text = asset.Name;
            if (art)
            {
                Texture texture;
                if (artAttribute.Exist())
                    texture = Resources.Load<Texture>(resource_path + _asset.GetAttribute(artAttribute));
                else
                    texture = Resources.Load<Texture>(resource_path + _asset.Id);
                if (texture) art.texture = texture;
                else art.enabled = false;
            }
        }


        public void SetNbOwned(int nb)
        {
            nbOwned = nb;
            nbAssets.text = $"x{nbLeft}";
            if (GetComponent<Image>())
                GetComponent<Image>().color = nbOwned > 0 ? new Color(1, 1, 1, 1) : noneOwnedColor;
        }

        public virtual void Consume()
        {
            if (nbLeft > 0)
                nbConsumed++;
            if (nbLeft == 0 && noLeftImage)
            {
                noLeftImage.enabled = true;
            }
        }
    }
}