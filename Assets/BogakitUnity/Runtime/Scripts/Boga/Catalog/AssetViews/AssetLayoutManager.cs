using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boga.Assets;
using BogakitUnity.Extensions;
using BogakitUnity.Layouts;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

namespace BogakitUnity.Boga.Catalog
{
    public class AssetLayoutManager : MonoBehaviour
    {
        [SerializeField] private AssetView assetPrefab;
        [SerializeField] private string assetName;
        [SerializeField] private Transform discardAnchor;
        [SerializeField] private Transform spawnAnchor;

        [Tooltip(" assets to be drawn")] [SerializeField]
        private List<string> deckAssets;

        [Tooltip(" refill when not full")] [SerializeField]
        private bool autofill;

        [Tooltip(" refill when empty")] [SerializeField]
        private bool renew;

        [SerializeField] private UnityEvent onSpawn;
        [SerializeField] private UnityEvent onDestroy;

        [Required("Layout must be set")] [SerializeField]
        private Layout layout;

        // [SerializeField] private Layout playLayout;
        [SerializeField] private float discardDuration = 0.4f;

        [Button("Spawn")]
        public void Spawn()
        {
            StartCoroutine(SpawnCoroutine(0.5f));
        }
    
        public void Spawn(string assetName)
        {
            StartCoroutine(SpawnCoroutine(0.5f,assetName));
        }

        private void DrawFromDeck(float duration)
        {
      
            string asset = deckAssets.FirstOrDefault() ?? assetName;
            if (deckAssets.Count > 0) deckAssets.RemoveAt(0);
            StartCoroutine(SpawnCoroutine(duration,asset));
        }

        private IEnumerator SpawnCoroutine(float duration)
        {
            yield return SpawnCoroutine( duration, assetName);
        }

        private IEnumerator SpawnCoroutine(float duration, string newAssetName)
        {
            AssetView asset = Instantiate(assetPrefab, layout.IsFull ? null : transform);
            asset.transform.position = spawnAnchor.position;
      
            try
            {
                AssetTemplate assetTemplate = ResourceManager.GetAssetTemplateWithName<AssetTemplate>(newAssetName);
                asset.PopulateWithAsset(assetTemplate);
            }
            catch (ResourceException)
            {
                asset.LoadImageFromName(newAssetName);
            }
            // layout is full
            if (asset.transform.parent == null)
            {
                asset.transform.DOMove(discardAnchor.position, discardDuration).OnComplete(() => asset.Destroy());
            }
            else
                onSpawn?.Invoke();
            //  asset.transform.DOLocalRotate(new Vector3(0, 0, 20), 0.35f).SetEase(Ease.InOutElastic).SetRelative().From();

            yield return new WaitForSeconds(duration);
            layout.Organize();

            //asset.transform.DOPunchScale()
        }

        [Button("Discard")]
        public void Discard()
        {
            //  transform.DOKill();
            var t = transform.GetChild(0);
            Sequence discard = DOTween.Sequence();
            discard.Append( t.DOMove(discardAnchor.position, discardDuration).SetEase(Ease.OutSine));
            discard.Join( t.DOLocalRotate(new Vector3(0, 0, -40), discardDuration/4).SetEase(Ease.OutBack).SetRelative());
            discard.Join( t.DOLocalRotate(new Vector3(0, 0, 0), discardDuration/3).SetDelay(discardDuration/3).SetEase(Ease.Linear));
            discard.OnComplete(() =>  t.GetComponent<AssetView>().Destroy());
     
            // discardAnchor.GetComponent<DOTweenAnimation>().targetGO = transform.GetChild(0).gameObject;
            // discardAnchor.GetComponent<DOTweenAnimation>().DORestart();
        }


        [Button("Destroy Selection")]
        public void DestroySelection()
        {
            var selected = layout.transform.GetChildren().Select(go => go.GetComponent<AssetView>())
                .Where(a => a.isSelected).ToList();
            foreach (var asset in selected)
            {
                asset.Destroy();
            }
        }

        public IEnumerator DiscardCoroutine(GameObject go, float delay)
        {
            go.transform.DOKill();
            go.transform.DOMove(discardAnchor.position, discardDuration).SetEase(Ease.OutExpo).SetDelay(delay)
                .OnComplete(() => Destroy(go));
            go.transform.DOLocalRotate(new Vector3(0, 0, 40), 0.35f).SetEase(Ease.OutBack).SetDelay(delay).SetRelative()
                .From();
            // discardAnchor.GetComponent<DOTweenAnimation>().targetGO = transform.GetChild(0).gameObject;
            // discardAnchor.GetComponent<DOTweenAnimation>().DORestart();
            yield return new WaitForSeconds(delay);
        }

        public IEnumerator DiscardAllCoroutine(float delay)
        {
            List<GameObject> children = transform.GetChildren().ToList();
            foreach (var go in children)
            {
                yield return DiscardCoroutine(go, delay);
            }
        }

        [Button("Fill")]
        public void Fill()
        {
            StartCoroutine(FillCoroutine(0.5f));
        }

        private IEnumerator FillCoroutine(float delay)
        {
            if (!layout.HasMaxCapacity) yield break;
            // draw missing amount
            Debug.Log("draw missing amount => " + (layout.Capacity - layout.transform.childCount));
            for (int i = layout.transform.childCount; i < layout.Capacity; i++)
            {
                yield return StartCoroutine(SpawnCoroutine(0.5f, assetName));
            }
        }

        /// <summary>
        /// discard all and fill with new
        /// </summary>
        [Button("Renew")]
        public void Renew()
        {
            StartCoroutine(RenewCoroutine()); //discard();
        }


        [Button("draft")]
        public void Draft()
        {
            DrawFromDeck(1);
        }

        [Button("Select all ")]
        public void SelectAll()
        {
            foreach (var go in layout.transform.GetChildren())
            {
                go.GetComponent<AssetView>().Select();
            }
        }

        [Button("destroy all ")]
        public void DestroyAll()
        {
            var children = layout.transform.GetChildren();
            foreach (var asset in children)
            {
                asset.GetComponent<AssetView>().Destroy();
            }
        }

        private IEnumerator RenewCoroutine()
        {
            yield return DiscardAllCoroutine(0.5f);
            yield return StartCoroutine(FillCoroutine(0.5f));
        }

        [Button("Replace selection")]
        public void ReplaceSelection()
        {
            StartCoroutine(ReplaceSelectionCoroutine());
        }

        // [Button("Play in layout")]
        // public void Play()
        // {
        //     if (playLayout == null) return;
        //     var selected = layout.transform.GetChildren().Select(go => go.GetComponent<AssetView>())
        //         .Where(a => a.isSelected).ToList();
        //
        //     foreach (var asset in selected)
        //     {
        //         asset.transform.SetParent(playLayout.transform);
        //     }
        //
        //     playLayout.Organize();
        //     layout.Organize();
        // }

        // private IEnumerator PlaySelectionCoroutine()
        // {
        //     if (playLayout == null) yield break;
        //     var selected = layout.transform.GetChildren().Select(go => go.GetComponent<AssetView>())
        //         .Where(a => a.isSelected).ToList();
        //
        //     int nb = selected.Count();
        //     var delay = 0f;
        //
        //     foreach (var asset in selected)
        //     {
        //         asset.transform.SetParent(playLayout.transform);
        //     }
        //
        //     playLayout.Organize();
        // }

        private IEnumerator ReplaceSelectionCoroutine()
        {
            var selected = layout.transform.GetChildren().Select(go => go.GetComponent<AssetView>())
                .Where(a => a.isSelected).ToList();

            int nb = selected.Count();
            var delay = 0f;

            foreach (var asset in selected)
            {
                yield return DiscardCoroutine(asset.gameObject, delay);
                delay += 0.5f;
            }

            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < nb; i++)
            {
                yield return SpawnCoroutine(delay);
            }
        }

        private void Awake()
        {
            //layout = transform.parent.GetComponent<Layout>();
        }

        // Start is called before the first frame update
        IEnumerator Start()
        {
            if (autofill && layout.HasMaxCapacity && !layout.IsFull)
            {
                yield return StartCoroutine(FillCoroutine(.2f));
            }

            if (renew && layout.HasMaxCapacity && layout.IsEmpty)
            {
                yield return StartCoroutine(FillCoroutine(.2f));
            }

            yield return null;
        }

        // Update is called once per frame
        void Update()
        {
            if (autofill && layout.HasMaxCapacity && !layout.IsFull)
            {
                Fill();
            }

            if (renew && layout.HasMaxCapacity && layout.IsEmpty)
            {
                Fill();
            }
        }
    }
}