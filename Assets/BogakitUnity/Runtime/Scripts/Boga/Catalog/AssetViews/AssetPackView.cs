using System;
using BogakitUnity.Selections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BogakitUnity.Boga.Catalog
{
    public class AssetPackView : MonoBehaviour, IPointerClickHandler
    {
    
        public static event Action<AssetPackView> OpenAssetPack;
        [SerializeField] private TextMeshPro nameLabel;
    
        [SerializeField] internal string packName;
        void Start()
        {
        
        }
        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.clickCount == 2)
            {
                Debug.Log("double click");
                Selection.Deselect();
                OpenAssetPack?.Invoke(this);
            }
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
