using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Boga.Assets;
using Boga.Core;
using BogakitUnity.Selections;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    /// <summary>
    /// Represents a view component that displays information about an asset.
    /// </summary>
    public class AssetView : MonoBehaviour
    {
        [SerializeField] private bool selectable;

        /// <summary>
        /// Represents an asset template.
        /// </summary>
        public AssetTemplate asset { get; private set; }


        /// <summary>
        /// Represents a label for the name of an asset.
        /// </summary>
        [SerializeField] private StatLabel nameLabel;

        /// <summary>
        /// Represents a label for displaying description of an asset.
        /// </summary>
        [SerializeField] private StatLabel descriptionLabel;

        /// <summary>
        /// Represents a view for an asset.
        /// </summary>
        [SerializeField] private StatLabel type;


        /// <summary>
        /// The string attribute used to load the asset image.
        /// </summary>
        /// <remarks>
        /// This attribute determines the path of the asset image. If not specified, the name of the asset with spaces replaced by underscores will be used.
        /// </remarks>
        [SerializeField] protected string artAttribute;

        /// be loaded from.
        [SerializeField] protected string imagePath;

        /// <summary>
        /// Represents an asset view.
        /// </summary>
        [SerializeField] protected SpriteRenderer art;

        
        /// statistic value of the associated asset.
        [SerializeField] protected List<StatLabel> stats = new List<StatLabel>();

        /// <summary>
        /// Represents a view for displaying an asset.
        /// </summary>
        [SerializeField] protected List<AttributeLabel> attributes = new List<AttributeLabel>();

        /// <summary>
        /// The onPopulate event is triggered when the AssetView is populated with an AssetTemplate.
        /// </summary>
        [Foldout("events")] [SerializeField] private UnityEvent onPopulate;
        [Foldout("events")] [SerializeField] private UnityEvent onStart;
        [Foldout("events")] [SerializeField] private UnityEvent onSelect;
        [Foldout("events")] [SerializeField] private UnityEvent onDeselect;

        // bind a action name to a click on a button
        [SerializeField] protected List<ActionButton> actions = new List<ActionButton>();


        [ShowNativeProperty] public bool isSelected { get; protected set; }

        public static event Action<AssetView> assetPlayed;
        public static event Action<AssetView, string> assetPlayedAction;


        /// <summary>
        /// The name of the asset.
        /// </summary>
        [Foldout("debug")] [SerializeField] private string assetName;


        protected virtual void Start()
        {
            onStart?.Invoke();
        }


        /// <summary>
        /// Populates the AssetView component with the provided AssetTemplate.
        /// </summary>
        /// <param name="newAsset">The AssetTemplate to populate with.</param>
        public virtual void PopulateWithAsset(AssetTemplate newAsset)
        {
            Assert.IsNotNull(newAsset, "asset cannot be null");
            asset = newAsset;
            if (nameLabel.label) nameLabel.label.text = newAsset.Name;
            if (descriptionLabel.label) descriptionLabel.label.text = newAsset.Description;

            if (type.label) type.label.text = newAsset.Type;

            LoadImageFromName(newAsset.Name);
            foreach (var stat in stats)
            {
                stat.UpdateWithValue(newAsset.GetStat(stat.statName));
            }

            foreach (var stat in attributes)
            {
                stat.UpdateWithValue(newAsset.GetAttribute(stat.attributeName));
            }

            onPopulate?.Invoke();
        }

        [Button("Load asset Image")]
        public void LoadImageFromAssetName()
        {
            LoadImageFromName(assetName);
        }

        public void LoadImageFromName(string newAsset)
        {
            string pathInfo = Path.Combine(imagePath);
            string imageName = artAttribute.Exist() ? artAttribute : newAsset.Replace(" ", "_");

            Sprite[] sprites = Resources.LoadAll<Sprite>(pathInfo);
            Sprite targetSprite = sprites.FirstOrDefault(s => s.name == imageName);

            if (targetSprite != null)
            {
                if (art) art.sprite = targetSprite;
            
            }
            else
            {
                Debug.LogWarning($"Sprite not found: {newAsset}");
            }


            // if (art) art.sprite = Resources.LoadAll<Sprite>(newAsset).RandomSampleNoPseudo() ;
            // if (artImage) artImage.sprite = Resources.Load<Sprite>(pathInfo);
        }


        /// the `AssetView` with the loaded asset.
        [Button("Load from Asset Name")]
        public void LoadFromAssetName()
        {
            try
            {
                PopulateWithAsset(ResourceManager.GetAssetTemplateWithName<AssetTemplate>(assetName));
            }
            catch (ResourceException e)
            {
                Debug.LogWarning($"Asset not found: {assetName} : {e.Message}");
            }
        }

        /// <summary>
        /// Checks all asset images and logs consistency errors.
        /// </summary>
        [Button("Check all Asset Images")]
        public void CheckAssetsImages()
        {
            string consistencyErrors = "";
            foreach (var templates in ResourceManager.Shared.templates)
            {
                foreach (var template in templates.Value)
                {
                    string pathInfo = Path.Combine(imagePath,
                        artAttribute.Exist() ? template.GetAttribute(artAttribute) : template.Name.Replace(" ", "_"));
                    if (Resources.Load<Sprite>(pathInfo) == null)
                        consistencyErrors += $"Image not found: {template.Name} {templates.Key}: {pathInfo} \n";
                }
            }

            Debug.LogWarning(consistencyErrors);
        }

        [Button("Toggle Selected")]
        protected virtual void ToggleSelected()
        {
            if (!selectable) return;

            if (isSelected) Deselect();
            else Select();
        }

        public virtual void Select()
        {
            if (isSelected)
            {
                return;
            }

            isSelected = true;
            onSelect?.Invoke();
        }

        public virtual void Deselect()
        {
            if (!isSelected)
            {
                return;
            }

            isSelected = false;
            onDeselect?.Invoke();
        }

        public virtual void OnFocus(bool hasFocus)
        {
        }

        [Button("Play")]
        public virtual void Play()
        {
            assetPlayed?.Invoke(this);
        }


        public virtual void Play(string action)
        {
            assetPlayedAction?.Invoke(this, action);
        }

        public virtual void Destroy()
        {
            //  assetPlayed?.Invoke(this);
        }
    }

    [Serializable]
    public class ActionButton
    {
        public string action;
        public Button Button;
    }

    /// <summary>
    /// Represents a label for displaying a statistic or attribute value.
    /// </summary>
    [Serializable]
    public class StatLabel
    {
 
        /// <summary>
        /// Represents a view for an asset.
        /// </summary>
        public TextMeshPro label;

        public bool HideIfZero = true;

        /// <summary>
        /// Represents a view for an asset template.
        /// </summary>
        [SerializeField] public string statName;

        public void UpdateWithValue(int stat)
        {
            if (HideIfZero && stat == 0)
            {
                if (label) label.gameObject.SetActive(false);
                return;
            }

            ;
            if (label) label.text = stat.ToString();
        
        }
    }


    /// <summary>
    /// Represents a label for displaying a statistic or attribute value.
    /// </summary>
    [Serializable]
    public class AttributeLabel
    {
  
        /// <summary>
        /// Represents a view for an asset.
        /// </summary>
        public TextMeshPro label;

        public bool HideIfEmpty = true;

        /// <summary>
        /// Represents a view for an asset template.
        /// </summary>
        [SerializeField] public string attributeName;

        public void UpdateWithValue(string value)
        {
            if (HideIfEmpty && !value.Exist())
            {
                if (label) label.gameObject.SetActive(false);
                return;
            }

            ;
            if (label) label.text = value;
       
        }
    }
}