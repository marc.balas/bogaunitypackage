using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Boga.Assets;
using Boga.Core;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    /// <summary>
    /// Represents a view component that displays information about an asset.
    /// </summary>
    public class UIAssetView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        // [SerializeField] private bool selectable;

        /// <summary>
        /// Represents an asset template.
        /// </summary>
        public AssetTemplate asset { get; private set; }


        /// <summary>
        /// Represents a label for the name of an asset.
        /// </summary>
        [SerializeField] private TextMeshProUGUI nameLabel;

        /// <summary>
        /// Represents a label for displaying description of an asset.
        /// </summary>
        [SerializeField] protected TextMeshProUGUI descriptionLabel;

        /// <summary>
        /// Represents a view for an asset.
        /// </summary>
        [SerializeField] private TextMeshProUGUI type;


        /// <summary>
        /// The string attribute used to load the asset image.
        /// </summary>
        /// <remarks>
        /// This attribute determines the path of the asset image. If not specified, the name of the asset with spaces replaced by underscores will be used.
        /// </remarks>
        // [SerializeField] protected string artAttribute;

        /// be loaded from.
        [SerializeField] protected string imagePath;

        /// <summary>
        /// Represents the image component used to display artwork of an asset.
        /// </summary>
        [SerializeField] protected Image artImage;

        /// statistic value of the associated asset.
        [SerializeField] protected List<UIStatLabel> stats = new List<UIStatLabel>();

        /// <summary>
        /// Represents a view for displaying an asset.
        /// </summary>
        [SerializeField] protected List<UIAttributeLabel> attributes = new List<UIAttributeLabel>();

        /// <summary>
        /// The onPopulate event is triggered when the AssetView is populated with an AssetTemplate.
        /// </summary>
        [Foldout("events")] [SerializeField] private UnityEvent onPopulate;

        [Foldout("events")] [SerializeField] private UnityEvent onStart;

        [FormerlySerializedAs("onSelect")] [Foldout("events")] [SerializeField]
        private UnityEvent onEnter;

        [FormerlySerializedAs("onDeselect")] [Foldout("events")] [SerializeField]
        private UnityEvent onExit;


        [ShowNativeProperty] protected bool isSelected { get; set; }

        public static event Action<UIAssetView> assetPlayed;
        public static event Action<UIAssetView, string> assetPlayedAction;


        /// <summary>
        /// The name of the asset.
        /// </summary>
        [Foldout("debug")] [SerializeField] private string assetName;

        [SerializeField] private int id;


        protected virtual void Start()
        {
            onStart?.Invoke();
        }


        /// <summary>
        /// Populates the AssetView component with the provided AssetTemplate.
        /// </summary>
        /// <param name="newAsset">The AssetTemplate to populate with.</param>
        public virtual void PopulateWithTemplate(AssetTemplate newAsset)
        {
            Assert.IsNotNull(newAsset, "asset cannot be null");
            asset = newAsset;
            if (nameLabel) nameLabel.text = newAsset.Name;
            if (descriptionLabel) descriptionLabel.text = newAsset.Description;

            if (type) type.text = newAsset.Type;

            LoadImageFromName(newAsset.Name);
            foreach (var stat in stats)
            {
                stat.UpdateWithValue(newAsset.GetStat(stat.statName));
            }

            foreach (var stat in attributes)
            {
                stat.UpdateWithValue(newAsset.GetAttribute(stat.attributeName));
            }

            onPopulate?.Invoke();
        }

        [Button("Populate with template id")]
        public void PopulateWithId()
        {
            if (id > 0) PopulateWithTemplate(ResourceManager.GetAnyTemplateWithId(id));
        }


        [Button("Load asset Image")]
        public void LoadImageFromAssetName()
        {
            LoadImageFromName(assetName);
        }

        public void LoadImageFromName(string newAsset)
        {
            string pathInfo = Path.Combine(imagePath);
            string imageName = newAsset.Replace(" ", "_");

            Sprite[] sprites = Resources.LoadAll<Sprite>(pathInfo);
            Sprite targetSprite = sprites.FirstOrDefault(s => s.name == imageName);

            if (targetSprite != null)
            {
                if (artImage) artImage.sprite = targetSprite;
            }
            else
            {
                Debug.LogWarning($"Sprite not found: {newAsset}");
            }


            // if (art) art.sprite = Resources.LoadAll<Sprite>(newAsset).RandomSampleNoPseudo() ;
            // if (artImage) artImage.sprite = Resources.Load<Sprite>(pathInfo);
        }


        /// the `AssetView` with the loaded asset.
        [Button("Load from Asset Name")]
        public void LoadFromAssetName()
        {
            try
            {
                PopulateWithTemplate(ResourceManager.GetAssetTemplateWithName<AssetTemplate>(assetName));
            }
            catch (ResourceException e)
            {
                Debug.LogWarning($"Asset not found: {assetName} : {e.Message}");
            }
        }

        /// <summary>
        /// Checks all asset images and logs consistency errors.
        /// </summary>
        [Button("Check all Asset Images")]
        public void CheckAssetsImages()
        {
            string consistencyErrors = "";
            foreach (var templates in ResourceManager.Shared.templates)
            {
                foreach (var template in templates.Value)
                {
                    string pathInfo = Path.Combine(imagePath, template.Name.Replace(" ", "_"));
                    if (Resources.Load<Sprite>(pathInfo) == null)
                        consistencyErrors += $"Image not found: {template.Name} {templates.Key}: {pathInfo} \n";
                }
            }

            Debug.LogWarning(consistencyErrors);
        }

        [Button("Toggle Selected")]
        protected virtual void ToggleSelected()
        {
            //   if (!selectable) return;

            if (isSelected) Deselect();
            else Select();
        }

        public virtual void Select()
        {
            if (isSelected)
            {
                return;
            }

            isSelected = true;
            onEnter?.Invoke();
        }

        public virtual void Deselect()
        {
            if (!isSelected)
            {
                return;
            }

            isSelected = false;
            onExit?.Invoke();
        }

        public virtual void OnFocus(bool hasFocus)
        {
        }

        [Button("Play")]
        public virtual void Play()
        {
            assetPlayed?.Invoke(this);
        }


        public virtual void Play(string action)
        {
            assetPlayedAction?.Invoke(this, action);
        }

        public virtual void Destroy()
        {
            //  assetPlayed?.Invoke(this);
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            onEnter?.Invoke();
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            onExit?.Invoke();
        }
    }

    [Serializable]
    public class UIActionButton
    {
        public string action;
        [FormerlySerializedAs("Button")] public Button button;
    }

    /// <summary>
    /// Represents a label for displaying a statistic or attribute value.
    /// </summary>
    [Serializable]
    public class UIStatLabel
    {
        /// <summary>
        /// Represents a view for displaying an asset.
        /// </summary>
        [SerializeField] public TextMeshProUGUI labelUI;


        public bool HideIfZero = true;

        /// <summary>
        /// Represents a view for an asset template.
        /// </summary>
        [SerializeField] public string statName;

        public void UpdateWithValue(int stat)
        {
            if (HideIfZero && stat == 0)
            {
                if (labelUI) labelUI.gameObject.SetActive(false);
                return;
            }
            if (labelUI) labelUI.text = stat.ToString();
        }
    }


    /// <summary>
    /// Represents a label for displaying a statistic or attribute value.
    /// </summary>
    [Serializable]
    public class UIAttributeLabel
    {
        /// <summary>
        /// Represents a view for displaying an asset.
        /// </summary>
        [SerializeField] public TextMeshProUGUI labelUI;

        public bool HideIfEmpty = true;

        /// <summary>
        /// Represents a view for an asset template.
        /// </summary>
        [SerializeField] public string attributeName;

        public void UpdateWithValue(string value)
        {
            if (HideIfEmpty && !value.Exist())
            {
                if (labelUI) labelUI.gameObject.SetActive(false);
                return;
            }

            if (labelUI) labelUI.text = value;
        }
    }
}