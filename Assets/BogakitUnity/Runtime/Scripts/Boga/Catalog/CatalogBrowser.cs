using System;
using System.Collections.Generic;
using System.Linq;
using Boga.Assets;
using BogakitUnity.Extensions;
using BogakitUnity.UI.Popups;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    public class CatalogBrowser : MonoBehaviour
    {
        public AccountInfo playerCatalog => AccountInfo.shared;
        private PopupManager popupManager;

        [InfoBox("class name of assets to populate the catalog")] [SerializeField]
        private string assetType = "asset";

        [SerializeField] private string WildCard = "all";
        [SerializeField] private bool allowCrafting;
        [SerializeField] private bool allowDeckCreation;

        private List<AssetCatalogView> assetViews = new List<AssetCatalogView>();
        private List<AssetCatalogView> assetViewsFiltered = new List<AssetCatalogView>();

        [SerializeField] private TextMeshProUGUI title;

        [FormerlySerializedAs("assetPrefab")] [SerializeField]
        private AssetCatalogView assetCatalogPrefab;

        [SerializeField] private GameObject assetContainer;

        [InfoBox("Assets list : will overwrite loaded assets")]
        public TextAsset catalog;

        [FormerlySerializedAs("catalogPlayer")] [InfoBox("Account : will overwrite player account")]
        public TextAsset accountFile;

        [SerializeField] private string attributeFilterName;
        [SerializeField] private string statFilterName;

        [FormerlySerializedAs("isCrafting")] [SerializeField]
        private bool showNotOwned;

        public static event Action<AssetCatalogView> CatalogAssetClicked;

        private void Awake()
        {
            //Assert.IsNotNull(catalog);
            //Assert.IsNotNull(catalogPlayer);

            // AccountInfo accountInfo = AccountInfo.CreateFromData(catalogPlayer.text);

            popupManager = FindObjectOfType<PopupManager>();
            AccountInfo.path =  Application.persistentDataPath + "/account.json";
            if (accountFile) AccountInfo.CreateFromData(accountFile.text);
            if (AccountInfo.shared == null)
            {
                
                AccountInfo.LoadFromFile( Application.persistentDataPath,"account.json" );
            }
           
            if (catalog) ResourceManager.RegisterAssets<AssetTemplate>(catalog.text);
            assetContainer.transform.Clear();
            //Debug.Log($"assembly: {BogaGame.shared.Assembly.FullName}");
        }

        private void OnEnable()
        {
            DeckBuilder.DeckSelected += DeckBuilderOnDeckSelected;
        }

        private void OnDisable()
        {
            DeckBuilder.DeckSelected -= DeckBuilderOnDeckSelected;
        }

        private void DeckBuilderOnDeckSelected(Deck obj)
        {
            showNotOwned = false;
            UpdateAssets();
        }

        /// <summary>
        /// change logic to add card cost or card limitation
        /// </summary>
        /// <param name="assetCatalogView"></param>
        protected virtual void BuyAsset(AssetCatalogView assetCatalogView)
        {
            if (!showNotOwned)
                return;
            // int nb = playerCatalog.catalog.GetNbAssets(assetCatalogView.asset.Id);
            //if (nb == 0)
            {
                popupManager.OpenPopup<PopupTwoButtons>("PopupCraft", (popup) =>
                {
                    popup.text.text = $"Do you want to craft {assetCatalogView.asset.Name} ?\ncost 100";
                    popup.button.text.text = "Yes";
                    popup.button2.text.text = "No";
                    popup.button.onClickEvent.AddListener(() =>
                    {
                        assetCatalogView.SetNbOwned(playerCatalog?.AddAsset(assetCatalogView.asset).nb ??
                                                                         0);
                        popup.Close();
                    });
                    
                    popup.button2.onClickEvent.AddListener(popup.Close);
                });
            }
        }

        private void Start()
        {
            foreach (var template in ResourceManager.GetTemplatesOfType<AssetTemplate>(assetType))
            {
                GameObject asset = Instantiate(assetCatalogPrefab.gameObject, assetContainer.transform);
                AssetCatalogView assetCatalogView = asset.GetComponent<AssetCatalogView>();
                assetCatalogView.PopulateWithAsset(template);
                int nb = playerCatalog?.GetNbAssetsWithId(template.Id) ?? 0;
                assetCatalogView.SetNbOwned(nb);
                assetCatalogView.GetComponent<Button>().onClick.AddListener(() =>
                {
                    CatalogAssetClicked?.Invoke(assetCatalogView);
                    BuyAsset(assetCatalogView);
                });
                assetViews.Add(assetCatalogView);
            }

            assetViewsFiltered = assetViews;
            // load asset from player´s catalog
            UpdateAssets();
        }

        public void ToggleCrafting()
        {
            showNotOwned = !showNotOwned;
            UpdateAssets();
        }

        private void FilterAssetsByStat(int value, string statName)
        {
            assetViewsFiltered =
                value < 0 ? assetViews : assetViews.Where(a => a.asset.GetStat(statName) == value).ToList();
            UpdateAssets();
        }

        public void FilterAssetsByStatValue(int value)
        {
            assetViewsFiltered = value < 0
                ? assetViews
                : assetViews.Where(a => a.asset.GetStat(statFilterName) == value).ToList();
            UpdateAssets();
        }

        // public void FilterAssetsCost(int value)
        // {
        //     FilterAssetsByStat(value, statFilterName);
        // }

        private void UpdateAssets()
        {
            title.text = $"catalog: {(showNotOwned ? "Crafting" : "deck building")}";

            foreach (var assetView in assetViews)
            {
                assetView.gameObject.SetActive(false);
            }

            foreach (var assetView in assetViewsFiltered)
            {
                assetView.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, assetView.nbOwned > 0 ? 1 : 0.5f);
                assetView.gameObject.SetActive(showNotOwned || assetView.nbOwned > 0);
            }
        }

        public void FilterAssetsByAttribute(string attributeValue, string attributeName)
        {
            if (attributeValue.Equals(WildCard, StringComparison.CurrentCultureIgnoreCase) || attributeValue == "")
            {
                assetViewsFiltered = assetViews;
            }
            else if (attributeName.Equals("type", StringComparison.CurrentCultureIgnoreCase))
            {
                assetViewsFiltered = assetViews.Where(a =>
                    a.asset.Type.Equals(attributeValue, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            else
            {
                assetViewsFiltered = assetViews.Where(a => a.asset.GetAttribute(attributeName) == attributeValue)
                    .ToList();
            }

            UpdateAssets();
        }

        public void FilterAssetsByAttribute(string attributeValue)
        {
            FilterAssetsByAttribute(attributeValue, attributeFilterName);
        }

        public void FilterAssetsByText(string searchText)
        {
            assetViewsFiltered = assetViews.Where(a =>
                    a.asset.Description.Contains(searchText) || a.asset.Name.ToLower().Contains(searchText.ToLower()))
                .ToList();
            UpdateAssets();
        }
    }
}