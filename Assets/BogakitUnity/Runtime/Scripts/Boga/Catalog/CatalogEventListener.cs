using Boga.Assets;
using BogakitUnity.Extensions;
using TMPro;
using UnityEngine;

namespace BogakitUnity.Boga.Catalog
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class CatalogEventListener: MonoBehaviour
    {
        
        CatalogBrowser catalogBrowser;


        [SerializeField] private TextMeshProUGUI label;
        private void Awake()
        {
            label = GetComponent<TextMeshProUGUI>();
            catalogBrowser = FindObjectOfType<CatalogBrowser>();
          
        }

        private void Start()
        {
            if (AccountInfo.shared == null)
            {
                label.text = "No account";
                return;
            }

            
            CatalogBrowser.CatalogAssetClicked += CatalogBrowserOnCatalogAssetClicked;
            AccountInfo.shared.AssetModified += OnAssetModified;
            AccountInfo.shared.DeckAdded += OnDeckAdded;
            AccountInfo.shared.DeckSaved += OnDeckSaved;
                
        }

        private void OnDisable()
        {
            
            if (AccountInfo.shared == null)
            {
                return;
            }
         
            CatalogBrowser.CatalogAssetClicked -= CatalogBrowserOnCatalogAssetClicked;
            AccountInfo.shared.AssetModified -= OnAssetModified;
            AccountInfo.shared.DeckSaved -= OnDeckSaved;
            AccountInfo.shared.DeckAdded -= OnDeckAdded;
        }

        private void CatalogBrowserOnCatalogAssetClicked(AssetCatalogView asset)
        {
            label.ShowText( $"{asset} clicked",1); 
        }

        private void OnDeckAdded(Deck deck)
        {
            label.ShowText( $"Deck {deck} created",1); 
        }

        private void OnDeckSaved(Deck deck)
        {
            label.ShowText( $"Deck {deck} saved",1); 
        }

        private void OnAssetModified(AssetCardinality asset)
        {
            label.ShowText( $"{asset}",1); 
        }

    }
}