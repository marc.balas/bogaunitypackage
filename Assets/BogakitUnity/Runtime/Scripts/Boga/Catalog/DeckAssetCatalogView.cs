using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity.Boga.Catalog
{
    public class DeckAssetCatalogView : AssetCatalogView
    {
        [SerializeField] private DeckBuilder deckBuilder;

        protected void Start()
        {
            deckBuilder = FindObjectOfType<DeckBuilder>();
        }

        [Button]
        public void RemoveAsset()
        {
           deckBuilder.deck.RemoveAsset(asset.Id);
        }

    }
}