using System;
using System.Linq;
using Boga.Assets;
using BogakitUnity.Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    public class DeckBuilder : MonoBehaviour
    {
        private CatalogBrowser catalogBrowser;

        [SerializeField]
        private TextMeshProUGUI deckTitle;

        [SerializeField]
        private GameObject deckSlotPrefab;

        [SerializeField]
        private GameObject deckItemPrefab;

        [SerializeField]
        private Transform deckSlotContainer;
        [SerializeField]
        private Button saveDeckButton;
        [SerializeField]
        private Button newDeckButton;

        // if null show list of decks
        public Deck deck { get; private set; }
        public static event Action<Deck> DeckSelected;
        // Start is called before the first frame update
        void Start()
        {
            catalogBrowser = FindObjectOfType<CatalogBrowser>();
            Assert.IsNotNull(catalogBrowser);
            DeselectDeck();
        }

        private void OnEnable()
        {
            CatalogBrowser.CatalogAssetClicked += CatalogBrowserOnCatalogAssetClicked;
            Deck.AssetRemoved += UpdateDeckAsset;
            Deck.AssetAdded += UpdateDeckAsset;
        }
    

        private void UpdateDeckAsset(int assetId)
        {
            Assert.IsNotNull(deck);
     
            AssetCatalogView assetCatalogView = deckSlotContainer.GetChildren().Select(go => go.GetComponent<AssetCatalogView>()).FirstOrDefault(v => v?.asset.Id == assetId);
            if (assetCatalogView)
            {
                int nb = deck.GetNbAsset(assetId);
                if (nb == 0)
                    DestroyImmediate(assetCatalogView.gameObject);
                else assetCatalogView.SetNbOwned(nb);
            }

            else
            {
                GameObject go = Instantiate(deckSlotPrefab, deckSlotContainer);
                AssetCatalogView assetCatalog = go.GetComponent<AssetCatalogView>();
                AssetTemplate assetTemplate = ResourceManager.GetAssetTemplateWithId<AssetTemplate>(assetId);
                assetCatalog.PopulateWithAsset(assetTemplate);
                assetCatalog.SetNbOwned(1);
            }
        }

        private void OnDisable()
        {
            CatalogBrowser.CatalogAssetClicked -= CatalogBrowserOnCatalogAssetClicked;
            Deck.AssetRemoved -= UpdateDeckAsset;
            Deck.AssetAdded -= UpdateDeckAsset;
        }

        private void CatalogBrowserOnCatalogAssetClicked(AssetCatalogView obj)
        {
            if (deck == null)
                return;
            deck?.AddAsset(obj.asset);
            UpdateDeckAsset(obj.asset.Id);
        }

        private void DeselectDeck()
        {
            if( catalogBrowser.playerCatalog == null)return;
            deck = null;
            deckSlotContainer.Clear();
        
            newDeckButton.gameObject.SetActive(true);
            saveDeckButton.gameObject.SetActive(false);
            foreach (var deck1 in catalogBrowser.playerCatalog.decks)
            {
                GameObject go = Instantiate(deckItemPrefab, deckSlotContainer);
                go.GetComponent<DeckView>()?.Populate(deck1.descriptions);
                go.GetComponent<DeckView>()?.selectDeckButton.onClick.AddListener(() => SelectDeck(deck1));
                go.GetComponentInChildren<DeckView>()?.deleteDeckButton.onClick.AddListener(() => DeleteDeck(deck1));
            }
        }

        public void CreateDeck()
        {
        
            SelectDeck(catalogBrowser.playerCatalog?.CreateDeck("New deck"));
        }

        private void DeleteDeck(Deck deckToDelete)
        {
        
            catalogBrowser.playerCatalog.DeleteDeck(deckToDelete);
            DeselectDeck();
        }

        public void SaveDeck()
        {
            Assert.IsNotNull(deck);
            DeselectDeck();
        }

        private void SelectDeck(Deck deckToSelect)
        {
            if (deckToSelect == null)
            {
                DeselectDeck();
                return;
            }
            deck = deckToSelect;
            deckSlotContainer.Clear();
            deckTitle.text = deck.name;
            newDeckButton.gameObject.SetActive(false);
            saveDeckButton.gameObject.SetActive(true);
            DeckSelected?.Invoke(deck);

            // create an item for each template of the deck
            foreach (var kvp in deck.assets)
            {
                GameObject go = Instantiate(deckSlotPrefab, deckSlotContainer);
                AssetCatalogView assetCatalogView = go.GetComponent<AssetCatalogView>();
                AssetTemplate assetTemplate = ResourceManager.GetAnyTemplateWithId(kvp.id);
                assetCatalogView.PopulateWithAsset(assetTemplate);
                assetCatalogView.SetNbOwned(kvp.nb);
            }
        }
    }
}