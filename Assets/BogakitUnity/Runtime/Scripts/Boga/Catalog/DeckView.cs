using BogakitUnity.UI;
using UnityEngine;
using UnityEngine.UI;

namespace BogakitUnity.Boga.Catalog
{
    public class DeckView : MultipleTextUIView
    {
    
        [SerializeField]
        public Button deleteDeckButton;
    
        [SerializeField]
        public Button selectDeckButton;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
