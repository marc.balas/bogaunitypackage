using System;
using Boga.Core;
using TMPro;
using UnityEngine;
using DG.Tweening;



[RequireComponent(typeof(TextMeshProUGUI))]
public class CommandListener : MonoBehaviour
{

    private TextMeshProUGUI info;
    private void OnEnable()
    {
        Command.CommandExecuted += OnCommandExecuted;
        info = GetComponent<TextMeshProUGUI>();
    }

    private void OnDisable()
    {
        Command.CommandExecuted -= OnCommandExecuted;
    }

    private void OnCommandExecuted(Command command, Exception ex)
    {
        if (info == null) return;
        if (ex == null)
        {
            info.color = Color.green;
            info.text = command.commandString + " executed";
        }
        else
        {
            info.color = Color.red;
            info.text = ex.Message;
        }

        Sequence s = DOTween.Sequence();
        s.Append(info.DOFade(1, .5f));
        s.AppendInterval(3);
        s.Append(info.DOFade(0, 2));
    }
}
