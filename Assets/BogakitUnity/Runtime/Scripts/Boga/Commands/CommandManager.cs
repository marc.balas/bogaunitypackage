﻿using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using Boga.Kit;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace BogakitUnity.Helpers
{
    public sealed class CommandManager : MonoBehaviour
    {
        public string command;
        private string _command;

        private float coolDown = 0.5f;
        private float lastCoolDown = 0;

        [FormerlySerializedAs("validCommands")] [ReadOnly]
        public List<string> matchingCommands;

        [ReadOnly] public List<string> validCommands;
        [ReadOnly] public List<string> debugCommands;

        private BogaGame game;


        private void Awake()
        {
            game = BogaGame.shared;
        }


        [Button("execute")]
        private void Execute()
        {
            game.ExecuteCommandString(command);
        }

        [Button("execute command index")]
        public void ExecuteIndex(int index)
        {
            game.ExecuteCommand(game.commandManager.validCommands[index]);
        }

        [Button("execute first")]
        public void ExecuteFirst()
        {
            game.ExecuteCommand(game.commandManager.validCommands.First());
        }

        [Button("execute last")]
        public void ExecuteLast()
        {
            game.ExecuteCommand(game.commandManager.validCommands.Last());
        }


        [Button("execute random command")]
        public void ExecuteRandom()
        {
            game.ExecuteCommand(game.commandManager.validCommands.RandomSample());
        }

        public void ExecuteCustom(string custom)
        {
            game.ExecuteCommandString(custom);
        }

        private void Update()
        {
            if (Time.time - lastCoolDown < coolDown)
            {
                return;
            }

            if (command != "" && command != _command)
            {
                Check();
            }

            UpdateValidCommands();
        }

        [Button("update valid commands")]
        private void UpdateValidCommands()
        {
            validCommands = game?.commandManager?.validCommands.Select(c => c.commandString).ToList() ??
                            new List<string>();

            //debugCommands = game?.commandManager.debugCommands;
        }

        [Button("check")]
        private void Check()
        {
            if (game == null)
            {
                Debug.LogError("game is null");
                return;
            }

            lastCoolDown = Time.time;
            _command = command;
            matchingCommands.Clear();
            matchingCommands.AddRange(game.commandManager.validCommands.Where(c=>c.commandString.StartsWith( _command)).Select(c => c.commandString));
        }
    }
}