using System.Collections.Generic;
using Boga.Solver;
using TMPro;
using UnityEngine;
using DG.Tweening;
using Sequence = DG.Tweening.Sequence;


[RequireComponent(typeof(TextMeshProUGUI))]
public class EventListener : MonoBehaviour
{

    private TextMeshProUGUI info;
    
    
    private Queue<string> infos = new Queue<string>();
    private void OnEnable()
    {
        EventSolver.OnEventRaised += EventSolverOnOnEventRaised ;
        info = GetComponent<TextMeshProUGUI>();
    }

   private void OnDisable()
    {
        EventSolver.OnEventRaised -= EventSolverOnOnEventRaised ;
    }

   
   
    private void EventSolverOnOnEventRaised(RuntimeEvent obj)
    {
        infos.Enqueue(obj.Info);

        Show();
        
      
    }

    private void Show()
    {
        if (infos.Count == 0)
        {
           info.DOFade(0, 2);
            return;
        }
        
        string text =  infos.Dequeue();
        if (info == null ) return;
        info.color = Color.green;
        info.text = text + " raised";
      
        Sequence s = DOTween.Sequence();
        s.Append(info.DOFade(1, .5f));
        s.AppendInterval(3);
        s.Append(info.DOFade(0, 2));
        s.OnComplete( Show );
    }
}
