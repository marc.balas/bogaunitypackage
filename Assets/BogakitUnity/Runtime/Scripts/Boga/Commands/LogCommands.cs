﻿using Boga.Kit;
using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity.Helpers
{
    
    public sealed class LogCommands : MonoBehaviour
    {

        
        private BogaGame game;
        private void Start()
        {
            game = BogaGame.shared;
        }

        [Button("Log players")]
        private void LogPlayers()
        {
            if(game == null)
                return;
            game.ExecuteCommandString("players");
        }
        
        [Button("Log Zones")]
        private void LogZones()
        {
            if(game == null)
                return;
            game.ExecuteCommandString("zones");
        }
    }
}