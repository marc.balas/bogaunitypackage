using Boga.Core;
using Boga.Kit;

namespace BogakitUnity.EntityViews.Abstract
{
    public abstract class ElementView : EntityView
    {
        //public Element element;// => entity as Element;
        public virtual Element element { get; protected set; }
        public override IEntity Entity => element;
        //public TextMeshPro title; 
        
       // public ZoneView zoneView;
       public abstract void PopulateWithElement(Element el);
    }
}