using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using UnityEngine;

namespace BogakitUnity.EntityViews.Abstract
{
    public abstract class EntityView : MonoBehaviour //, IFocusable
    {
        public static readonly List<EntityView> all = new List<EntityView>();
        
        // NOTE: zone and element can share unique Id. collision safe with class filtering OfType<T>()
        public static T WithId<T>(int id) where T: EntityView => all.OfType<T>().FirstOrDefault(c => c.Entity?.Id == id);
        public abstract IEntity Entity { get; }
       // public abstract void OnFocus(bool hasFocus);

        public override string ToString() => name;

       // public virtual void OnStatChanged(string statType, int newValue, int oldValue){}

        protected virtual void Awake()
        {
           all.Add(this);
        }

        protected virtual  void OnDestroy()
        {
            all.Remove(this);
        }

        public virtual void Select()
        {
            
        }
    }
}