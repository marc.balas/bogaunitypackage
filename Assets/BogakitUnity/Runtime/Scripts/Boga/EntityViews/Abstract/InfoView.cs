using Boga.Core;
using UnityEngine;

namespace BogakitUnity.EntityViews.Abstract
{
    public abstract class InfoView : MonoBehaviour
    {
        public abstract void Populate<T>(T el) where T : IEntity ;
    }
}