using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity
{
   

    [CreateAssetMenu(menuName = "Bogakit/PrefabType", fileName = "PrefabType", order = 0)]
    public class PrefabType : ScriptableObject
    {
        
        public string assetType;
        [ShowAssetPreview]
        public GameObject prefab;
        
    }
}