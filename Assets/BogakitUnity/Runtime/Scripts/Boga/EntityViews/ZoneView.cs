﻿using System;
using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using Boga.Kit;
using BogakitUnity.EntityViews.Abstract;
using BogakitUnity.Extensions;
using BogakitUnity.Layouts;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

namespace BogakitUnity.EntityViews
{

    [DisallowMultipleComponent]
    [AddComponentMenu("BogaKit/ZoneView", order: -1)]
    public class ZoneView : EntityView
    {
        #region FIELDS

        [SerializeField]
        private UnityEvent<ZoneView> onEnterAction;
        [SerializeField]
        private UnityEvent<ZoneView> onExitAction;

        public Zone zone { get; private set; }
        public Layout Layout;

        // [SerializeField]
        // private string name;
        [SerializeField]
        private string type;
        
        [Tooltip("use  transforms in anchors parent as references for elements' transforms")]
        [SerializeField]
        private Transform anchorsParent;

        public List<Transform> Anchors => anchorsParent.GetChildren().Select(g => g.transform).ToList();
        public List<ElementView> ElementViews => GetComponentsInChildren<ElementView>().ToList();

        public override IEntity Entity => zone;
        
        [SerializeField] private float defaultDuration = .5f;
        [SerializeField] private List<PrefabType> prefabs;

        #region SPACES

        // [Button]
        // private void CreateSpace(int index = -1)
        // {
        //     zone?.AddSpaceAtIndex(index);
        // }

        #endregion

        #region API

        public void SetZone(Zone newZone)
        {
            Assert.IsNotNull(newZone);
            zone = newZone;
            Debug.Log($"zone {newZone.Name} assigned to {name}");
        }

        [Button]
        public void ShuffleAndOrganize()
        {
            zone.ShuffleElements();
            Organize();
        }
        
        [Button]
        public void SyncElements()
        {
            //destroy all elements
            Layout.transform.Clear();
            foreach (var el in zone.GetElements< IZoneEntity>().OfType<Element>())
            {
                //create new element
               var go = Instantiate(PrefabForElement(el), Layout.transform);
               go.GetComponent<ElementView>().PopulateWithElement(el);
            }
        }

        private GameObject PrefabForElement(Element el)
        {
           return prefabs.FirstOrDefault( p => p.assetType == el.template.Type)?.prefab;
        }

        #endregion
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            onEnterAction?.Invoke(this);
        }
        private void OnCollisionExit2D(Collision2D other)
        {
            onExitAction?.Invoke(this);
        }

        public void Organize()
        {
            if (!Layout) return;
            // change layout children order base on data
            // sort children with index of element
            List<GameObject> gos = Layout.transform.GetChildren().Where(t => t.GetComponent<ElementView>()!=null).ToList();
                    
            List<ElementView> views = gos.Select(t => t.GetComponent<ElementView>()).OrderBy(v => v.element.indexInZone).ToList();
            for (int i = views.Count - 1; i >= 0; i--)
            {
                ElementView view = views[i];

                view.transform.SetSiblingIndex(0);
            }
            Layout.Organize(defaultDuration);
        }

        [ContextMenu("destroy all elements")]
        public void DestroyAllElements()
        {
            //views and element
            Layout.transform.Clear();
            zone.elements.Clear();
        }

        #endregion

        public void Organize(float duration, Action onComplete = null)
        {
            if (Layout)
            {
                Layout.Organize(duration, onComplete);
            }
        }

        public void AddElement(EntityView entity)
        {
            entity.transform.SetParent(Layout ? Layout.transform : transform);
          
        }
    }
}