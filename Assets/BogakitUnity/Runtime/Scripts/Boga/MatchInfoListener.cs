
using Boga.Core;
using BogakitUnity.Extensions;
using TMPro;
using UnityEngine;
using DG.Tweening;


namespace BogakitUnity.UI
{

    [RequireComponent(typeof(TextMeshProUGUI))]
    public class MatchInfoListener : MonoBehaviour
    {
        private TextMeshProUGUI info;
        private float anchorY;

        private void OnEnable()
        {
            MatchInfo.ValueChanged += OnMatchInfoChanged;
            info = GetComponent<TextMeshProUGUI>();
            anchorY = info.transform.position.y;
        }

        private void OnDisable()
        {
            MatchInfo.ValueChanged -= OnMatchInfoChanged;
        }

        private void OnMatchInfoChanged(string what, string newValue)
        {
            if (info == null) return;

            info.color = Color.green;
            info.text = $"{what} set to {newValue}";
            info.transform.position = info.transform.position.WithY(anchorY);
            DOTween.KillAll();
            Sequence s = DOTween.Sequence();
            s.Append(info.DOFade(1, .5f));
            s.AppendInterval(2);
            s.Append(info.transform.DOMoveY(anchorY + 50, .5f).SetEase(Ease.OutExpo));
            s.Join(info.DOFade(0, .5f));

        }
    }
}