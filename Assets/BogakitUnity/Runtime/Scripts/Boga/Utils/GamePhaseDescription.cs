using Boga.Kit;
using TMPro;
using UnityEngine;

namespace BogakitUnity
{
    [RequireComponent(typeof( TextMeshProUGUI))]
    public class GamePhaseDescription : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            gameObject.GetComponent<TextMeshProUGUI>().text = BogaGame.shared?.phase.description;
        }
    }
}
