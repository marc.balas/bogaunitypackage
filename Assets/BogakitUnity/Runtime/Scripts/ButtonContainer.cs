using System;
using System.Collections.Generic;
using BogakitUnity.Extensions;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BogakitUnity
{
    public class ButtonContainer : MonoBehaviour
    {

        [SerializeField]
        private Button buttonPrefab;
    
        [SerializeField]
        private List<Button> buttons;
    
        // Start is called before the first frame update
        void Start()
        {
            //GetComponentInParent<Canvas>().gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        // Update is called once per frame

        [Button("Add button")]
        public void AddTestButton()
        {
            Button newButton = Instantiate(buttonPrefab, transform);
            newButton.onClick.AddListener( ()=> Debug.Log("Test button clicked"));
            newButton.name = $"button {transform.childCount}";
            newButton.GetComponentInChildren<TextMeshProUGUI>().text = newButton.name;
        }

        public void PopulateWithActions(List<(string,Action)> actions)
        {
        
            buttons.ForEach(b=>Destroy(b.gameObject));
            buttons.Clear();

            transform.Clear();
            foreach (var item in actions)
            {
                Button newButton = Instantiate(buttonPrefab,transform);
                newButton.onClick.AddListener( ()=> item.Item2());
                newButton.GetComponentInChildren<TextMeshProUGUI>().text = item.Item1;
            }
        }
    }
}
