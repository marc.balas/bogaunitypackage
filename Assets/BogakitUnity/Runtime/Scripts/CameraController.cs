using System.Collections;
using BogakitUnity.Extensions;
using BogakitUnity.Selections;
using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity
{
    public class CameraController : MonoBehaviour
    {
    
        [SerializeField] private float edgeThreshold = 10f; // Distance from screen edge to trigger movement
        [SerializeField] private  float maxMoveSpeed = 10f; // Maximum speed the camera can move
        [SerializeField] private  float smoothTime = 0.2f; // Smoothing factor

        // [SerializeField]
        // private Bounds _bounds;
        [SerializeField] private Vector3 minVector;
        [SerializeField] private Vector3 maxVector;
        private Vector3 velocity = Vector3.zero; // Used for smooth damp
        // Start is called before the first frame update
        void Start()
        {
        }

        void Update()
        {
        
            Vector3 moveDirection = Vector3.zero;
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                moveDirection = Vector3.left;
                ;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                moveDirection = Vector3.right;
            
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                moveDirection = Vector3.up;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                moveDirection = Vector3.down;
            
            }

            if (Input.GetKey(KeyCode.Q))
            {
                moveDirection = Vector3.forward;
            
            }

            if (Input.GetKey(KeyCode.A))
            {
                moveDirection = Vector3.back;
            }

        
      
        
            //Vector3 targetPosition;// = transform.position + moveMouseDirection * maxMoveSpeed * Time.unscaledDeltaTime;
        
            // Apply smoothed movement
      

            if (Input.anyKey)
            {
                transform.position += moveDirection * maxMoveSpeed * Time.unscaledDeltaTime * 0.2f;
           
            
            }
            else
            {
                Vector3 moveMouseDirection = GetMoveDirectionBasedOnMousePosition();
                Vector3 targetPosition = transform.position + moveMouseDirection * maxMoveSpeed * Time.unscaledDeltaTime;
                transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
            }
            transform.position = transform.position.ClampVector3(minVector, maxVector);
            //  Debug.Log("Clamped Vector: " + transform.position);
        }
    
        Vector3 GetMoveDirectionBasedOnMousePosition()
        {
            Vector3 moveDirection = Vector3.zero;
            Vector2 mousePosition = Input.mousePosition;

            // Check horizontal movement
            if (mousePosition.x < edgeThreshold)
            {
                moveDirection.x = -1; // Move left
            }
            else if (mousePosition.x > Screen.width - edgeThreshold)
            {
                moveDirection.x = 1; // Move right
            }

            // Check vertical movement
            if (mousePosition.y < edgeThreshold)
            {
                moveDirection.y = -1; // Move down
            }
            else if (mousePosition.y > Screen.height - edgeThreshold)
            {
                moveDirection.y = 1; // Move up
            }

            // Normalize to prevent faster diagonal movement
            return moveDirection.normalized;
        }

        [Button("Move to focus object")]
        public void MoveToFocusObject()
        {
            if (Selection.CurrentFocus == null) return;
            StartCoroutine(MoveToTargetCoroutine(Selection.CurrentFocus.transform.position, 0.5f));
        }
    
    

        /// <summary>
        /// move the camera to focus on an object
        /// </summary>
        /// <param name="obj"></param>
        public IEnumerator MoveToTargetCoroutine(Vector3 targetPosition, float duration)
        {
            float time = 0;
            Vector3 startPosition = transform.position;
            Vector3 endPosition = targetPosition.WithZ(-14);

            // float timeElapsed = 0;

            while (time < duration)
            {
                float t = time / duration;
                t = t * t * (3f - 2f * t);
                transform.position = Vector3.Lerp(startPosition, endPosition, t);
                time += Time.unscaledDeltaTime;

                yield return null;
            }


            //
            // while (time < duration)
            // {
            //     // Increment the time by the delta time each frame
            //     time += Time.unscaledTime;
            //     // Calculate the interpolation factor, ensuring it does not exceed 1
            //     float t = Mathf.Clamp(time / duration, 0, 1);
            //     // Apply damping to the interpolation factor
            //     // Use Mathf.SmoothStep for a simple damping effect
            //     t = Mathf.SmoothStep(0, 1, t);
            //     // Interpolate the object's position between the start and target positions using the damping factor
            //     transform.position = Vector3.Lerp(startPosition, targetPosition, t);
            //     // Wait until the next frame
            //     yield return null;
            // }
            // Ensure the object's position is exactly the target position at the end
            transform.position = endPosition;
        }
    }
}