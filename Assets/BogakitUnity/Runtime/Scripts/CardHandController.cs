﻿using System.Collections.Generic;
using BogakitUnity.Extensions;
using BogakitUnity.Selections;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BogakitUnity
{
    [ExecuteInEditMode]
    public class CardHandController : MonoBehaviour
    {
        [Required] [SerializeField]  private DragDropManager dragDropDelegate;

        [Tooltip("Y position of the card, origin is center Y.")] [Required] [SerializeField]
        private CurveParameters curve;

        [Tooltip("Overlay camera that is rendering the cards. Used for raycasting mouse position")]
        [Required]
        [SerializeField]
        private Camera cam;

        [Header("Settings")]
        [Tooltip("Force cards to face upwards when selected, for easier readability.")]
        [Foldout("settings")]
        [SerializeField]
        private bool cardUprightWhenSelected = true;

        [SerializeField] [Tooltip("card tilt when dragged, based on the mouse velocity")] [Foldout("settings")]
        private bool cardTilt = true;

        [Tooltip("if true, the cards can be swapped inside  inside the hand with the mouse")]
        [SerializeField]
        [Foldout("settings")]
        private bool allowSwap;

        
        [SerializeField]
        [Foldout("settings")]
        private bool cancelOnReenter;

        [SerializeField] [Foldout("settings")] private bool clearOnStart;

        //[SerializeField] [Foldout("settings")] private bool updateHierarchyOrder = true;

        [Tooltip("Controls the area which is considered 'in-hand', allowing cards to be selected/reordered. " +
                 "If a card leaves this area it can be used upon releasing the mouse button.")]
        [SerializeField]
        [Foldout("settings")]
        private Vector2 handSize = new Vector2(10, 2f);

        [SerializeField] [Foldout("settings")] private float hoverYOffset;
        [SerializeField] [Foldout("settings")] private float speed;
        [SerializeField] [Foldout("settings")] private float cardHeldScale = 0.9f;
        [SerializeField] [Foldout("settings")] private bool showDebugGizmos = true;

        [Range(0, 5)] [Tooltip("spacing nearby the selected card.")] [SerializeField] [Foldout("settings")]
        private float selectionSpacing = 1;

        [SerializeField] [Foldout("settings")] private UnityEvent<GameObject> onSelect;
        
        private Plane plane; // world XY plane, used for mouse position raycasts
        //private List<GameObject> cards = new List<GameObject>(); // Cards currently in hand

        // Card index that is nearest to mouse
        [SerializeField] [Foldout("debug")] private int nearestCardIndexToMouse = -1;

        // Card index that is held by mouse (inside of hand)
        [SerializeField] [Foldout("debug")] private int draggedCardIndex = -1;


        [SerializeField] [Foldout("debug")]
        private GameObject heldCard; // Card that is held by mouse (when outside of hand)

        [SerializeField] [Foldout("debug")] private Vector3 mouseWorldPos;
        [SerializeField] [Foldout("debug")] private bool isMouseInsideHand;
        [SerializeField] [Foldout("debug")] private GameObject nearestCard;
        private Vector2 prevMousePos;
        private Vector2 mousePosDelta;
        private Rect handBounds;
        private Vector2 heldCardTilt;
        private Vector2 force;
        private Vector2 mousePosClamped;
        private float sqrDistanceMouseToClosestCard;
        public bool IsDraggingCard => heldCard || draggedCardIndex > -1;

        private void Awake()
        {
            // dragDropDelegate = FindObjectOfType<DragDropManager>();
            Assert.IsNotNull(dragDropDelegate, "missing dragDrop delegate from BaseUI");
            Assert.IsNotNull(cam, "missing main camera from BaseUI");
            plane = new Plane(-Vector3.forward, transform.position);
            prevMousePos = Input.mousePosition;
            handBounds = new Rect((-handSize / 2), handSize);
            if (clearOnStart) ClearHand();
           
        }

        private void ClearHand()
        {
            while (transform.childCount > 0)
            {
                DestroyImmediate(transform.GetChild(0));
            }

            //transform.Clear();
        }

        // [Tooltip("Add transform children to hand")]
        // [Button("populate")]
        // internal void PopulateHand()
        // {
        //     // Add transform children to hand
        //     int count = transform.childCount;
        //     for (int i = 0; i < count; i++)
        //     {
        //         Transform cardTransform = transform.GetChild(i);
        //         cards.Add(cardTransform.gameObject);
        //     }
        // }

        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.blue;

            if (isMouseInsideHand)
            {
                Gizmos.color = Color.red;
            }

            //Gizmos.DrawCube(position, Vector3.one * 0.1f);
            Gizmos.DrawWireCube(transform.position, handSize);
        }

        private void Update()
        {
            UpdateMouseWorldPosition();
            sqrDistanceMouseToClosestCard = SqrDistanceMouseToClosestCard();
            UpdateCardsPosition();


            if (ShouldSelectDraggedCard())
            {
                SelectCard(transform.GetChild(draggedCardIndex).gameObject);
            }

            SwapDraggedCardPositionInHand();
            HandleHelCard();
            if (!Input.GetMouseButton(0))
            {
                // Stop dragging
                draggedCardIndex = -1;
            }
        }

        private bool ShouldSelectDraggedCard()
        {
            return heldCard == null && draggedCardIndex != -1 && Input.GetMouseButton(0) && !isMouseInsideHand;
        }

        /// <summary>
        /// swap dragged card indixe, but only if the mouse is inside the hand
        /// </summary>
        private void SwapDraggedCardPositionInHand()
        {
            if (!allowSwap) return;
            if (heldCard == null && Input.GetMouseButton(0) && draggedCardIndex != -1 && nearestCardIndexToMouse != -1)
            {
                // Move dragged card
                MoveCardToIndex(draggedCardIndex, nearestCardIndexToMouse);
            }
        }

        private void HandleHelCard()
        {
            if (heldCard == null) return;

            if (isMouseInsideHand  && cancelOnReenter)
            {
                if (allowSwap) MoveCardToIndex(draggedCardIndex, nearestCardIndexToMouse);
                else MoveBackSelectedCard(); 
                return;
            }
            
            if (Input.GetMouseButtonUp(0))
            {
                if (isMouseInsideHand)
                {
                  
                    if (allowSwap) MoveCardToIndex(draggedCardIndex, nearestCardIndexToMouse);
                    else MoveBackSelectedCard();
                }
                else
                {
                    DropCard();
                }

                return;
            }

            UpdateHeldCard(Input.GetMouseButton(0), mousePosClamped);
        }

        private void UpdateMouseWorldPosition()
        {
            mousePosClamped = ClampedMousePosition();
            // Get world position from mouse
            Ray ray = cam.ScreenPointToRay(mousePosClamped);
            if (plane.Raycast(ray, out float enter))
            {
                mouseWorldPos = ray.GetPoint(enter);
            }

            isMouseInsideHand =
                handBounds.Contains(transform.InverseTransformPoint(mouseWorldPos) - transform.position);
        }

        private Vector2 ClampedMousePosition()
        {
            Vector2 mousePos = Input.mousePosition;

            // Allows mouse to go outside game window but keeps cards within window
            // If mouse doesn't need to go outside, could use "Cursor.lockState = CursorLockMode.Confined;" instead
            mousePos.x = Mathf.Clamp(mousePos.x, 0, Screen.width);
            mousePos.y = Mathf.Clamp(mousePos.y, 0, Screen.height);
            return mousePos;
        }

        private void DropCard()
        {
            if (dragDropDelegate.CanDropOn(heldCard, Selection.CurrentFocus))
            {
                dragDropDelegate.DidDropOn(heldCard, Selection.CurrentFocus);
            }
            else
            {
                if (allowSwap) MoveCardToIndex(draggedCardIndex, nearestCardIndexToMouse);
                else MoveBackSelectedCard();
                //MoveCardBack();
            }

            EventSystem.current.SetSelectedGameObject(null);
            Selection.Deselect();
            heldCard = null;
        }

        private void UpdateHeldCard(bool mouseButtonDown, Vector2 mousePos)
        {
            if (cardTilt) UpdateCardTilt(mousePos);

            Transform cardTransform = heldCard.transform;
            Vector3 cardUp = Vector3.up;
            Vector3 cardPos = Vector3.MoveTowards(cardTransform.position, mouseWorldPos, speed * Time.deltaTime);
            ; // + heldCardOffset; mouseWorldPos + heldCardOffset;
            Vector3 cardForward = Vector3.forward;
            if (cardTilt && mouseButtonDown)
            {
                cardForward -= new Vector3(heldCardTilt.x, heldCardTilt.y, 0);
            }

            // Bring card to front
            cardPos.z = transform.position.z - 0.5f;

            // Handle Position & Rotation
            cardTransform.rotation = Quaternion.RotateTowards(cardTransform.rotation,
                Quaternion.LookRotation(cardForward, cardUp), 80f * Time.deltaTime);
            cardTransform.position = cardPos;
        }

        private void MoveBackSelectedCard()
        {
            Debug.Log($"MoveBackSelectedCard to {heldCard} at {draggedCardIndex}");
            // Card has gone back into hand
            AddCardAtIndex(heldCard, draggedCardIndex);
            draggedCardIndex = nearestCardIndexToMouse;
            nearestCardIndexToMouse = -1;
            heldCard.transform.DOScale(Vector3.one, .2f);
            heldCard = null;
        }

        private void SelectCard(GameObject card)
        {
            // Card is outside the hand, so is considered "held" ready to be used
            // Remove from hand, so that cards in hand fill the hole that the card left
            heldCard = card;
            heldCard.transform.DOScale(Vector3.one * cardHeldScale, .2f);

            RemoveCardAtIndex(draggedCardIndex);
            // count--;
            //keep value for move back
            //draggedCardIndex = -1;
            onSelect?.Invoke(heldCard);
            Selection.Select(heldCard);
            EventSystem.current.SetSelectedGameObject(heldCard);
        }

        private void MoveCardBack()
        {
            heldCard.transform.DOScale(Vector3.one, .2f);
            AddCardAtIndex(heldCard, nearestCardIndexToMouse);
        }

        private void UpdateCardTilt(Vector2 mousePos)
        {
            // Mouse movement velocity

            mousePosDelta = (mousePos - prevMousePos) * new Vector2(1600f / Screen.width, 900f / Screen.height) *
                            Time.deltaTime;
            prevMousePos = mousePos;

            float tiltStrength = 3f;
            float tiltDrag = 3f;
            float tiltSpeed = 50f;

            force += (mousePosDelta * tiltStrength - heldCardTilt) * Time.deltaTime;
            force *= 1 - tiltDrag * Time.deltaTime;
            heldCardTilt += force * Time.deltaTime * tiltSpeed;
            // these calculations probably aren't correct, but hey, they work...? :P

            if (showDebugGizmos)
            {
                Debug.DrawRay(mouseWorldPos, mousePosDelta, Color.red);
                Debug.DrawRay(mouseWorldPos, heldCardTilt, Color.cyan);
            }
        }

        private float YPositioningAtValue(float t)
        {
            return (curve.positioning.Evaluate(t) * curve.positioningInfluence) * (transform.childCount - 1);
        }


        private float HandRotation(float t)
        {
            return (curve.rotation.Evaluate(t) * curve.rotationInfluence) * (transform.childCount - 1);
        }


        private void UpdateCardsPosition()
        {
            // Get distance to current selected card (for comparing against other cards later, to find closest)
           // int count = cards.Count; //transform.childCount;
            
            for (int i = 0; i < transform.childCount; i++)
            {
                GameObject card = transform.GetChild(i).gameObject;
                Transform cardTransform = card.transform;

                bool noCardHeld = (heldCard == null); // Whether a card is "held" (outside of hand)

                bool isDraggedCard = noCardHeld && draggedCardIndex == i;

                card.UpdateSortingOrder(i);


                var cardPositions = GetCardPosition(i);
                var cardPos = cardPositions.Item1;
                var normalizedCardPosition = cardPositions.Item2;
                bool isHovered = IsHovered(cardPos, i);

                // Handle Start Dragging
                if (isHovered && Input.GetMouseButtonDown(0) && dragDropDelegate.CanDrag(card)) draggedCardIndex = i;

                float angle = Mathf.LerpAngle(cardTransform.eulerAngles.z, HandRotation(normalizedCardPosition),
                    speed / 2 * Time.deltaTime);

                // Specific for active card
                if (isHovered || isDraggedCard)
                {
                    // When selected bring card to front
                    if (cardUprightWhenSelected) angle = 0; // cardUp = Vector3.up;
                    cardPos.z = transform.position.z;
                    cardPos.y += hoverYOffset;
                    card.UpdateSortingOrder(99);
                }

                cardTransform.localEulerAngles = new Vector3(0, 0, angle);
                
                // Handle Card Position
                // Held by mouse / dragging
                cardPos = Vector3.MoveTowards(cardTransform.position,
                    isDraggedCard ? mouseWorldPos : cardPos, speed * Time.deltaTime);
                cardTransform.position = cardPos;

                ShowDebugGizmos(i, cardPos);
            }
        }

        /// <summary>
        /// We do not use collision to determine if card is hovered
        /// Collision is unstable for overlapping objects holding juicy animations
        /// We use pointer proximity and min distance to center
        /// This allow to be agnostic of object type in Hand
        /// </summary>
        /// <param name="cardPos"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        private bool IsHovered(Vector3 cardPos, int i)
        {
            float distanceMouseToClosestCard = (cardPos - mouseWorldPos).sqrMagnitude;
            bool isCloseEnoughToMouse = distanceMouseToClosestCard < 1.5f;
            bool isClosestCardToMouse = nearestCardIndexToMouse == i;
            bool isHovered = isClosestCardToMouse && isCloseEnoughToMouse && isMouseInsideHand && heldCard == null &&
                             draggedCardIndex == -1;

            if (distanceMouseToClosestCard < sqrDistanceMouseToClosestCard)
            {
                sqrDistanceMouseToClosestCard = distanceMouseToClosestCard;
                nearestCardIndexToMouse = i;
            }

            return isHovered;
        }

        private (Vector3, float) GetCardPosition(int i)
        {
            float selectOffset = 0;
            if (heldCard == null && isMouseInsideHand)
            {
                selectOffset = 0.02f * Mathf.Clamp01(1 - Mathf.Abs(Mathf.Abs(i - nearestCardIndexToMouse) - 1) /
                    (float)transform.childCount *
                    3) * Mathf.Sign(i - nearestCardIndexToMouse);
            }

            float normalizedCardPosition = (i + 0.5f) / transform.childCount + selectOffset * selectionSpacing;
            Vector3 cardPos = transform.position + new Vector3(
                transform.position.x + (normalizedCardPosition - 0.5f) * handSize.x,
                YPositioningAtValue(normalizedCardPosition) + transform.position.y,
                0);


            return (cardPos, normalizedCardPosition);
        }

        // private void UpdateSortingOrder(GameObject card, int i)
        // {
        //     if (card.GetComponentInChildren<Canvas>()) card.GetComponentInChildren<Canvas>().sortingOrder = i;
        //     if (card.GetComponentInChildren<SortingGroup>())
        //         card.GetComponentInChildren<SortingGroup>().sortingOrder = i;
        // }

        private float SqrDistanceMouseToClosestCard()
        {
            float distanceMouseToClosestCard = 10000;
            if (nearestCardIndexToMouse >= 0 && nearestCardIndexToMouse < transform.childCount)
            {
                float t = (nearestCardIndexToMouse + 0.5f) / transform.childCount;
                Vector3 p =
                    new Vector3(t * handSize.x - handSize.x / 2, YPositioningAtValue(t) - handSize.y / 2, 0) +
                    transform.position;
                distanceMouseToClosestCard = (p - mouseWorldPos).sqrMagnitude;
            }

            return distanceMouseToClosestCard;
        }

        private void ShowDebugGizmos(int index, Vector3 position)
        {
            // Debug Gizmos
            if (!showDebugGizmos) return;
            Debug.DrawLine(position, mouseWorldPos, nearestCardIndexToMouse == index ? Color.blue : Color.red);
        }

        private void OnValidate()
        {
           Update();
        }

        /// <summary>
        /// Moves the card in hand from the currentIndex to the toIndex. If you want to move a card that isn't in hand, use AddCardToHand
        /// </summary>
        private void MoveCardToIndex(int currentIndex, int toIndex)
        {
            if (currentIndex == toIndex) return; // Same index, do nothing
            Transform card = transform.GetChild(currentIndex);
            
            card.SetSiblingIndex(toIndex);
            draggedCardIndex = nearestCardIndexToMouse;
        }

        /// <summary>
        /// Adds a card to the hand. Optional param to insert it at a given index.
        /// </summary>
        /// 
        private void AddCardAtIndex(GameObject card, int index)
        {
            
            if (index < 0)
            {
                // Add to end
                index = transform.childCount-1;
            }
            card.transform.SetParent(transform);
            card.transform.SetSiblingIndex(index);
        }

        public void AddCard(GameObject card)
        {
            AddCardAtIndex(card, -1);
        }


        /// <summary>
        /// Remove the card at the specified index from the hand.
        /// </summary>
        private bool RemoveCardAtIndex(int index)
        {
            if (index < 0 || index >= transform.childCount)
                return false;
            Transform card = transform.GetChild(index);
            //TODO: to null or parent? 
            card.SetParent(null);
            card.SetSiblingIndex(transform.GetSiblingIndex() + 1);

            return true;
        }
        
      
    }
}