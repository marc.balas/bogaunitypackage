﻿using UnityEngine;

//https://gamedev.stackexchange.com/questions/137265/how-to-make-card-movement-behave-like-those-in-hearthstone-and-eternal-ccg

namespace BogakitUnity.Anim
{
    
   
    public class DragRotator : MonoBehaviour
    {
        private const float EPSILON = 0.0001f;
     //   private const float SMOOTH_DAMP_SEC_FUDGE = 0.1f;
        
        private DragRotatorInfo m_info => settings.DragRotatorInfo;
        private float pitchVelocity;
        private float pitchInDegree;
        
        private float rollVelocity;
        private float rollInDegree;
        
        private Vector3 previousPosition;
        private Vector3 originalAngles;
        
        [SerializeField] private DragRotatorSettings settings;
        private void Awake()
        {
            Reset();
            previousPosition = transform.position;
            originalAngles = transform.localRotation.eulerAngles;
            
        }

        private void OnEnable()
        {
            Reset();
        }

        private void Update()
        {
            Vector3 position = transform.position;
            Vector3 vector3 = position - previousPosition;
            if (vector3.sqrMagnitude > EPSILON)
            {
                pitchInDegree += vector3.y * m_info.PitchSettings.Multiplicator;
                pitchInDegree = Mathf.Clamp(pitchInDegree, m_info.PitchSettings.MinDegrees, m_info.PitchSettings.MaxDegrees);
                rollInDegree -= vector3.x * m_info.RollSettings.Multiplicator;
                rollInDegree = Mathf.Clamp(rollInDegree, m_info.RollSettings.MinDegrees, m_info.RollSettings.MaxDegrees);
            }
            pitchInDegree = Mathf.SmoothDamp(pitchInDegree, 0.0f, ref pitchVelocity, m_info.PitchSettings.RestingTime * 0.1f);
            rollInDegree = Mathf.SmoothDamp(rollInDegree, 0.0f, ref rollVelocity, m_info.RollSettings.RestingTime * 0.1f);
            transform.localRotation = Quaternion.Euler(originalAngles.x + pitchInDegree, originalAngles.y + rollInDegree, originalAngles.z);
            previousPosition = position;
        }


        public void Reset()
        {
            previousPosition = transform.position;
            transform.localRotation = Quaternion.Euler(originalAngles);
            rollInDegree = 0.0f;
            rollVelocity = 0.0f;
            pitchInDegree = 0.0f;
            pitchVelocity = 0.0f;
        }
    }
}