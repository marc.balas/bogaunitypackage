﻿using System;
using UnityEngine;

namespace BogakitUnity.Anim
{
    [CreateAssetMenu(fileName = "DragRotatorSettings", menuName = "BogaKit/Settings/dragRotator")]
    public class DragRotatorSettings : ScriptableObject
    {
        [SerializeField] private DragRotatorInfo dragRotatorInfo;
        public DragRotatorInfo DragRotatorInfo => dragRotatorInfo;
    }

    [Serializable]
    public class DragRotatorInfo
    {
        public DragRotatorAxisSettings PitchSettings;
        public DragRotatorAxisSettings RollSettings;
    }

    [Serializable]
    public class DragRotatorAxisSettings
    {
        public float Multiplicator = 15f;
        public float MinDegrees = -45f;
        public float MaxDegrees = 45f;
        public float RestingTime = 1f;
    }
}