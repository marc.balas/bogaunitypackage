﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

namespace BogakitUnity.Anim
{
    public class TiltDragging : MonoBehaviour
    {
        [SerializeField]
        private Transform tiltTarget;

        private float EPSILON = 0.01f;

        [FormerlySerializedAs("m_info")]
        [SerializeField]
        private DragRotatorInfo mInfo;

        private float m_pitchDeg;
        private float m_rollDeg;
        private float m_pitchVel;
        private float m_rollVel;
        private Vector3 m_originalAngles;

        private float m_targetPitchDeg;
        private float m_targetRollDeg;

        [SerializeField]
        private DragRotatorSettings settings;

        private Vector3 _pointerDisplacement;
        private bool hover;
        private Camera mainCamera;


        private void OnMouseEnter()
        {
            hover = true;
        }


        private void OnMouseExit()
        {
            hover = false;
        }


        private void Awake()
        {
            mainCamera = Camera.main;
            Assert.IsNotNull(tiltTarget);
            Reset();

            m_originalAngles = tiltTarget.localRotation.eulerAngles;
            mInfo = settings.DragRotatorInfo;
        }

        private void OnEnable()
        {
            Reset();
        }

        private Vector3 MouseInWorldCoords()
        {
            var screenMousePos = Input.mousePosition;
            //Debug.Log(screenMousePos);
            screenMousePos.z = transform.position.z - mainCamera.transform.position.z;
            return mainCamera.ScreenToWorldPoint(screenMousePos);
        }

        private void Update()
        {
            if (!tiltTarget)
                return;

            //distance to center
            _pointerDisplacement = hover ? MouseInWorldCoords() - transform.position : Vector3.zero;
            _pointerDisplacement.z = 0;
            //Debug.Log("displacement : " + _pointerDisplacement + " magnitude " + _pointerDisplacement.sqrMagnitude);

            float zDisp = transform.position.z - mainCamera.transform.position.z;
            //            Debug.Log("delta displacement :" + delta);
            if (_pointerDisplacement.magnitude > EPSILON)
            {
                float deltaPitch = _pointerDisplacement.y * zDisp * 0.1f * mInfo.PitchSettings.Multiplicator;
                float deltaRoll = _pointerDisplacement.x * zDisp * 0.1f * mInfo.RollSettings.Multiplicator;
                //Debug.Log("deltaPitch : " + deltaPitch + " deltaRoll " + deltaRoll);

                m_targetPitchDeg = deltaPitch;
                m_targetPitchDeg = Mathf.Clamp(m_targetPitchDeg, mInfo.PitchSettings.MinDegrees,
                    mInfo.PitchSettings.MaxDegrees);

                m_targetRollDeg = -deltaRoll;
                m_targetRollDeg = Mathf.Clamp(m_targetRollDeg, mInfo.RollSettings.MinDegrees,
                    mInfo.RollSettings.MaxDegrees);

                m_pitchDeg = Mathf.SmoothDamp(m_pitchDeg, m_targetPitchDeg, ref m_pitchVel,
                    mInfo.PitchSettings.RestingTime * 0.1f);
                m_rollDeg = Mathf.SmoothDamp(m_rollDeg, m_targetRollDeg, ref m_rollVel,
                    mInfo.RollSettings.RestingTime * 0.1f);
            }

            if (!hover)
            {
                m_pitchDeg = Mathf.SmoothDamp(m_pitchDeg, 0.0f, ref m_pitchVel, mInfo.PitchSettings.RestingTime * 0.1f);
                m_rollDeg = Mathf.SmoothDamp(m_rollDeg, 0.0f, ref m_rollVel, mInfo.RollSettings.RestingTime * 0.1f);
            }

            tiltTarget.localRotation = Quaternion.Euler(m_originalAngles.x + m_pitchDeg, m_originalAngles.y + m_rollDeg,
                m_originalAngles.z);
        }

        public DragRotatorInfo GetInfo()
        {
            return mInfo;
        }

        public void SetInfo(DragRotatorInfo info)
        {
            mInfo = info;
        }

        public void Reset()
        {
            tiltTarget.localRotation = Quaternion.Euler(m_originalAngles);
            m_rollDeg = 0.0f;
            m_rollVel = 0.0f;
            m_pitchDeg = 0.0f;
            m_pitchVel = 0.0f;
        }
    }
}