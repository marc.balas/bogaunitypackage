﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Boga.Core;
using BogakitUnity.Layouts;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace BogakitUnity.Extensions
{
    public static class UnityExtension
    {
        public static void SetAlpha(this SpriteRenderer sr, float alpha)
        {
            var newColor = sr.color;
            newColor.a = 0.0f;
            sr.color = newColor;
        }

        public static void SetChildActive(this GameObject gameObject, string childName, bool active)
        {
            Transform child = gameObject.transform.Find(childName);
            //if (child == null) Debug.Log($"{gameObject.name} has no child {childName}");
            if (child) child.gameObject.SetActive(active);
        }

        public static void UpdateSortingOrder(this GameObject go, int order, string sortingLayer = "")
        {
            SortingGroup sortingGroup = go.GetComponentInChildren<SortingGroup>();
            if (sortingGroup)
            {
                sortingGroup.sortingOrder = order;
                if (sortingLayer.Exist()) sortingGroup.sortingLayerName = sortingLayer;
            }

            var canvas = go.GetComponentInChildren<Canvas>();
            if (canvas)
            {
                canvas.sortingOrder = order;
                if (sortingLayer.Exist()) canvas.sortingLayerName = sortingLayer;
            }
        }

        public static void Deactivate(this GameObject gameObject, string childName)
        {
            gameObject.SetChildActive(childName, false);
        }


        public static void Activate(this GameObject gameObject, string childName)
        {
            gameObject.SetChildActive(childName, true);
        }

        public static void DeactivateAllChildren(this GameObject gameObject)
        {
            gameObject.SetAllChildrenActive(false);
        }

        public static void ActivateAllCChildren(this GameObject gameObject)
        {
            gameObject.SetAllChildrenActive(true);
        }

        public static GameObject CopyTransform(this GameObject gameObject, Transform toCopy)
        {
            gameObject.transform.position = toCopy.position;
            gameObject.transform.rotation = toCopy.rotation;
            gameObject.transform.localScale = toCopy.localScale;
            gameObject.transform.parent = toCopy.parent;
            return gameObject;
        }

        public static void SetAllChildrenActive(this GameObject gameObject, bool active)
        {
            foreach (var child in gameObject.transform.GetChildren())
            {
                child.gameObject.SetActive(active);
            }
        }

        public static string ColorMarkdown(this string textToColor, string markdown, Color color)
        {
            string newText = textToColor;
            //   MatchCollection matches = Regex.Matches (textToColor, @"%%\w+");

            string pattern = markdown + @"\w+";
            //Regex regex = new Regex(@"%%\w+");
            Regex regex = new Regex(pattern);
            newText = regex.Replace(newText,
                m => "<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">" +
                     m.Value.Substring(markdown.Length, m.Value.Length - markdown.Length) + "</color>");
            return newText;
        }

        public static string ColorTo(this string textToColor, Color color)
        {
            return "<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">" + textToColor + "</color>";
        }

        public static string StrikeThrough(this string text)
        {
            return "<s>" + text + "</s>";
        }

        private static string SystemColorToHtmlStringRGB(this System.Drawing.Color color)
        {
            Color32 color32 = new Color32(
                (byte)Mathf.Clamp(Mathf.RoundToInt(color.R * (float)byte.MaxValue), 0, (int)byte.MaxValue),
                (byte)Mathf.Clamp(Mathf.RoundToInt(color.G * (float)byte.MaxValue), 0, (int)byte.MaxValue),
                (byte)Mathf.Clamp(Mathf.RoundToInt(color.B * (float)byte.MaxValue), 0, (int)byte.MaxValue), (byte)1);
            return $"{color32.r:X2}{color32.g:X2}{color32.b:X2}";
        }

        public static Vector3 Abs(this Vector3 vector3)
        {
            return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
        }

        public static Vector3 ClampVector3(this Vector3 value, Vector3 min, Vector3 max)
        {
            return new Vector3(
                Mathf.Clamp(value.x, min.x, max.x),
                Mathf.Clamp(value.y, min.y, max.y),
                Mathf.Clamp(value.z, min.z, max.z)
            );
        }

        public static Vector3 GetSize(this GameObject gameObject)
        {
            if (gameObject.GetComponent<RectTransform>())
            {
                return gameObject.GetComponent<RectTransform>().sizeDelta;
            }

            if (gameObject.transform.GetComponent<Renderer>())
            {
                return gameObject.GetComponent<Renderer>().bounds.size;
            }

            if (gameObject.GetComponent<Collider>())
            {
                return gameObject.GetComponent<Collider>().bounds.size;
            }

            if (gameObject.GetComponent<Collider2D>())
            {
                return gameObject.GetComponent<Collider2D>().bounds.size;
            }

            return Vector3.zero;
        }

        public static void SetText(this GameObject gameObject, string text)
        {
            TextMeshProUGUI textMeshProUGUI = gameObject.GetComponent<TextMeshProUGUI>();
            TextMeshPro textMeshPro = gameObject.GetComponent<TextMeshPro>();

            if (textMeshProUGUI != null)
            {
                textMeshProUGUI.text = text;
            }

            if (textMeshPro != null)
            {
                textMeshPro.text = text;
            }
        }

        public static void SetToClosestIndexOfLayout(this Transform transform, Layout layout)
        {
            int closestIndex = layout.ClosestIndexFromPosition(transform.position);
            transform.SetSiblingIndex(closestIndex);
        }

        public static void ShuffleChildrenOrder(this Transform parent)
        {
            for (int i = 0; i < parent.childCount; i++)
            {
                // Random index between i and the last child
                int randomIndex = Random.Range(i, parent.childCount);

                // Swap positions in the hierarchy
                parent.GetChild(i).SetSiblingIndex(randomIndex);

                // If the randomIndex is the same as i, it could skip a child, 
                // so we ensure the current child is also moved to a new position if needed.
                if (randomIndex != i)
                {
                    parent.GetChild(randomIndex).SetSiblingIndex(i);
                }
            }
        }


        public static Vector3 GetClosestGridPosition(this Transform transform, float gridSize)
        {
            float x = Mathf.Round(transform.position.x / gridSize) * gridSize;
            float y = Mathf.Round(transform.position.y / gridSize) * gridSize;
            float z = Mathf.Round(transform.position.z / gridSize) * gridSize;

            return new Vector3(x, y, z);
        }

        public static void ShowText(this MonoBehaviour behaviour, GameObject go, TextMeshProUGUI textUI, string message,
            float duration = -1)
        {
            textUI.text = message;
            go.SetActive(true);
            if (duration > 0) behaviour.StartCoroutine(behaviour.HideCoroutine(go, duration));
        }

        public static void ShowText(this TextMeshProUGUI textUI, string message,
            float duration = -1)
        {
            textUI.text = message;
            if (duration > 0) textUI.StartCoroutine(textUI.HideTextCoroutine(duration));
        }

        public static IEnumerator HideTextCoroutine(this TextMeshProUGUI text, float duration = 1)
        {
            yield return new WaitForSeconds(duration);
            text.text = "";
        }

        public static IEnumerator HideCoroutine(this MonoBehaviour behaviour, GameObject go, float duration = 1)
        {
            yield return new WaitForSeconds(duration);
            go.SetActive(false);
        }

        //Breadth-first search
        public static Transform FindDeepChild(this Transform aParent, string aName)
        {
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(aParent);
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (c.name == aName)
                    return c;
                foreach (Transform t in c)
                    queue.Enqueue(t);
            }

            return null;
        }

        public static Transform SetActive(this Transform transform, string name, bool active = true)
        {
            if (string.IsNullOrEmpty(name))
                return null;
            Transform child = transform.FindDeepChild(name);
            if (child)
            {
                child.gameObject.SetActive(active);
                return child;
            }

            Debug.LogWarning($"child named {name} not found in {transform.name}");

            return null;
        }

        public static void ToggleSprite(this Transform transform, string name)
        {
            Transform child = transform.Find(name);
            if (!child) return;
            SpriteRenderer sr = child.GetComponent<SpriteRenderer>();
            if (sr)
            {
                sr.enabled = !sr.enabled;
            }
        }

        public static void Toggle(this Image image)
        {
            image.enabled = !image.enabled;
        }

        public static Transform Show(this Transform transform, string childName)
        {
            return transform.SetActive(childName);
        }

        public static Transform Hide(this Transform transform, string childName)
        {
            return transform.SetActive(childName, false);
        }

        public static Transform Clear(this Transform transform)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Transform child = transform.GetChild(i);
                if (child.gameObject.activeSelf)
                {
                    //      GameObject.Destroy(child.gameObject);
#if UNITY_EDITOR
                    Object.DestroyImmediate(child.gameObject);
#else
            GameObject.Destroy(child.gameObject);
#endif
                }
            }

            return transform;
        }

        public static void DestroyAll(this List<GameObject> list)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                Object.DestroyImmediate(list.ElementAt(i));
            }
        }

        public static List<GameObject> GetChildren(this Transform transform)
        {
            List<GameObject> children = new List<GameObject>();
            for (int i = 0; i < transform.childCount; i++)
            {
                children.Add(transform.GetChild(i).gameObject);
            }

            return children;
        }
    }
}