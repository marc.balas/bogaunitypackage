using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace BogakitUnity
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        public UnityEvent<bool> togglePause;
        private bool isGamePaused = false;

        void Awake()
        {
            // Implementing a simple Singleton pattern
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject); // Optional: Keep the GameManager across scenes
            }
            else
            {
                Destroy(gameObject);
            }
            //PauseGame();
        }

        [Button("Toggle Pause")]
        // Pauses the game by setting time scale to 0
        public void TogglePauseGame()
        {
            if(isGamePaused) ResumeGame(); 
            else PauseGame();
            
        }

        [Button("Pause game")]
        // Pauses the game by setting time scale to 0
        public void PauseGame()
        {
            isGamePaused = true;
            Time.timeScale = 0f;
            togglePause?.Invoke(true);
            Debug.Log("Game Paused");
        }

        [Button("Resume game")]
        // Resumes the game by setting time scale back to 1
        public void ResumeGame()
        {
            isGamePaused = false;
            Time.timeScale = 1f;
            togglePause?.Invoke(false);
            Debug.Log("Game Resumed");
        }

        // Exits the game. Note: This will only work in a built game, not in the Unity Editor
        public void ExitGame()
        {
            Application.Quit();
            Debug.Log("Exit Game");
        }

        // Loads a new scene by its name
        public void LoadScene(string sceneName)
        {
            // Ensure the game is not paused when loading a new scene
            if (isGamePaused)
            {
                ResumeGame();
            }

            SceneManager.LoadScene(sceneName);
            Debug.Log($"Loading scene: {sceneName}");
        }

        // Example usage: Load the next scene in the build settings order
        public void LoadNextScene()
        {
            // Ensure the game is not paused when loading a new scene
            if (isGamePaused)
            {
                ResumeGame();
            }

            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
            int nextSceneIndex = (currentSceneIndex + 1) % SceneManager.sceneCountInBuildSettings;
            SceneManager.LoadScene(nextSceneIndex);
            Debug.Log($"Loading next scene: {nextSceneIndex}");
        }
    }
}