using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace BogakitUnity
{
    public class InputManager : MonoBehaviour
    {

        public static InputManager Instance;
        public float cooldownTime = 0.15f; // Time in seconds between allowed actions

        private float nextActionTime = 0f;
        private Vector2 NavigationInput { get; set; }

        private InputAction _navigationAction;
        [Required]
        [SerializeField]
        public GameObject firstElement;
        [SerializeField]public List<GameObject> elements;
        private static PlayerInput PlayerInput { get; set; }
        private GameObject CurrentSelection => EventSystem.current.currentSelectedGameObject;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            PlayerInput = GetComponent<PlayerInput>();
            _navigationAction = PlayerInput.actions["Navigate"];
            EventSystem.current.SetSelectedGameObject(firstElement);
        }

        // Update is called once per frame
        void Update()
        {
            NavigationInput = _navigationAction.ReadValue<Vector2>();
            
            //move left and right selection
            if (NavigationInput.x > 0)
            {
                HandleNextElementSelection(1);
            }
            if (NavigationInput.x < 0)
            {
                HandleNextElementSelection(-1);
            }
            //   if( NavigationInput.y > 0)  HandleNextCardSelection(1);
            // if( NavigationInput.y < 0)  HandleNextCardSelection(-1);
            
        }
        
        private void HandleNextElementSelection(int addition)
        {
            if (Time.time < nextActionTime)return;
            nextActionTime = Time.time + cooldownTime;
            
            if (CurrentSelection != null)
            {
                EventSystem.current.SetSelectedGameObject(addition > 0 ? elements.After(CurrentSelection) : elements.Before(CurrentSelection));
               // Debug.Log($" selection is  {EventSystem.current.currentSelectedGameObject.name}");
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(elements.FirstOrDefault());
            }
        
        }
    }
}
