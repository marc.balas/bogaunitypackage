﻿using Boga.Core;
using Boga.Kit;
using BogakitUnity.EntityViews.Abstract;
using BogakitUnity.Selections;
using UnityEngine;

namespace BogakitUnity.Interactions.Abstract
{
    public abstract class DelayedElementInteraction : ElementInteraction
    {
        [SerializeField]
        private float loadingTime;
        [SerializeField]
        private float residualTime;
        private float enterTime;
        private float exitTime;

        protected override void OnFocusChanged(GameObject newObj, GameObject oldObj)
        {
            if(Log.level >= LogLevel.Debug)Debug.Log($"focus changed to {newObj}");
            if (Selection.CurrentSelection != null)
                return;
            ElementView ev = newObj ? newObj.GetComponent<ElementView>() : null;
            Element element = null;
            if (ev != null)
            {
                element = ev.element;
            }

            IEntity entity = sourceView != null ? sourceView.GetComponent<EntityView>().Entity : null;
            if (element != null && element != entity)
            {
                BeginWithSource(newObj);
                SourceElementChanged();
            }
            else if (element == null && sourceView != null)
            {
                sourceView = null;
                Stop();
            }
        }

        protected virtual void Update()
        {
            if (!activated && sourceView != null && Time.time - enterTime >= loadingTime)
            {
                activated = true;
                Activate();
            }
            else if (sourceView == null && activated && Time.time - exitTime >= residualTime)
            {
                activated = false;
                Deactivate();
            }
        }

        protected bool activated { get; private set; }

        public override void OnSelectionChanged(GameObject obj)
        {
            Deactivate();
        }

        private void Start()
        {
            Deactivate();
        }

        private void BeginWithSource(GameObject card)
        {
            sourceView = card;
            enterTime = Time.time;
        }

        protected abstract void Deactivate();

        protected abstract void Activate();
        protected abstract void SourceElementChanged();

        private void Stop()
        {
            exitTime = Time.time;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Deactivate();
        }
    }
}