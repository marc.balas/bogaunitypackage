using BogakitUnity.Selections;
using UnityEngine;

namespace BogakitUnity.Interactions.Abstract
{
  
    public abstract class ElementInteraction : MonoBehaviour
    {
        protected Camera mainCamera;
        
        public GameObject sourceView;
      
        
        protected virtual void Awake()
        {
            mainCamera = Camera.main;
        }

        protected virtual void OnEnable()
        {
            Selection.OnFocusChanged += OnFocusChanged;
            Selection.OnSelect += OnSelectionChanged;
            Selection.OnDeselect += OnSelectionDeselect;
        }

        protected virtual void OnSelectionDeselect(GameObject deselected)
        {
        }

        protected virtual void OnDisable()
        {
            Selection.OnFocusChanged -= OnFocusChanged;
            Selection.OnSelect -= OnSelectionChanged;
            Selection.OnDeselect -= OnSelectionDeselect;
        }

        protected abstract void OnFocusChanged(GameObject newObj, GameObject oldObj);

        public abstract void OnSelectionChanged(GameObject obj);


        #region Utils

        // protected Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
        // {
        //     Ray ray = mainCamera.ScreenPointToRay(screenPosition);
        //     Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        //     float distance;
        //     xy.Raycast(ray, out distance);
        //     return ray.GetPoint(distance);
        // }

        // returns mouse position in World coordinates for our GameObject to follow. 
        protected Vector3 MouseInWorldCoords(float z)
        {
            var screenMousePos = Input.mousePosition;
            //Debug.Log(screenMousePos);
            screenMousePos.z = z;
            return mainCamera.ScreenToWorldPoint(screenMousePos);
        }

        #endregion

        /*private Vector3 MouseInWorldCoords()
        {
            var screenMousePos = Input.mousePosition;
            //Debug.Log(screenMousePos);
            screenMousePos.z = transform.position.z - mainCamera.transform.position.z;
            return mainCamera.ScreenToWorldPoint(screenMousePos);
        }*/

        protected Vector3 GetMouseWorldPosition()
        {
            Plane plane = new Plane(-Vector3.forward, transform.position);
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out float enter))
            {
                return ray.GetPoint(enter);
            }

            return Vector3.zero;
        }
    }
}