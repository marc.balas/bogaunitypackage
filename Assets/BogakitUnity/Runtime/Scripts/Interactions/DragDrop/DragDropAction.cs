using System;
using System.Collections.Generic;
using Boga.Core;
using BogakitUnity.Extensions;
using BogakitUnity.Interactions.Abstract;
using BogakitUnity.Managers;
using BogakitUnity.Selections;
using DG.Tweening;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

namespace BogakitUnity
{
    public class DragDropAction : ElementInteraction, IDragDropAction
    {
        
        [FormerlySerializedAs("useTargeting")]
        [HorizontalLine(color: EColor.Green)]
        [SerializeField]
        protected bool _isTargeting = false;
        public bool isTargeting => _isTargeting;

        [Header("Drag settings")]
        [Foldout("Drag settings")]
        [SerializeField]
        private float validationDistance = 2f;

        [Foldout("Drag settings")]
        [SerializeField]
        private bool cancelWhenDistanceToSmall = true;

        [Foldout("Drag settings")]
        [SerializeField]
        private float dragAltitude = 2;

        [SerializeField]
        [Foldout("Drag settings")]
        [Min(0)]
        private float dragDamping = .15f;

        [Tooltip("altitude in Z, of the dragging plane")]
        [Foldout("Drag settings")]
        [SerializeField]
        private bool keepRotation;

        [SerializeField]
        [Foldout("Drag settings")]
        private bool cursorInCenter;

        
        [SerializeField]
        [NaughtyAttributes.Tag]
        public List<string> ignoreDragTagList;
        [SerializeField]
        [NaughtyAttributes.Tag]
        [Foldout("Drag settings")]
        public List<string> dropTags;

        [SerializeField]
        [Layer]
        public string draggingLayerName;

        [SerializeField]
        private KeyCode cancelKey = KeyCode.Escape;

        [Header("Targeting Arrow")]
        [SerializeField]
        private Sprite validTargetSprite;
        [SerializeField]
        private Sprite invalidTargetSprite;

        private IDragDropDelegate dragDropDelegate; // => dragDrop as IDragDropDelegate;

        [SerializeField]
        protected TargetingArrow TargetingArrow;

        [Header("debug")]
        [SerializeField]
        private bool debug;

        [Foldout("debug")]
        [SerializeField]
        private float dragDistance;

        [SerializeField]
        [Foldout("debug")]
        private float totalDraggedDistance;

        [SerializeField]
        [Foldout("debug")]
        private string oldLayerName;

        [SerializeField]
        [Foldout("debug")]
        protected Vector3 anchorDraggingPosition;

        public Vector3 sourcePosition => anchorDraggingPosition;

        [FormerlySerializedAs("_pointerDisplacement")]
        [SerializeField]
        [Foldout("debug")]
        private Vector3 pointerDisplacement;

        [SerializeField]
        private TextMeshProUGUI helpLabel;

        public static event Action<GameObject, GameObject> DidDropOnEvent;
        public static event Action<GameObject> BeginDragDropEvent;
        public static event Action<DragDropAction> EndDragDropOnEvent;

        public static DragDropAction CurrentDragDropAction;

        [Header("sound")]
        [Foldout("sound")]
        [SerializeField]
        private string soundStartName;

        [SerializeField]
        [Foldout("sound")]
        private string soundCancelName;

        [SerializeField]
        [Foldout("sound")]
        private string soundDropName;

        [SerializeField]
        [Foldout("sound")]
        private string soundHoverTarget;

        [ShowNativeProperty] bool wasActionValidated { get; set; }
        

        protected override void Awake()
        {
            base.Awake();
            dragDropDelegate = FindObjectOfType<DragDropManager>();
            TargetingArrow = FindObjectOfType<TargetingArrow>();
            Assert.IsNotNull(TargetingArrow, "missing targeting arrow");
            Assert.IsNotNull(dragDropDelegate, "missing dragDrop delegate from BaseUI");
        }

        private void Update()
        {
            if (!sourceView)
                return;
            if (Selection.CurrentSelection == null)
            {
                OnCancelDrag();
                return;
                
            }
            //Assert.IsNotNull(Selection.CurrentSelection);
            Assert.IsTrue(CurrentDragDropAction == this);
            if (ShouldCancelInteraction()) //|| !InteractionIsValid) //action was valid, but it is not anymore =>cancel
            {
                OnCancelDrag();
                return;
            }

            DraggingUpdate();
            CheckInteractionValidated();

            // TODO : do we cancel ??
            if (Input.GetMouseButtonUp(0))
            {
                if (wasActionValidated)
                    DropOn(sourceView, Selection.CurrentFocus);

                // cancel if we dragged a bit already (avoid cancel on click select)
                else if (totalDraggedDistance > .2f)
                {
                    Selection.Deselect();
                    OnCancelDrag();
                }
            }
        }

        #region private

        public GameObject GetSource()
        {
            return sourceView;
        }

        public void DropOn(GameObject source, GameObject dropElement)
        {
            DidDropOnEvent?.Invoke(source, dropElement);
            dragDropDelegate?.DidDropOn(sourceView, dropElement);
        }

        public void BeginWithSource(GameObject element)
        {
            sourceView = element;
            Assert.IsNotNull(sourceView, "dragdrop action source has no GO");
            // reset internal vars
            totalDraggedDistance = 0;
            CurrentDragDropAction = this;
            Selection.Select(sourceView.gameObject);
            Vector3 sourcePosition = sourceView.transform.position;
            pointerDisplacement = GetMouseWorldPosition() + new Vector3(0, 0, sourcePosition.z) - sourcePosition;
            anchorDraggingPosition = sourcePosition;

            // TODO : move to delegate
            if (sourceView.GetComponent<SortingGroup>())
            {
                oldLayerName = sourceView.GetComponent<SortingGroup>().sortingLayerName;
                sourceView.GetComponent<SortingGroup>().sortingLayerName = draggingLayerName;
            }

            AudioManager.Play(soundStartName);
            if (debug) Debug.Log($"{name} start dragging {sourceView}");
            dragDropDelegate?.BeginDrag( sourceView);
            OnStartDrag();
        }

        private void SourceFollowMouse()
        {
            Assert.IsNotNull(sourceView);
            Assert.IsNotNull(Selection.CurrentSelection);
            Vector3 mouseWorldPos =
                GetMouseWorldPosition() + new Vector3(0, 0, -dragAltitude) -
                (cursorInCenter ? Vector3.zero : pointerDisplacement); //sourceView.transform.position.z);
            // ignore Z for this distance, only distance on dragging plane 
            var position = sourceView.transform.position;
            totalDraggedDistance += Vector2.Distance(position, mouseWorldPos);
            position = Vector3.Lerp(position, mouseWorldPos, Time.deltaTime / dragDamping);
            sourceView.transform.position = position;
        }

        #endregion

        #region Specific

        protected virtual void OnStartDrag()
        {
            if (!_isTargeting) return;
            BeginDragDropEvent?.Invoke(sourceView);
            TargetingArrow.BeginFrom(sourceView.gameObject);
            Cursor.visible = false;
        }

        protected virtual void CheckInteractionValidated()
        {
            if (!sourceView)
                return;
            if (wasActionValidated) //do it once
                return;

            wasActionValidated = validationDistance <= 0 || dragDistance > validationDistance;
            if (wasActionValidated)
            {
                if (debug) Log.Info("dragdrop activated");
            }
        }

        protected virtual bool ShouldCancelInteraction()
        {
            if (!sourceView)
                return true;
            if (Input.GetKeyDown(cancelKey))
                return true;
            // was validated but dragged to close to initial position
            return wasActionValidated && dragDistance < validationDistance && cancelWhenDistanceToSmall;
        }

        private void UpdateDistanceValidation()
        {
            if (_isTargeting)
            {
                // use mouse NOT source
                Vector3 mouseWorldPos = GetMouseWorldPosition() + new Vector3(0, 0, anchorDraggingPosition.z);

                dragDistance = Vector2.Distance(anchorDraggingPosition, mouseWorldPos);
            }
            else
            {
                dragDistance = Vector2.Distance(anchorDraggingPosition, sourceView.transform.position);
            }
        }

        protected virtual void DraggingUpdate()
        {
            Assert.IsNotNull(sourceView);
            if (helpLabel)
            {
                helpLabel.text = dragDropDelegate?.GetHelpText();
                helpLabel.transform.position = Input.mousePosition;//GetMouseWorldPosition();
            }

            UpdateDistanceValidation();
            if (_isTargeting) return;
            SourceFollowMouse();
            if (!keepRotation) sourceView.transform.DORotate(Vector3.zero, .25f);
            //else if(snapRotation) sourceView.transform.DORotate(sourceView.transform.localEulerAngles, .25f);
        }

        protected virtual void UpdateTargetingArrow(GameObject target)
        {
            if (target != null && dropTags.Contains(target.tag))
            {
                AudioManager.Play(soundHoverTarget);
                TargetingArrow.SetTargetSprite(dragDropDelegate.CanDropOn(sourceView, target) ? validTargetSprite : invalidTargetSprite);
            }
            else
            {
                TargetingArrow.SetTargetSprite(null);
            }

            TargetingArrow.SetTarget(Selection.CurrentFocus);
        }

        //always called last
        public virtual void OnEndDrag()
        {
            if (!sourceView)
                return;

            if (helpLabel)
            {
                helpLabel.text = "";
            }

            if (debug) Debug.Log($" stop dragging {sourceView.name}");

            //after for sorting issues
            if (sourceView.GetComponent<SortingGroup>())
                sourceView.GetComponent<SortingGroup>().sortingLayerName = oldLayerName;
            sourceView = null;
            TargetingArrow.enabled = false;
            CurrentDragDropAction = null;
            wasActionValidated = false;
            EndDragDropOnEvent?.Invoke(this);
            dragDropDelegate?.EndDrag(this);
            Cursor.visible = true;
            Selection.Deselect();
        }

        //called when selection was canceled
        public virtual void OnCancelDrag()
        {
            if (sourceView == null)
            {
                return;
            }
             Selection.Deselect();
            if (sourceView && sourceView.GetComponent<SortingGroup>())
            {
                sourceView.GetComponent<SortingGroup>().sortingLayerName = oldLayerName;
            }

            //TODO:   add ? Selection.Deselect();
            //sourceView.GetComponent<ElementView>()?.Reset();

            AudioManager.Play(soundCancelName);
           // dragDropDelegate?.CancelDragDropOnAction(this);
            OnEndDrag();
        }

        #endregion

        #region Selection interface

        protected override void OnSelectionDeselect(GameObject deselected)
        {
            if (sourceView) OnEndDrag();
        }
        public sealed override void OnSelectionChanged(GameObject obj)
        {
            // if (EventSystem.current.IsPointerOverGameObject())
            //     return;
            
            if (sourceView) DropOn(sourceView,obj);
            else
            {
                if (CurrentDragDropAction != null) return;
                if (obj == null) return;

               if(ignoreDragTagList.Contains(obj.tag))
                   return;
                
                // local drag Drop : only This object is allowed as source
                if (GetComponent<Collider2D>() != null && obj != gameObject)
                {
                    Debug.LogWarning("Local Dragdrop: source is not this");
                    return;
                }

                if (!dragDropDelegate.CanDrag( obj)) return;
                BeginWithSource(obj);
            }
        }

        protected sealed override void OnFocusChanged(GameObject newObj, GameObject oldObj)
        {
            // TODO: check why right condition?
            if (sourceView == null)
                return;
            if (_isTargeting) UpdateTargetingArrow(newObj);
        }

        public void UseTargeting(bool useIt)
        {
            _isTargeting = useIt;
        }

        #endregion

        public void AddArrowStep(Vector3 position)
        {
            TargetingArrow.AddPoint(position);
        }

        public void MoveSourceBack()
        {
            if (sourceView == null)
                return;

            sourceView.transform.position = anchorDraggingPosition;
        }

        public void PutSourceDown()
        {
            if (sourceView == null)
                return;

            sourceView.transform.position = sourceView.transform.position.WithZ(0);
        }
    }
}