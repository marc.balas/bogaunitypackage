using UnityEngine;

namespace BogakitUnity
{
    public abstract class DragDropManager : MonoBehaviour, IDragDropDelegate
    {
        #region DragDropDelegate

        public abstract bool CanDropOn(GameObject source, GameObject drop);

        public abstract bool CanDrag(GameObject element);

        public virtual void BeginDrag(GameObject arg2)
        {
            Debug.Log($"BeginDropAction {arg2.name} :");
        }

        public virtual void EndDrag(DragDropAction obj)
        {
            Debug.Log($"End Drop");
        }

        public virtual void DidDropOn(GameObject source, GameObject drop)
        {
            Debug.Log($"DropOn:  did drop {source} on {drop} :");
        }
       
        public virtual string GetHelpText()
        {
            return "TODO:drag drop help";
        }

        public virtual void CancelDragDropOnAction(DragDropAction dropAction)
        {
            Debug.Log($"Cancel Drop: {dropAction} ");
        }
        #endregion
    }
}
