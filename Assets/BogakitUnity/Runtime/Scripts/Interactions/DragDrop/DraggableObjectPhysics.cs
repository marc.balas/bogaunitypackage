using UnityEngine;

namespace BogakitUnity
{
    [RequireComponent(typeof(Rigidbody))]
    public class DraggableObjectPhysics : MonoBehaviour
    {
    
        public Vector3 CurrentMousePosition;
        private Camera mainCamera;

        private Rigidbody _rigidbody;
        private float _startYPos;
        public Vector3 difference;
        public float altitude = 1;
        public float speedFactor = 10;
        Plane m_Plane;
        void Start()
        {
            //  mainCamera = Camera.main;
            //  _board = GetComponentInParent<BoardController>();
            _rigidbody = GetComponent<Rigidbody>();

       
            _startYPos = 0; // Better to not hardcode that one but whatever
        }

        private void OnMouseDrag()
        {
            Vector3 newWorldPosition = new Vector3(CurrentMousePosition.x, altitude , CurrentMousePosition.z);

            difference = newWorldPosition - transform.position;

            var speed = speedFactor * difference;
            _rigidbody.velocity = speed;
            _rigidbody.rotation = Quaternion.Euler(new Vector3(speed.z, 0, -speed.x));
        }
    
        // void Update()
        // {
        //     Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        //     RaycastHit[] hits = Physics.RaycastAll(ray);
        //
        //     foreach (var hit in hits)
        //     {
        //         if (hit.collider.gameObject != gameObject) continue; //.layer != LayerMask.NameToLayer("Table"))
        //         Debug.DrawLine(ray.origin, ray.origin + ray.direction * 100, Color.red);
        //
        //         CurrentMousePosition = hit.point;
        //         
        //         break;
        //     }
        // }
    
        void Update()
        {
            //Detect when there is a mouse click
            //Create a ray from the Mouse click position
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //Initialise the enter variable
            float enter = 0.0f;
            //Create a new plane with normal (0,1,0) at the position away from the camera you define in the Inspector. This is the plane that you can click so make sure it is reachable.
            m_Plane = new Plane(Vector3.up,  - altitude);

            if (m_Plane.Raycast(ray, out enter))
            {
                Debug.DrawLine(ray.origin, ray.origin + ray.direction * 100, Color.red);
                //Get the point that is clicked
                Vector3 hitPoint = ray.GetPoint(enter);
                CurrentMousePosition = hitPoint;
                //Move your cube GameObject to the point where you clicked
                //m_Cube.transform.position = hitPoint;
            }
        
        }
    }
}