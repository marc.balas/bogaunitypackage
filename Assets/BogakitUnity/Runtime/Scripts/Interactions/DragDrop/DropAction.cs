using UnityEngine;
using UnityEngine.Events;
using GameObject = UnityEngine.GameObject;

namespace BogakitUnity
{
    public class DropAction : MonoBehaviour
    {
        [SerializeField] private UnityEvent<GameObject> dropEvent;
        //[SerializeField] private GameObject dropZone;
    
        // [Required("hey hey ")]
        // [SerializeField]   private DragDropAction dragDropAction;


        private void Awake()
        {
      
            // Assert.IsNotNull(dragDropAction);
        }

        private void OnEnable()
        {
            DragDropAction.DidDropOnEvent += DragDropActionOnDidDropOnEvent;
            
      

        }

        private void DragDropActionOnDidDropOnEvent(GameObject source, GameObject go)
        {
            if (go == gameObject) 
                dropEvent?.Invoke(source);
     
        }

        private void OnDisable()
        {
            DragDropAction.DidDropOnEvent -= DragDropActionOnDidDropOnEvent;
        }

        // 
    

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
