using UnityEngine;

namespace BogakitUnity
{
    public interface IDragDropAction
    {
        public void OnCancelDrag();
        public void BeginWithSource(GameObject elementView);
        //public UnityEngine.GameObject GetSource();
        // public void OnEndDrag();
        public void DropOn(GameObject source,GameObject go);

        void OnEndDrag();
    }
}