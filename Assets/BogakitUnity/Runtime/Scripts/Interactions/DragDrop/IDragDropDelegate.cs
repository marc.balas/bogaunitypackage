using UnityEngine;

namespace BogakitUnity
{
    public interface IDragDropDelegate
    {
        bool CanDropOn(GameObject source, GameObject gameObject);
        bool CanDrag(GameObject source);
        void BeginDrag( GameObject source);

        void EndDrag(DragDropAction source);
       // void CancelDragDropOnAction(DragDropAction obj);

        void DidDropOn(GameObject source, GameObject target);
        string GetHelpText();
    }

    public interface ITargetingDelegate
    {
        bool IsValidTargeting(GameObject source, GameObject target);
        bool CanStartTargetingWith(GameObject source);
        void BeginTargeting( GameObject source);
        void EndTargeting(GameObject source, GameObject target);
        
    }
}