﻿using BogakitUnity.Interactions.Abstract;
using UnityEngine;
using UnityEngine.Events;

namespace BogakitUnity.Interactions.Info
{
    public class DelayedElementActionInteraction : DelayedElementInteraction
    {
        [SerializeField]
        private UnityEvent<GameObject,GameObject> activateAction;

        [SerializeField]
        private UnityEvent<GameObject> deactivateAction;

        private GameObject previousSource;
        protected override void Deactivate()
        {
            deactivateAction?.Invoke(previousSource);
            previousSource = null;
        }

        protected override void Activate()
        {
            SourceElementChanged();
        }

        protected override void SourceElementChanged()
        {
           
            activateAction?.Invoke(sourceView,previousSource);
            previousSource = sourceView;
        }
    }
}