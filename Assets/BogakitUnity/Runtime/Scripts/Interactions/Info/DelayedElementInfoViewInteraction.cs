﻿using Boga.Kit;
using BogakitUnity.EntityViews.Abstract;
using BogakitUnity.Interactions.Abstract;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity.Interactions.Info
{
    public class DelayedElementInfoViewInteraction : DelayedElementInteraction
    {
        [SerializeField] private InfoView infoPanel;
        [SerializeField] private Transform FixedAnchor;

        [Header("in pixels")] [SerializeField]
        private Vector3 infoOffset
            ;

        protected override void Awake()
        {
            base.Awake();
            Assert.IsNotNull(infoPanel);
        }

        protected override void Deactivate()
        {
            if (infoPanel) infoPanel.gameObject.SetActive(false);
        }

        protected override void Activate()
        {
            // if (!source?.go)
            //     return;
            infoPanel.gameObject.SetActive(true);
            SourceElementChanged();
        }

        protected override void SourceElementChanged()
        {
            infoPanel.transform.position =
                FixedAnchor == null ? (mainCamera.WorldToScreenPoint(sourceView.transform.position) + infoOffset) : FixedAnchor.transform.position + infoOffset;
            Element el = sourceView.GetComponent<ElementView>()?.element;
            if(el != null)infoPanel.Populate(el);
        }

        /*private Vector3 CenteredOffset()
        {
            Vector3 panelPos = mainCamera.WorldToScreenPoint(currentElement.go.transform.position);
            
            return infoOffset;
        }*/
    }
}