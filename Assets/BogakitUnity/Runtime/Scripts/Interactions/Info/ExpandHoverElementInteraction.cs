using BogakitUnity.EntityViews.Abstract;
using BogakitUnity.Interactions.Abstract;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

namespace BogakitUnity.Interactions.Info
{
    public class ExpandHoverElementInteraction : DelayedElementInteraction
    {
        
        [Header("transform settings")]
        [SerializeField] private Vector3 offset = new Vector3(0, 1, -1);
        [SerializeField] private Vector3 targetRotation;
        [SerializeField] private bool localAngle;
        [Header("scale settings")]
        [SerializeField] private float reduceDuration = 0.5f;
        [SerializeField] private float expandDuration = 0.5f;
        [SerializeField] private float expandScale = 1.5f;
        [Header("other settings")]
        [SerializeField] private string targetLayerName;
        [SerializeField] private string transformToExpand ="Visual";

        //cached 
        [FormerlySerializedAs("hoverCard")] [Header("debug")]
        [SerializeField] private ElementView hoverElementView;
        
        [SerializeField]  private string oldLayerName;
        

        protected override void Deactivate()
        {
            if (hoverElementView == null)
                return;
            Debug.Log("reduce card hover");
            Transform t = hoverElementView.transform.Find(transformToExpand);
            if(hoverElementView.GetComponent<SortingGroup>())
                hoverElementView.GetComponent<SortingGroup>().sortingLayerName = oldLayerName;
            t.DOKill();
            t.DOLocalMove(Vector3.zero, reduceDuration);
            t.DOLocalRotate(Vector3.zero, reduceDuration);
            t.DOScale(Vector3.one, reduceDuration);
           // hoverElementView.OnFocus(false);
            hoverElementView = null;
            //sourceView = null;
        }

        protected override void Activate()
        {
            ElementView ev = sourceView.GetComponent<ElementView>();
            if(!ev)
                return;
            hoverElementView = ev;
          
            Transform t = sourceView.transform.Find(transformToExpand);
            if (t == null)
            {
                Debug.LogError($"{name}: Not child named {transformToExpand} in  {sourceView.name}");
                return;
            }
            
            if (sourceView.GetComponent<SortingGroup>())
            {
              if(sourceView.GetComponent<SortingGroup>().sortingLayerName != targetLayerName) oldLayerName = sourceView.GetComponent<SortingGroup>().sortingLayerName;
               sourceView.GetComponent<SortingGroup>().sortingLayerName = targetLayerName;
            }
            
            t.DOKill();
            if(localAngle) t.DOLocalRotate(targetRotation, reduceDuration);
            else t.DORotate(targetRotation, reduceDuration);
            t.DOLocalMove(offset, expandDuration);
            t.DOScale(Vector3.one * expandScale, expandDuration);
            // if (hoverElementView != null)
            //     hoverElementView.OnFocus(true);
        }

        protected override void SourceElementChanged()
        {
            Deactivate();
         if(activated) Activate();
        }

        public override void OnSelectionChanged(GameObject obj)
        {
            Deactivate();
            // this interaction is shut down as soon as something is selected
            sourceView = null;
        }
    }
}