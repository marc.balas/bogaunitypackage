﻿using System.Collections.Generic;
using BogakitUnity.EntityViews.Abstract;
using BogakitUnity.Extensions;
using BogakitUnity.Interactions.Abstract;
using UnityEngine;

namespace BogakitUnity.Interactions.Info
{
    /// <summary>
    /// not an element interaction cause timer doesnt make sense
    /// </summary>
    public class ShowChildOfValidTargetViews : ElementInteraction
    {
        [SerializeField] private string targetChildName;

        protected override void OnFocusChanged(GameObject newObj, GameObject oldObj)
        {
        }

        protected override void OnSelectionDeselect(GameObject deselected)
        {
            HideTargets();
        }


        public override void OnSelectionChanged(GameObject obj)
        {
        }

        private void HideTargets()
        {
                foreach (var target in EntityView.all)
                {
                    if (target)
                    {
                        //target.go.GetComponent<ElementView>().transform.Hide(targetChildName);
                        Transform targetT = target.GetComponent<ElementView>()?.transform.FindDeepChild(targetChildName);
                        if (targetT != null) targetT.GetComponent<SpriteRenderer>().enabled = false;
                    }
                }
            
        }

        private void Update()
        {
            // if (CommandInteraction.current != null)
            // {
            //     if (sourceView != null) return;
            //     HideTargets();
            //     sourceView = CommandInteraction.current.sourceView;
            //     ShowTargets();
            // }
            // else if (sourceView != null)
            // {
            //     HideTargets();
            //     sourceView = null;
            // }
        }

        public void ShowChildren(List<EntityView> views)
        {
            foreach (var target in views)
            {
                    Transform targetT = target.GetComponent<ElementView>()?.transform.FindDeepChild(targetChildName);
                    if (targetT != null) targetT.GetComponent<SpriteRenderer>().enabled = true;
            }
        }
    }
}