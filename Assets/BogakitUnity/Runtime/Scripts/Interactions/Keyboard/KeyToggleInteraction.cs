﻿using UnityEngine;

namespace BogakitUnity.Interactions.Keyboard
{
     public sealed class KeyToggleInteraction : KeyboardAction
    {

        
        [SerializeField] private string Description;
        [SerializeField] private GameObject toggleObject;

        protected override string description => Description;

        protected override void TryExecute()
        {
            toggleObject.SetActive(!toggleObject.activeSelf);
        }
      
    }
}