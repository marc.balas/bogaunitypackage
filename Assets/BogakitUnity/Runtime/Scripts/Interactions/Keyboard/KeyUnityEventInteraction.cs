﻿using UnityEngine;
using UnityEngine.Events;

namespace BogakitUnity.Interactions.Keyboard
{
     public sealed class KeyUnityEventInteraction : KeyboardAction
    {

        [SerializeField] private UnityEvent actionOnKey;
        [SerializeField] private string eventDescription;

        protected override string description => eventDescription;
        // Update is called once per frame
        protected override void TryExecute()
        {
            actionOnKey?.Invoke();
        }
      
    }
}