using System;
using System.Collections.Generic;
using UnityEngine;

namespace BogakitUnity.Interactions.Keyboard
{
    
    [Serializable]
    public class KeyInfo
    {
        public KeyCode key;
        public string description;

        public KeyInfo(KeyCode keyCode, string s)
        {
            key = keyCode;
            description = s;
        }
    }
    
    public abstract class KeyboardAction : MonoBehaviour
    {
        public static readonly List<KeyInfo> KeyInfos = new List<KeyInfo>();
        public KeyCode keyCode;
        protected virtual string description { get; } = "Key interaction";

        protected virtual void Awake()
        {
            Register();
        }

        protected abstract void TryExecute();
        
        private void Register()
        {
          KeyInfos.Add(new KeyInfo(keyCode,description));
        }
        protected virtual void Update()
        {
            if (Input.GetKeyDown(keyCode))
            {
                TryExecute();
            }
        }
    }
    
}