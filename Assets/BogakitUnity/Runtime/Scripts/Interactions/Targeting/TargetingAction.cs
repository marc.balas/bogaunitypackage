using BogakitUnity.Selections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace BogakitUnity
{
    public class TargetingAction : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IDropHandler,
        IPointerClickHandler
    {
        public static GameObject targetingSource;
         
        [SerializeField]private TargetingArrow targetingArrow;

        private TargetingManager targetManager;
        [SerializeField] public Vector3 anchor;
        [SerializeField] private bool allowClick = true;

        public GameObject target => gameObject == Selection.CurrentSelection ? Selection.CurrentFocus : null;

        //[SerializeField] private Vector3 lineAnchor;
        protected virtual void Awake()
        {
            targetManager = FindObjectOfType<TargetingManager>();
            if (targetingArrow == null) targetingArrow = FindObjectOfType<TargetingArrow>();
            Assert.IsNotNull(targetingArrow, "missing targeting arrow");
            Assert.IsNotNull(targetManager, "missing targeting delegate from BaseUI");
        }

        // Update is called once per frame
        void Update()
        {
            
        //    if (Selection.CurrentSelection != gameObject) return;
         //   if (Selection.CurrentFocus == null && Input.GetMouseButtonUp(0)) EndTargeting();
         if(targetingSource != gameObject)return;
            
            if (Input.GetMouseButtonUp(0))
            {

                // if (Selection.CurrentFocus == gameObject && targetingSource == null)
                // {
                //     StartTargeting();
                // }
                // else
                if(Selection.CurrentFocus != gameObject)
                {
                    targetManager.EndTargeting(targetingSource, targetingArrow.CurrentTarget);
                    EndTargeting();
                }
                
            }
            else UpdateTarget(Selection.CurrentFocus);
        }

        public void OnDrag(PointerEventData eventData)
        {
            // Debug.Log( "dragging" + eventData.pointerDrag?.name + $" distance {eventData.delta}" + " on "+ Selection.CurrentFocus?.name);
            UpdateTarget(Selection.CurrentFocus);
        }

        protected virtual void UpdateTarget(GameObject newTarget)
        {
            if (newTarget == gameObject) return;
            if (newTarget == targetingArrow.CurrentTarget) return;
            // additional check for valid target
            if (!targetManager.IsValidTargeting(gameObject, newTarget))
            {
                targetingArrow.SetTarget(null);
                return;
            }
            targetingArrow.SetTarget(newTarget);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            Debug.Log(" TargetingAction:OnBeginDrag: " + eventData.pointerDrag.name);
           // if (!CanStartTargeting()) return;
            StartTargeting();
        }

        private void StartTargeting()
        {
            Debug.Log($"TargetingAction:{name} start targeting ");
           // anchor = transform.position;
          //  if (!CanStartTargeting()) return;
            EventSystem.current.SetSelectedGameObject(gameObject);
            Selection.Select(gameObject);
            targetingSource = gameObject;
            targetingArrow.BeginFrom(gameObject);
          
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("TargetingAction: end drag " + eventData.pointerDrag.name);
           // transform.DOMove(anchor, 0.4f).SetEase(Ease.OutExpo).SetId(1);
           targetManager.EndTargeting(gameObject, Selection.CurrentFocus);
            EndTargeting();
        }

        private void EndTargeting()
        {
            Selection.HoverChanged(null);
            Debug.Log($"TargetingAction:end targeting {gameObject.name}");
            targetingArrow.EndTargeting();
            //Selection.CurrentSelection.transform.DOMove (anchor, 0.4f).SetEase( Ease.OutExpo).SetId(1); 
            Selection.Deselect();
            targetingSource = null;
        }

        public void OnDrop(PointerEventData eventData)
        {
            Debug.Log("TargetingAction:drop " + eventData.pointerDrag.name + " on " + Selection.CurrentFocus?.name + " current " +
                      gameObject.name);
            if (Selection.CurrentFocus != eventData.pointerDrag)
            {
                targetManager.EndTargeting(eventData.pointerDrag, Selection.CurrentFocus);
                
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Debug.Log("TargetingAction:clicked " + gameObject.name + " selection " + Selection.CurrentSelection?.name);
            if (!allowClick) return;
            
            //if (Selection.CurrentSelection == null)
                if (targetingSource == null)
            {
                if ( targetManager.CanStartTargetingWith(gameObject))
                {
                    StartTargeting();
                }

            }
            else 
            {
                targetManager.EndTargeting(targetingSource, eventData?.pointerCurrentRaycast.gameObject);
                EndTargeting();
            }
        }

      

        private void OnDisable()
        {
            // Check if the application is still playing

            EndTargeting();
        }

        protected void UpdateTargetSprite(Sprite sprite)
        {
            targetingArrow.SetTargetSprite(sprite);
        }
    }
}