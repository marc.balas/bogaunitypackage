﻿using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Serialization;
using GameObject = UnityEngine.GameObject;

namespace BogakitUnity
{
    
    
    [RequireComponent(typeof(LineRenderer))]
    public sealed class TargetingArrow : MonoBehaviour
    {
        [ShowIf("debug")]private GameObject currentTarget;
        [SerializeField] private Vector2 uvAnimationRate = new Vector2(-1.5f, 0.0f);
        [SerializeField] private float arrowWidth = 0.5f;
        [SerializeField] private float minDistance = 0.5f;
        [FormerlySerializedAs("DashSize")] [SerializeField] private float dashSize = .5f;

        [SerializeField] [Required]  private GameObject head;
        [SerializeField]  private SpriteRenderer target; 

        [Header("debug")] [SerializeField] private bool debug;

        public GameObject CurrentTarget => currentTarget;
       
       
        [SerializeField] private GameObject rallyPointPrefab;
        [FormerlySerializedAs("rallyPoints")] [SerializeField]
        private List<GameObject> dropObjects;

        private LineRenderer lineRenderer;
        private Vector2 uvOffset = Vector2.zero;
        private Vector3 initialPosition;
        private Camera mainCamera;
        
        private readonly int mainTex = Shader.PropertyToID("_MainTex");


        private void Awake()
        {
            mainCamera = Camera.main;
            lineRenderer = gameObject.GetComponent<LineRenderer>();
            //target.enabled = true;
            Assert.IsNotNull(lineRenderer);
            Assert.IsNotNull(target);
            target.gameObject.SetActive(false);
            lineRenderer.enabled = false;
            head.SetActive(false);
        }

        private void Update()
        {
            UpdateArrowLine(currentTarget ? currentTarget.transform.position: GetMouseWorldPosition());
        }

        public void AddPoint(Vector3 rallyPoint)
        {
            lineRenderer.positionCount += 1;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, rallyPoint);
            CreateRallyPoint(rallyPoint);
        }

        private void LateUpdate()
        {
            uvOffset += uvAnimationRate * Time.deltaTime;
            lineRenderer.material.SetTextureOffset(mainTex, uvOffset);
        }

        public void BeginFrom(GameObject go)
        {
            gameObject.SetActive(true);
            enabled = true;
            head.SetActive(true);
            initialPosition = go.transform.position;
            InitLineRenderer(); 
            SetTarget(null);
        }

        private void CreateRallyPoint(Vector3 position)
        {
            GameObject rally = Instantiate(rallyPointPrefab, position, Quaternion.identity, transform.parent);
            rally.SetActive(true);
            dropObjects.Add(rally);
        }

        private void InitLineRenderer()
        {
            lineRenderer.enabled = true;
            lineRenderer.SetPosition(0, initialPosition);
        }


        private void UpdateArrowLine(Vector3 position)
        {
            lineRenderer.startWidth = arrowWidth;
            lineRenderer.endWidth = arrowWidth;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, position);
            var arrowLastBeforeEnd = lineRenderer.GetPosition(lineRenderer.positionCount - 2);
            var arrowEnd = lineRenderer.GetPosition(lineRenderer.positionCount - 1);

            var arrowLen = CalculateTotalLength();
            if (debug) Debug.Log($"{initialPosition} ===>{position}, distance :{Vector3.Distance(initialPosition, position)}");
            if (debug) Debug.Log($"arrow length {arrowLen}");
            float textureScaleX = 50 * arrowLen / (dashSize *lineRenderer.material.mainTexture.width);
            lineRenderer.material.mainTextureScale = new Vector2(textureScaleX, arrowWidth);

            head.transform.position = arrowEnd; //lineRenderer.GetPosition(lineRenderer.positionCount-1);
            var angle = Mathf.Atan2(arrowEnd.y - arrowLastBeforeEnd.y, arrowEnd.x - arrowLastBeforeEnd.x) * 180 /
                        Mathf.PI;
            head.transform.rotation = Quaternion.Euler(0, 0, angle + 180);
        }

        private float CalculateTotalLength()
        {
            float length = 0;
            for (int i = 1; i < lineRenderer.positionCount; i++)
            {
                var arrowStart = lineRenderer.GetPosition(i - 1);
                var arrowEnd = lineRenderer.GetPosition(i);
                length += Vector3.Distance(arrowStart, arrowEnd);
            }

            return length;
        }

        private void Clear()
        {
            SetTarget(null);
            lineRenderer.positionCount = 2;
            foreach (var go in dropObjects)
            {
                Destroy(go);
            }
            dropObjects.Clear();
        }

        private void OnDisable()
        {
            Clear();
        }

        public void SetTarget(GameObject newTarget)
        {
            if (newTarget == null)
            {
                target.gameObject.SetActive(false);
                currentTarget = null;
            }
            //target changed
            else if (currentTarget != newTarget)
            {
                currentTarget = newTarget;
                //activate visual target if there is a target
                target.gameObject.SetActive(true);
                target.transform.position = currentTarget.transform.position;
                target.transform.eulerAngles = Vector3.zero;
                //if (currentTarget) CreateTarget(currentTarget.transform.position);
            }
        }

        public void SetTargetSprite(Sprite newTargetSprite)
        {
            target.GetComponentInChildren<SpriteRenderer>().sprite = newTargetSprite;
        }
        
        public void EndTargeting()
        {
            if(lineRenderer)lineRenderer.enabled = false;
            if(head)head.SetActive(false);
            enabled = false;
            
        }

        private Vector3 GetMouseWorldPosition()
        {
            //create plane , where the current arrow start => initial pos
            Plane plane = new Plane(-Vector3.forward, initialPosition);
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out float enter))
            {
                return ray.GetPoint(enter);
            }

            return Vector3.zero;
        }
    }
}