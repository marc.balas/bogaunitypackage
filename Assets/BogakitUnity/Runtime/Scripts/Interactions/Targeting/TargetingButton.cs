using BogakitUnity.Selections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BogakitUnity
{
    
    [RequireComponent(typeof(Button))]
    public class TargetingButton: MonoBehaviour //, IPointerClickHandler
    {
        
      [SerializeField] public UnityEvent<GameObject> onClick;
      [SerializeField] GameObject target;
      [SerializeField] TextMeshProUGUI helpLabel;
      [SerializeField] string helpText;
      bool isTargeting = false;
      //private Button button;

      private void Awake()
      {
      //    button = GetComponent<Button>();
      //    button.onClick.AddListener(StartTargeting);
      }

      public void StartTargeting()
      {
          isTargeting = true;
         helpLabel.gameObject.SetActive(true);
         target.SetActive(true);
         helpLabel.text = helpText;
      //   EventSystem.current.SetSelectedGameObject(button.gameObject);
      }

      private void Update()
      {
          if(!isTargeting ) return;
          if (Input.GetMouseButtonDown(0)  )
          {
              isTargeting = false;
              target.SetActive(false);
              helpLabel.gameObject.SetActive(false);
              onClick?.Invoke(Selection.CurrentFocus);
              Debug.Log("current selected " + EventSystem.current.currentSelectedGameObject?.name);
              
          }
          else 
          {
              target.transform.position = Selection.CurrentFocus?.transform.position ?? Input.mousePosition;
          //    helpText.text = Selection.CurrentFocus?.name ?? "-";
          }
      }

      // public void OnPointerClick(PointerEventData eventData)
      // {
      //     Debug.Log($"targeting button click on {eventData.pointerCurrentRaycast.gameObject?.name}");
      //     onClick?.Invoke(eventData.pointerCurrentRaycast.gameObject);
      // }
    }
}