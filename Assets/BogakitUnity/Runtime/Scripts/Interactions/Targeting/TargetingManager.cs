using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity
{
    public abstract class TargetingManager : MonoBehaviour, ITargetingDelegate
    {
        
        [Required] [SerializeField] private TargetingAction targetingAction;
        [Required] [SerializeField] private TargetingArrow targetingArrow;
        public abstract bool IsValidTargeting(GameObject source, GameObject target);
        public abstract bool CanStartTargetingWith(GameObject source);
        public abstract void BeginTargeting(GameObject source);
        //TODO: use state : isTargeting, current source or targeting action
        public abstract void EndTargeting(GameObject source, GameObject target);
    }
}
