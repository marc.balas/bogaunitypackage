﻿using System.Collections;
using UnityEngine;

namespace BogakitUnity.Anim
{


	public class CameraShake : MonoBehaviour
	{


		public float shakeAmount= 1f;
//The amount to shake this frame.
		public float shakeDuration = .5f;
//The duration this frame.
		public KeyCode shakeOnKey = KeyCode.None;

		void Update ()
		{

			if (shakeOnKey != KeyCode.None && Input.GetKeyDown (shakeOnKey)) {
				Shake ();
			}

		}


		public IEnumerator DoCameraShake ()
		{

			var camera = Camera.main.transform;
			var basePos = camera.localPosition;
			float startTime = Time.time;

			while (Time.time < startTime + shakeDuration) {

				float progress = Mathf.Clamp01 ((Time.time - startTime) / shakeDuration);
				camera.localPosition = basePos + new Vector3 (Random.Range (-1f, 1f), Random.Range (-1f, 1f), 0) * shakeAmount * (1f - progress);
				yield return null;
			}
			camera.localPosition = basePos;
		}

		public void Shake ()
		{

			StartCoroutine (DoCameraShake ());

		}



	}
}