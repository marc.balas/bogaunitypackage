﻿using UnityEngine;

namespace BogakitUnity.Anim
{
    public class LerpFollow : MonoBehaviour
    {

        [SerializeField]
        private Transform objectToFollow;

        [Min(0)]
        [SerializeField] private float damping;

        [SerializeField]  private Vector3 _relPos;
        
        [SerializeField] 
        private Vector3 offset;

        public Vector3 GetOffset()=> offset;

        // Update is called once per frame
        void Update()
        {
            if (damping >= 0 && objectToFollow)
            {
            
                transform.position = Vector3.Lerp(transform.position, objectToFollow.position+offset,  Time.unscaledDeltaTime/damping);
            
            }
        }

        public void SetOffset(Vector3 offset)
        {
            this.offset = offset;
        }
        public void Follow(Transform stackableTargetTransform)
        {
            objectToFollow = stackableTargetTransform;
        }
        public void Detach()
        {
            objectToFollow = null;
        }
    }
}
