using System;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BogakitUnity
{
    public class PunchHover : MonoBehaviour , IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
    {
        
        public bool ignoreWhenSelection;
        [Required][SerializeField] private Transform punchParent;

        [Header("Scale Parameters")]
        [SerializeField]
        public float scaleOnHover = 1.15f;
        [SerializeField] private float scaleTransition = .15f;
        [SerializeField] private Ease scaleEase = Ease.OutBack;

        
        [Header("Hover Parameters")]
        [SerializeField] private float hoverPunchAngle = 5;
        [SerializeField] private float hoverTransition = .15f;


        private void Update()
        {
         
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            
            Punch();
        }

        public void Punch()
        {
            if(!enabled)return;
            if (ignoreWhenSelection && EventSystem.current.currentSelectedGameObject != null) return;
            transform.DOScale(scaleOnHover, scaleTransition).SetEase(scaleEase);

            DOTween.Kill(2, true);
            punchParent.DOPunchRotation(Vector3.forward * hoverPunchAngle, hoverTransition, 20, 1).SetId(2);

        }

        public void OnPointerExit(PointerEventData eventData)
        {
            transform.DOScale(1, scaleTransition).SetEase(scaleEase);
            transform.DORotateQuaternion(Quaternion.Euler(0, 0, 0), hoverTransition).SetEase(Ease.OutExpo);   
        }


        public void OnSelect(BaseEventData eventData)
        {
            if (ignoreWhenSelection) return;
            OnPointerEnter(null);
        }

        public void OnDeselect(BaseEventData eventData)
        {
            OnPointerExit(null);
        }
    }
}