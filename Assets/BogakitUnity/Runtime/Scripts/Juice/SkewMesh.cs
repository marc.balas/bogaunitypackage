﻿using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity.Anim
{
    public class SkewMesh : MonoBehaviour
    {

        [SerializeField] private Transform skewPivotA;
        [SerializeField] private Transform skewPivotB;
        [SerializeField] private Transform skewPivotC;
        [SerializeField] private Transform controlPoint;

        Vector3 _origControlPointRelPos; 
        private void Awake()
        {
            _origControlPointRelPos = transform.InverseTransformPoint(controlPoint.position);
            Assert.IsTrue(_origControlPointRelPos != Vector3.zero);
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 controlPointRelPos = transform.InverseTransformPoint(controlPoint.position);
            Vector3 skewedUp = new Vector3(
                                   controlPointRelPos.x - _origControlPointRelPos.x,
                                   controlPointRelPos.y,
                                   controlPointRelPos.z - _origControlPointRelPos.z) / _origControlPointRelPos.y;


            Vector3 axis = Vector3.Cross(Vector3.up, skewedUp).normalized;
            float theta = Vector3.Angle(Vector3.up, skewedUp);
            float alpha = Vector3.SignedAngle(Vector3.forward, axis, Vector3.up);

            Quaternion targetRotA= Quaternion.Euler(0, alpha, -(90 - theta) / 2);
            Quaternion targetRotB = Quaternion.Euler(0, 0, 45);
            Quaternion targetRotC = Quaternion.Euler(0, -alpha, 0);
          
            float p = Mathf.Tan((theta + 90) / 2 * Mathf.Deg2Rad);
            float w = Mathf.Sqrt((1 + p * p) / 2);
            
            Vector3 pivotATargetScale = new Vector3(p, 1, 1);
            Vector3 pivotBTargetScale = new Vector3(1 / w, 1 + w / p * skewedUp.y, 1);

                skewPivotA.localRotation = targetRotA;
                skewPivotB.localRotation = targetRotB;
                skewPivotC.localRotation = targetRotC;

                skewPivotA.localScale = pivotATargetScale;
                skewPivotB.localScale = pivotBTargetScale;

            
        }
    }
}