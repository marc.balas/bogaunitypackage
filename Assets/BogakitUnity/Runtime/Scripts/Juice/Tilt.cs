using BogakitUnity.Selections;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace BogakitUnity
{
    public class Tilt : MonoBehaviour //, IPointerEnterHandler, IPointerExitHandler
    {
        [InfoBox("auto tilt by default, if hovered, it will look at the mouse")] 
        [Required] [SerializeField] private Transform tiltParent;

        [Tooltip("time tilt amount")] [SerializeField]
        private Vector2 autoTiltAmount = new Vector2(10, 10);

        [Tooltip("pointer tilt amount")] [SerializeField]
        private float manualTiltAmount = 20;

        [SerializeField] private float tiltSpeed = 20;

        [SerializeField] private int randomIndex;

        [Tooltip("on hover :look away from pointer ")] [SerializeField]
        private bool invertHover;

        [Header("Debug")] public bool isHovering;

        private void Awake()
        {
            randomIndex = Random.Range(0, 10);
        }

        private void Update()
        {
            isHovering = Selection.CurrentFocus == gameObject;
            tiltParent.eulerAngles = isHovering ? LookAtPointer() : GetAutoTiltAmount();
        }

        private Vector3 GetAutoTiltAmount()
        {
            float sine = Mathf.Sin(Time.time + randomIndex);
            float cosine = Mathf.Cos(Time.time + randomIndex);

            float lerpX = Mathf.LerpAngle(tiltParent.eulerAngles.x, (sine * autoTiltAmount.x),
                tiltSpeed * Time.deltaTime);
            float lerpY = Mathf.LerpAngle(tiltParent.eulerAngles.y, (cosine * autoTiltAmount.y),
                tiltSpeed * Time.deltaTime);
            float lerpZ = Mathf.LerpAngle(tiltParent.eulerAngles.z, 0, tiltSpeed / 2 * Time.deltaTime);
            return new Vector3(lerpX, lerpY, lerpZ);
        }

        private Vector3 LookAtPointer()
        {
            float sine = 0;
            float cosine = 0;
            float tiltX = 0;
            float tiltY = 0;
            // Convert the mouse position to world coordinates
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,
                Input.mousePosition.y, Camera.main.WorldToScreenPoint(transform.position).z));
            Vector3 offset = mouseWorldPosition - transform.position;
            tiltX = ((offset.y * -1) * manualTiltAmount);
            tiltY = ((offset.x) * manualTiltAmount);
            if (invertHover)
            {
                tiltX *= -1;
                tiltY *= -1;
            }

            //Debug.Log( $"mouse offset {offset.x}, {offset.y} : target rot " + tiltX + " " + tiltY + $"current  {tiltParent.eulerAngles.x}  {tiltParent.eulerAngles.y}");
            float lerpX = Mathf.LerpAngle(tiltParent.eulerAngles.x, tiltX, tiltSpeed * Time.deltaTime);
            float lerpY = Mathf.LerpAngle(tiltParent.eulerAngles.y, tiltY, tiltSpeed * Time.deltaTime);
            float lerpZ = Mathf.LerpAngle(tiltParent.eulerAngles.z, 0, tiltSpeed / 2 * Time.deltaTime);

            return new Vector3(lerpX, lerpY, lerpZ);
        }
    }
}