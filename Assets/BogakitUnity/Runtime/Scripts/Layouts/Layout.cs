﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using BogakitUnity.Extensions;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
using Selection = BogakitUnity.Selections.Selection;

#if UNITY_EDITOR

#endif

namespace BogakitUnity.Layouts
{
    /// <summary>
    /// Layout 3d GameObjects position/rotation and scale of children (transform)
    /// use a scriptable object LogicLayout (asset) for re-usability  
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu("BogaKit/Layout", order: -1)]
    public class Layout : MonoBehaviour
    {
        #region FIELDS

        [Header("layout elements logic")] [SerializeField] [Expandable]
        protected LayoutLogic logic;

        [SerializeField] private bool ignoreCurrentSelection = true;

        //    public string layerName;
        [FormerlySerializedAs("Size")] [Header("Settings")]
        public Vector3 size = Vector3.one;

        [FormerlySerializedAs("Offset")] public Vector3 offset = Vector3.zero;

        //[SerializeField] private bool UseHierarchy;

        [Tooltip("Constant offset added between each element")]
        public Vector3 elementSpacing = new Vector3(0, 0, 0f);

        // [EnumFlags]
        public string sortingLayer; //= SortingLayer.IDToName(0);

        [Foldout("Angles")] [Tooltip("Angle is inherited from element")] [SerializeField]
        private bool angleInherited;

        [Foldout("Angles")] [Tooltip("Angle added between each element")]
        public Vector3 angleIncrement = new Vector3(0, 0, 0.0f);

        [Foldout("Angles")] [Tooltip("Angle of element inside container")] [SerializeField]
        private Vector3 constantAngle = Vector3.zero;

        [Tooltip("Scale of elements when organized")] [SerializeField] [MinValue(0.0f)]
        protected float scale = 1;

        [Foldout("Randomness")] [Header("Randomness settings")] [SerializeField]
        private int randomnessSeed;

        [Foldout("Randomness")] [SerializeField]
        private Vector3 randomPositionRange = Vector3.zero;

        [Foldout("Randomness")] [SerializeField]
        private Vector3 randomAngleRange = Vector3.zero;

        [Foldout("Randomness")] public Vector2 randomScaleRange = Vector2.one;

        [Header("Dummies")] [Foldout("Dummies")] [Range(0, 100)] [SerializeField]
        private int nbDummies = 5;

        [Foldout("Dummies")] [SerializeField] private Vector3 dummySize = Vector3.one;

        //[Foldout("Dummies")]
        [SerializeField] private Color dummyColor => fixedCapacity > 0 ? Color.magenta : Color.green;

        [SerializeField] private bool debug;

        [SerializeField] private AnimationCurve positionCurve = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField] private AnimationCurve rotationCurve = AnimationCurve.Linear(0, 0, 1, 1);

        // [SerializeField]
        // private List<int> spaceIndexes = new List<int>();
        [SerializeField] [Tooltip("create empty obj for each slot")]
        private int fixedCapacity = -1;

        //[SerializeField]
        // [ShowIf("fixedCapacity", 1)]
        //private GameObject emptySlotPrefab;
        [SerializeField] private int maxCapacity = 3;

        public bool HasMaxCapacity => maxCapacity > 0;
        public bool IsFull => HasMaxCapacity && transform.childCount >= maxCapacity;
        public bool IsEmpty => transform.childCount == 0;
        public int Capacity => maxCapacity > 0 ? maxCapacity : -1;

        [SerializeField] private bool positionLocked;
        [SerializeField] private bool updateRotation;

        [SerializeField] private float defaultDuration = .5f;
        [SerializeField] private Ease ease;

        #endregion

        #region PROPERTIES

        public Vector3 elementScale => Vector3.one * scale;
        public bool HasFixedCapacity => fixedCapacity > 0;

        #endregion

        private void Awake()
        {
            //FillEmptySlots();
        }

        #region API

        // public int NbSpacesBefore(int index)
        // {
        //     return spaceIndexes.Count(i => i <= index);
        // }
        public Vector3 PositionAtIndex(int index, bool gizmos = false)
        {
            // add space to index
            // int spaces = NbSpacesBefore(index); 

            List<GameObject> children = transform.GetChildren();
            if (index < 0)
                return Vector3.zero;
            Vector3 randomOffset = Vector3.zero;
            Random.InitState(randomnessSeed + index);
            if (randomPositionRange != Vector3.zero)
                randomOffset = Vector3.Scale(Random.insideUnitSphere, randomPositionRange);

            if (logic)
            {
                return transform.position + offset +
                       logic.GetPositionAtIndex(index, this, gizmos ? nbDummies : children.Count) + randomOffset;
            }

            return transform.position + ((index) * elementSpacing) + randomOffset + offset;
        }

        public Vector3 NextPosition()
        {
            return PositionAtIndex(transform.childCount);
        }

        private Vector3 ScaleAtIndex(int index)
        {
            Random.InitState(randomnessSeed + index);
            Vector3 randomOffset = Vector3.zero;
            if (randomScaleRange != Vector2.one)
                randomOffset = Vector3.Scale(Random.insideUnitSphere, randomScaleRange);
            if (logic) return logic.GetScaleAtIndex(index, this) + randomOffset;
            return scale * Vector3.one + randomOffset;
        }

        private Vector3 RotationAtIndex(int index, bool forGizmos = false)
        {
            List<GameObject> children = transform.GetChildren();
            Vector3 randomOffset = Vector3.zero;
            Random.InitState(randomnessSeed + index);
            if (randomAngleRange != Vector3.zero)
                randomOffset = Vector3.Scale(Random.insideUnitSphere, randomAngleRange);

            if (angleInherited)
                return randomOffset;
            if (logic)
                return angleIncrement * index + transform.localEulerAngles + constantAngle +
                       logic.GetRotationAtIndex(index, this, forGizmos ? nbDummies : children.Count) + randomOffset;

            //hack to get rid of 360*N
            return Quaternion.Euler(constantAngle + index * angleIncrement + randomOffset).eulerAngles;
            //return Quaternion.Euler(constantAngle + index * angleIncrement + randomOffset).eulerAngles ;
        }

        #endregion

        #region Utils

        //what would be the index of a position in this layout
        public int ClosestIndexFromPosition(Vector3 position)
        {
            List<GameObject> children = transform.GetChildren();

            if (positionLocked)
                return children.Count;

            if (children.Count == 0) return 0;
            int closestIndex = -1;
            float closestDistance = float.MaxValue;
            //check also n+1 ?
            for (int i = 0; i < children.Count; i++)
            {
                float dist = Vector3.Distance(PositionAtIndex(i), position);
                // Debug.Log($"distance to index {i} : {dist:F0} ");
                if (dist + 0.01f < closestDistance)
                {
                    //   Debug.Log($"new index {i} : dist to beat : {dist:F0} ");
                    closestDistance = dist;
                    closestIndex = i;
                }
            }

            if (debug) Debug.Log("closest index " + closestIndex + " position " + position + " in " + name);
            return closestIndex;
        }

        public float ClosestDistance(Vector3 position)
        {
            List<GameObject> children = transform.GetChildren();
            if (children.Count == 0) return 0;
            float closestDistance = float.MaxValue;
            //check also n+1 ?
            for (int i = 0; i < children.Count; i++)
            {
                float dist = Vector3.Distance(PositionAtIndex(i), position);
                // Debug.Log($"distance to index {i} : {dist:F0} ");
                if (dist + 0.01f < closestDistance)
                {
                    //   Debug.Log($"new index {i} : dist to beat : {dist:F0} ");
                    closestDistance = dist;
                }
            }

            if (debug) Debug.Log("closest distance " + closestDistance + " position " + position + " in " + name);
            return closestDistance;
        }

        public Vector3 ClosestPosition(Vector3 position)
        {
            return PositionAtIndex(ClosestIndexFromPosition(position));
        }

        #endregion

        #region Gizmos

        private static void DrawCube(Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Matrix4x4 cubeTransform = Matrix4x4.TRS(position, rotation, scale);
            Matrix4x4 oldGizmosMatrix = Gizmos.matrix;
            Gizmos.matrix *= cubeTransform;
            Gizmos.DrawWireCube(Vector3.zero, scale);
            Gizmos.DrawLine(Vector3.zero, Vector3.up);
            Gizmos.matrix = oldGizmosMatrix;
        }

#if UNITY_EDITOR

        private void OnValidate()
        {
            if (isActiveAndEnabled) OrganizeAllChildren(.5f);
            if (fixedCapacity > 0) nbDummies = fixedCapacity;
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(transform.position, size);
            Random.InitState(randomnessSeed);
            for (int i = 0; i < nbDummies; i++)
            {
                Gizmos.color = dummyColor + Color.white * i / nbDummies;
                var gizmoPosition = PositionAtIndex(i, true);
                var gizmoRotation = RotationAtIndex(i, true);
                DrawCube(gizmoPosition, Quaternion.Euler(gizmoRotation + transform.eulerAngles), scale * dummySize);
            }
        }

#endif

        #endregion

        #region Organize

        private IEnumerator Organize(List<GameObject> list, float duration, Action onComplete = null,
            bool iterative = true)
        {
            float delayStep = duration / list.Count;

            for (int i = 0; i < list.Count; i++)
            {
                GameObject go = list[i];
                if (go == null)
                    continue;
                if (go == Selection.CurrentSelection && ignoreCurrentSelection)
                    continue;
                Vector3 targetPosition = PositionAtIndex(i);
                Vector3 targetScale = ScaleAtIndex(i);
                Vector3 targetRotation = RotationAtIndex(i);

                if (angleInherited)
                {
                    targetRotation = go.transform.eulerAngles;
                }

                go.UpdateSortingOrder(i, sortingLayer);

                go.transform.DOMove(targetPosition, duration).SetEase(ease);
                go.transform.DOScale(targetScale, duration).SetEase(ease);
                //TODO: ? replace by Quaternion newRot = transform1.localRotation * Quaternion.AngleAxis(180, Vector3.up);
                //transform.DOLocalRotateQuaternion(newRot, rotationDuration)
                if (updateRotation) go.transform.DOLocalRotate(targetRotation, duration).SetEase(ease);
                if (iterative) yield return new WaitForSeconds(delayStep);
            }

            if (!iterative) yield return new WaitForSeconds(duration);
            if (debug) Debug.Log($"Zone animation {name} completed ");
            onComplete?.Invoke();
        }

        [Button]
        public void Organize()
        {
            OrganizeAllChildren(defaultDuration);
        }

        [Button]
        public void Shuffle()
        {
            List<GameObject> gos = transform.GetChildren().ShuffleIteratorNoPseudo().ToList();

            for (int i = gos.Count - 1; i >= 0; i--)
            {
                GameObject view = gos[i];

                view.transform.SetSiblingIndex(0);
            }

            //   transform.ShuffleChildrenOrder();
            OrganizeAllChildren(defaultDuration);
        }


        [Button]
        public void OrganizeImmediate()
        {
            OrganizeAllChildren(.5f, true);
        }

        public void OrganizeAnimation(List<GameObject> list, float duration, Action action = null,
            bool iterative = false)
        {
            StartCoroutine(Organize(list, duration, action, iterative));
        }

        public void Organize(float duration, Action action = null, bool iterative = false)
        {
            StartCoroutine(Organize(transform.GetChildren(), duration, action, iterative));
        }

        public void OrganizeAllChildren(float organizeDuration, bool immediate = false)
        {
            if (debug) Debug.Log($"Organize Layout {name} ");
            foreach (var (go, i) in transform.GetChildren().WithIndex())
            {
                StartCoroutine(OrganizeElementCoroutine(go, organizeDuration, i, immediate));
            }
        }

        public IEnumerator OrganizeOnlyElement(GameObject element, float organizeDuration)
        {
            int i = element.transform.GetSiblingIndex(); //elements.IndexOf(element);

            StartCoroutine(OrganizeElementCoroutine(element, organizeDuration, i));
            yield return new WaitForSeconds(organizeDuration);
        }

        public IEnumerator OrganizeElementsBefore(GameObject element, float organizeDuration)
        {
            int i = element.transform.GetSiblingIndex(); //elements.IndexOf(element);

            for (int j = 0; j <= i; j++)
            {
                GameObject elementToOrganize = transform.GetChild(j).gameObject;
                StartCoroutine(OrganizeElementCoroutine(elementToOrganize, organizeDuration, j));
            }

            yield return new WaitForSeconds(organizeDuration);
        }

        private IEnumerator OrganizeElementCoroutine(GameObject element, float organizeDuration, int i,
            bool immediate = false)
        {
            if (element == null)
                yield break;
            if (element == Selection.CurrentSelection && ignoreCurrentSelection)
                yield break;
            element.UpdateSortingOrder(i, sortingLayer);
            Random.InitState(randomnessSeed + i);

            Vector3 targetPosition = PositionAtIndex(i);
            Vector3 targetScale = ScaleAtIndex(i);
            Vector3 targetRotation = RotationAtIndex(i);

            if (immediate || !Application.isPlaying)
            {
                element.transform.position = targetPosition;
                if (updateRotation) element.transform.localEulerAngles = targetRotation;
                element.transform.localScale = targetScale; //.Divide (container.transform.lossyScale);
            }
            else
            {
                if (updateRotation) element.transform.DOLocalRotate(targetRotation, organizeDuration).SetEase(ease);
                ;
                element.transform.DOMove(targetPosition, organizeDuration).SetEase(ease);
                ;
                element.transform.DOScale(targetScale, organizeDuration).SetEase(ease);
            }

            yield return null;
        }

        #endregion

        #region DEBUG

        [Button]
        public void LogPosition()
        {
            foreach (var go in transform.GetChildren())
            {
                print($" position => {go.name} : {go.transform.position}");
            }
        }

        [Button]
        public void LogDummyPosition()
        {
            for (int i = 0; i < nbDummies; i++)
            {
                print("position =>" + i + PositionAtIndex(i, true));
            }
        }

        #endregion

        public void Add(GameObject go)
        {
            go.transform.SetParent(transform);
        }

        public void AddAtIndex(GameObject go, int index)
        {
            go.transform.SetParent(transform);
            go.transform.SetSiblingIndex(index);
        }

        public void AddAtEnd(GameObject go)
        {
            go.transform.SetParent(transform);
        }

        public Vector3 Position(GameObject cardSpawned)
        {
            if (cardSpawned.transform.parent != transform)
                return cardSpawned.transform.position;
            return PositionAtIndex(cardSpawned.transform.GetSiblingIndex());
        }

        // public void AddAtClosestPosition(GameObject source)
        // {
        //     int index = ClosestIndexFromPosition(source
        //         .transform.position);
        //     AddAtIndex(source, index);
        // }
    }
}