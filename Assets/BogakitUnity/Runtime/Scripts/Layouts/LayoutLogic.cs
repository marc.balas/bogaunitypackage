﻿using System;
using UnityEngine;

namespace BogakitUnity.Layouts
{
    public enum Direction4
    {
        None = -1,
        North = 0,
        Top = 0,
        East = 1,
        Right = 1,
        South = 2,
        Bottom = 2,
        West = 3,
        Left = 3,
        Last = 3,
        Random = 4,
        Undefined = 8,
    }
    [Serializable]
    public abstract class LayoutLogic : ScriptableObject
    {
        // public virtual bool IsIndexAccessible(int index)
        // {
        //     return true;
        // }
        //local transform position 
        public virtual Vector3 GetPositionAtIndex(int index, Layout layout, int size)
        {
            return layout.elementSpacing * index ;
        }

        //absolute transform position 
        public virtual Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            //hack to get rid of 360*N
            return Quaternion.Euler( Vector3.zero).eulerAngles;
        }

        
        public virtual Vector3 GetScaleAtIndex(int index, Layout zoneView, bool forGizmos = false)
        {
            return zoneView.elementScale;
        }

        public virtual void OnDrawGizmosSelected()
        {
        }

    }
}