using BogakitUnity.Selections;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

namespace BogakitUnity.Layouts
{
    public class LayoutPlaceholder : MonoBehaviour, IDeselectHandler
    {
        // public GameObject placeHolder;
        [Required] [SerializeField] private Layout layout;
        [SerializeField] private float duration = .25f;
        public GameObject targetToFollow;
        
        [Required] [SerializeField] private GameObject placeHolder;

        public Layout Layout => layout;

        // Start is called before the first frame update
        [Header("debug")] [SerializeField] private int spaceIndex = -1;


        private void Awake()
        {
            Assert.IsNotNull(layout);
            placeHolder.gameObject.SetActive(false);
        }

        // private void OnEnable()
        // {
        //     Selection.OnFocusChanged += OnFocusChanged;
        // }
        //
        // private void OnDisable()
        // {
        //     Selection.OnFocusChanged -= OnFocusChanged;
        // }
        //
        // private void OnFocusChanged(GameObject current, GameObject previous)
        // {
        //     //      Debug.Log($"OnFocusChanged: to {current} previous: {previous}");
        //
        //     if (current != layout.gameObject)
        //     {
        //         Stop();
        //     }
        //     else StartFollowing(EventSystem.current.currentSelectedGameObject);
        // }


        public void StartFollowing(GameObject target)
        {
            if (layout.IsFull)
            {
                placeHolder.gameObject.transform.SetParent(null);
                Debug.LogWarning($"Cant use placeholder on layout  {Layout}: full");
                return;
            }

            targetToFollow = target;
            placeHolder.gameObject.SetActive(true);
            placeHolder.transform.SetParent(layout.transform);
        }

        public void UpdateTarget()
        {
            if (RaycastSelector.IsPointerOverObject(layout.gameObject)
                //&& selection is over layout 
                )
                StartFollowing(EventSystem.current.currentSelectedGameObject);
            else
                Stop();
        }

        private void Update()
        {
            // if (!targetToFollow)
            // {
            //     if (spaceIndex >= 0)
            //         Stop();
            //     return;
            // }
            //get closest 
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            if (!targetToFollow)
            {
                Stop();
                return;
            }
            // if (layout.IsFull)
            // {
            //     gameObject.transform.SetParent(null);
            //     return;
            // }

            // the placeholder has to stay outside
            if (layout.HasFixedCapacity)
            {
                placeHolder.gameObject.transform.SetParent(null);
                int closestIndex = layout.ClosestIndexFromPosition(targetToFollow.transform.position);
                if (closestIndex != spaceIndex)
                {
                    placeHolder.gameObject.SetActive(true);
                    spaceIndex = closestIndex;
                    placeHolder.gameObject.transform.DOMove(layout.PositionAtIndex(spaceIndex), .5f);
                }
            }
            else
            {
                placeHolder.gameObject.transform.SetParent(layout.transform);
                int closestIndex = layout.ClosestIndexFromPosition(targetToFollow.transform.position);
                if (closestIndex != spaceIndex)
                {
                    placeHolder.gameObject.SetActive(true);
                    spaceIndex = closestIndex;
                    placeHolder.gameObject.transform.SetSiblingIndex(spaceIndex);
                    layout.OrganizeAllChildren(duration);
                }
            }
        }

        public void Stop()
        {
            if (!targetToFollow)
                return;

            placeHolder.gameObject.SetActive(false);
            placeHolder.gameObject.transform.SetParent(null);
            spaceIndex = -1;
            targetToFollow = null;
            layout.Organize();
        }

        public void OnDeselect(BaseEventData eventData)
        {
            Stop();
        }
    }
}