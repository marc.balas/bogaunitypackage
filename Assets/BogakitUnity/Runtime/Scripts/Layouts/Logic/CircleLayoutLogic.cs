﻿using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    [CreateAssetMenu(fileName = "LL_CircleLayoutLogic", menuName = "BogaKit/LayoutLogics/CircleLayoutLogic", order = 35)]
    public class CircleLayoutLogic : LayoutLogic
    {
        public enum CircleLayoutStyle
        {
            AngleSpacing,
            Fit,
        }

        public CircleLayoutStyle style = CircleLayoutStyle.AngleSpacing;

        [Header("settings")] [SerializeField] private float angleSpacing = 15f;
        [SerializeField] private float radius = 5.0f;

        [SerializeField] private bool overrideElementAngle = true;
        [SerializeField] private Direction4 arcOrientation;
        [SerializeField] private bool fitWhenOverflow = true;

        private int AngleForOrientation => 90 * (int) arcOrientation;

        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            if (!overrideElementAngle)
            {
                return base.GetRotationAtIndex(index, zoneView, size);
            }

            float startAngle = AngleForOrientation;
           // int nbEntities = size ? layout.NbDummies : layout.NbSlots;


            if (style == CircleLayoutStyle.AngleSpacing)
            {
                if (size % 2 == 0)
                {
                    startAngle = -size / 2 * angleSpacing + 0.5f * angleSpacing + AngleForOrientation;
                }
                else
                {
                    startAngle = -size / 2 * angleSpacing + AngleForOrientation;
                }
            }

            //print ("angle for i " + index + "  " + (index * angleSpacing + startAngle).ToString() );
            Vector3 angle = Vector3.zero;
            if (style == CircleLayoutStyle.AngleSpacing)
            {
                angle = new Vector3(0, 0, index * angleSpacing + startAngle);
            }
            else
            {
                //if (style == ArrangeStyle.Fit) {

                startAngle = +AngleForOrientation;
                float angleOffset = 0;
                if (size >= 2) angleOffset = 360 / (size - 1);
                angle = new Vector3(0, 0, (index) * angleOffset + startAngle);
            }

            return -angle + index * zoneView.angleIncrement;
        }

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int nbEntities)
        {
            float startAngle = AngleForOrientation;
            //int nbEntities = size ;// ? layout.nbDummies : layout.NbSlots;
            if (style == CircleLayoutStyle.AngleSpacing)
            {
                if (nbEntities % 2 == 0)
                {
                    startAngle = -nbEntities / 2 * angleSpacing + 0.5f * angleSpacing + AngleForOrientation;
                }
                else
                {
                 
                    startAngle = -nbEntities / 2 * angleSpacing + AngleForOrientation;
                }
            }

            Vector3 pos;
            if (style == CircleLayoutStyle.AngleSpacing && ((nbEntities - 1) * angleSpacing < 360 || !fitWhenOverflow))
            {
                pos = new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (index * angleSpacing + startAngle)),
                          radius * Mathf.Cos(Mathf.Deg2Rad * (index * angleSpacing + startAngle)), 0); // +boundingBox. 
            }
            else
            {
                startAngle = AngleForOrientation;
                float angle = nbEntities > 1 ?  angle = 360 / ((float) nbEntities - 1) : 0;
                pos = new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * ((index) * angle + startAngle)),
                    radius * Mathf.Cos(Mathf.Deg2Rad * ((index) * angle + startAngle)), 0);
            }

            return pos  + zone.elementSpacing * index ;
        }
    }
}