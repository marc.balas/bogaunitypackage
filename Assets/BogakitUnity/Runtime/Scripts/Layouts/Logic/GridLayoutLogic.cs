﻿

using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
public class GridLayoutLogic : LayoutLogic {


	public enum FillPriority {RowFirst,ColumnFirst };

	public enum FillStyle {UseSpacing,Fit };


		[SerializeField] private int height = 3;
        [SerializeField] private int width = 3;
        [SerializeField] private float relativeColumnSpacing = .1f; //negative means overlap of elements
        [SerializeField] private float relativeRowSpacing = .1f; //negative means overlap of elements

        [HideInInspector]public FillPriority priority = FillPriority.RowFirst;

		public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
		{
			int nb = index/width;
			int left = index%width;
			Vector3 currentPos =  index * zone.elementSpacing ;
			if (priority == FillPriority.RowFirst) {
				currentPos.x += relativeColumnSpacing * zone.offset.x * left;
				currentPos.y -= relativeRowSpacing * zone.offset.y * nb;

			} else {
				nb = index/height;
				left = index%height;
				currentPos.x += relativeColumnSpacing * zone.offset.x * nb;
				currentPos.y -= relativeRowSpacing * zone.offset.y * left;
			}
			return currentPos ;

		}

}
}