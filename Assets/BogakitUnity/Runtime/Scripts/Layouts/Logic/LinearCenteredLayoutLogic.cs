﻿using System;
using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    [CreateAssetMenu(fileName = "LL_LinearCentered", menuName = "BogaKit/LayoutLogics/LinearCentered", order = 35)]
    [Serializable]
    public class LinearCenteredLayoutLogic : LayoutLogic
    {


        [Tooltip("spacing reduction when nb elements increase, in %of spacing")]
        public int contractionFactor;
        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {
            int nb = size;
            
            float offset = nb%2 == 0 ? -0.5F : 0;
            return  zone.elementSpacing * (- (nb/2 + offset - index) * (1 - size*contractionFactor/100));

        }
        
        



     
    }
}
