﻿using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    public enum LinearLayoutStyle
    {
        RelativeSpacing,
        Fit,
        SpacingPresetList
    }

    [CreateAssetMenu(fileName = "LL_Linear", menuName = "BogaKit/LayoutLogics/LinearLayout", order = 35)]
    public class LinearLayoutLogic : LayoutLogic
    {
        //  [SerializeField] private TextAnchor anchor = TextAnchor.MiddleCenter;
        [SerializeField] private LinearLayoutStyle spacingStyle;

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {
            if(size == 0)
                return Vector3.zero;
            
            if (spacingStyle == LinearLayoutStyle.RelativeSpacing)
                return Vector3.Scale(zone.elementSpacing * index, zone.size);
            else if (spacingStyle == LinearLayoutStyle.Fit)
            {
                float ratio = (float)index / size;
                return Vector3.Scale(Vector3.one*ratio, zone.size);
            }
            
            return Vector3.Scale(zone.elementSpacing * index, zone.size);
        }


        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            //hack to get rid of 360*N
            return Quaternion.Euler(index * zoneView.angleIncrement).eulerAngles;
        }

        // public override TextAnchor GetAnchor()
        // {
        //     return anchor;
        // }
    }
}