﻿using System.Collections.Generic;
using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
   [CreateAssetMenu(fileName = "LL_PresetLayout", menuName = "BogaKit/LayoutLogics/Preset Layout", order = 35)]

   public  class PresetLayoutLogic : LayoutLogic
    {

  [SerializeField]    private List<GameObject> PresetPositions = new List<GameObject> ();


        // public override bool IsIndexAccessible(int index)
        // {
        //     return true;
        //
        // }


        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            if (index < PresetPositions.Count) {
                GameObject dummy = PresetPositions [index];
                if (dummy) {
                    dummy.SetActive (index < size);
                    return dummy.transform.eulerAngles;
                }
            } 


                return Vector3.zero;

        }

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {
            if (index < PresetPositions.Count) {
                GameObject dummy = PresetPositions [index];
                if (dummy) {
                    dummy.SetActive (true);
                    return dummy.transform.position ;
                }
            }

            return Vector3.zero;

        }

        public override Vector3 GetScaleAtIndex(int index, Layout zoneView, bool forGizmos = false)
        {
            if (index < PresetPositions.Count) {
                GameObject dummy = PresetPositions [index];
                if (dummy) {
                    dummy.SetActive (forGizmos);
                    return  Vector3.Scale( dummy.transform.localScale , zoneView.elementScale) ;
                }
            } 

            return zoneView.elementScale;

        }

    }
}
