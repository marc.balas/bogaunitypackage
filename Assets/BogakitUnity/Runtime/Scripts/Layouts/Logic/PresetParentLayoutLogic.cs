﻿using System.Collections.Generic;
using System.Linq;
using BogakitUnity.Extensions;
using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
   [CreateAssetMenu(fileName = "LL_PresetParentLayoutLogic", menuName = "BogaKit/LayoutLogics/Preset Parent Layout", order =35)]

   
   //use another transform (positionReferenceParent) and its children as references for preset positions
   public  class PresetParentLayoutLogic : LayoutLogic
    {
        [SerializeField] private bool overwriteScale;
        [SerializeField] private bool overwriteRotation;
        [SerializeField] private string nameOfGameObjectWithPresets;

   private List<Transform> _presetPositions = new List<Transform> ();

        // public override bool IsIndexAccessible(int index)
        // {
        //     return true;
        //
        // }
        
        // public override int GetMaxCapacity(Layout layout)
        // {
        //     return _presetPositions.Count;
        //
        // }

        private  List<Transform> AssignPresetTransforms()
        {
            if (_presetPositions == null) return GameObject.Find(nameOfGameObjectWithPresets).transform.GetChildren().Select(g => g.transform).ToList();
            else return _presetPositions;
        }

        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            _presetPositions = AssignPresetTransforms();
            if (index < _presetPositions.Count && overwriteRotation) {
                Transform dummy = _presetPositions [index];
                if(dummy)
                    return dummy.eulerAngles;
                
            } 


                return Vector3.zero;

        }

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {
            _presetPositions =   AssignPresetTransforms();
            if (index < _presetPositions.Count) {
                Transform dummy = _presetPositions [index];
                if (dummy) {
                  
                    return dummy.transform.position;
                }
            }

            return Vector3.zero;

        }

        public override Vector3 GetScaleAtIndex(int index, Layout zoneView, bool forGizmos = false)
        {
            _presetPositions =   AssignPresetTransforms();
            if (index < _presetPositions.Count && overwriteScale) {
                Transform dummy = _presetPositions [index];
                if (dummy) {
                    
                    return  Vector3.Scale( dummy.transform.localScale , zoneView.elementScale) ;
                }
            } 

            return zoneView.elementScale;

        }

    }
}
