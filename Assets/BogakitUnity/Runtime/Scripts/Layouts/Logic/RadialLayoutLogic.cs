﻿using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    [CreateAssetMenu(fileName = "LL_RadialLayout", menuName = "BogaKit/LayoutLogics/Radial Layout", order = 35)]
    public  class RadialLayoutLogic : LayoutLogic
    {
        public enum RadialLayoutStyle
        {
            AngleSpacing,
            Fit,
        }
        [HideInInspector] public RadialLayoutStyle style = RadialLayoutStyle.AngleSpacing;

        [Header("Radial settings")]
        public float maxAngle = 45.0f;
        public float radius = 5.0f;
        public float angleSpacing = 15f;

        public bool overrideElementAngle = true;
        public Direction4 arcOrientation;
        public TextAlignment alignment = TextAlignment.Center;
        public bool fitWhenOverflow = true;

        public int angleForOrientation => 90 * (int)arcOrientation;

        public Vector3 offsetForOrientation => new Vector3(-Mathf.Sin(Mathf.Deg2Rad * angleForOrientation) * radius, -Mathf.Cos(Mathf.Deg2Rad * angleForOrientation) * radius, 0);


        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {

            if (!overrideElementAngle)
            {
                return base.GetRotationAtIndex(index,zoneView, size);
            }

            float startAngle = -maxAngle / 2 + angleForOrientation;
          //  int nbEntities = size ?  layout.NbDummies: layout.NbSlots;
            
            
            if (style == RadialLayoutStyle.AngleSpacing && alignment == TextAlignment.Center)
            {
                if (size % 2 == 0)
                {
                    startAngle = -size / 2 * angleSpacing + 0.5f * angleSpacing + angleForOrientation;
                }
                else
                {
                    startAngle = -size / 2 * angleSpacing + angleForOrientation;
                }
            }
            else if (alignment == TextAlignment.Right)
            {

                startAngle = maxAngle / 2 - (size - 1) * angleSpacing + angleForOrientation;
            }
            //print ("angle for i " + index + "  " + (index * angleSpacing + startAngle).ToString() );
            Vector3 angle = Vector3.zero;
            if (style == RadialLayoutStyle.AngleSpacing && (size * angleSpacing < maxAngle || !fitWhenOverflow))
            {
                angle = new Vector3(0, 0, index * angleSpacing + startAngle);
            }
            else
            {//if (style == ArrangeStyle.Fit) {

                startAngle = -maxAngle / 2 + angleForOrientation;
                float angleOffset = 0;
                if (size >= 2) angleOffset = maxAngle / (size -1);
                angle = new Vector3(0, 0, (index) * angleOffset + startAngle);
            }

            if (size == 1)
            {
                if (alignment == TextAlignment.Center)
                    return Vector3.zero;

            }
            
            return -angle + index * zoneView.angleIncrement;
        }

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int nbEntities)
        {

            float startAngle = -maxAngle / 2 + angleForOrientation;
         //   int nbEntities = size ? layout.nbDummies : layout.NbSlots;
            if (style == RadialLayoutStyle.AngleSpacing && alignment == TextAlignment.Center)
            {
                if (nbEntities % 2 == 0)
                {
                    startAngle = -nbEntities / 2 * angleSpacing + 0.5f * angleSpacing + angleForOrientation;
                }
                else
                {
                    startAngle = -nbEntities / 2 * angleSpacing + angleForOrientation;
                }
            }
            else if (alignment == TextAlignment.Right)
            {

                startAngle = maxAngle / 2 - (nbEntities - 1) * angleSpacing + angleForOrientation;
            }
            Vector3 pos = Vector3.zero;
            if (style == RadialLayoutStyle.AngleSpacing && ((nbEntities - 1) * angleSpacing < maxAngle || !fitWhenOverflow))
            {
                pos = new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * (index * angleSpacing + startAngle)), radius * Mathf.Cos(Mathf.Deg2Rad * (index * angleSpacing + startAngle)), 0) + zone.elementSpacing * index ;// +boundingBox. 
            }
            else
            {//if (style == ArrangeStyle.Fit) {
                startAngle = -maxAngle / 2 + angleForOrientation;
                float angle = 0;

                if (nbEntities >= 2)
                {
                    angle = maxAngle / ((float)nbEntities - 1);
                }

                pos = new Vector3(radius * Mathf.Sin(Mathf.Deg2Rad * ((index) * angle + startAngle)), radius * Mathf.Cos(Mathf.Deg2Rad * ((index) * angle + startAngle)), 0) + zone.elementSpacing * index;//+ boundingBox.transform.position
            }
            //print ("angle for i " + index + "  " + (index * angleSpacing + startAngle).ToString() );
            return pos+ offsetForOrientation ;
        }
    }
}

