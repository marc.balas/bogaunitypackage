﻿using UnityEngine;
using UnityEngine.Assertions;
using Random = System.Random;

namespace BogakitUnity.Layouts.Logic
{
    [CreateAssetMenu(fileName = "LL_RandomLayout", menuName = "BogaKit/LayoutLogics/Random Layout", order = 35)]
    public class RandomLayoutLogic : LayoutLogic
    {
        [Header("Random settings")] [SerializeField]
        private int seed = 0;
        [SerializeField] private bool randomAngle = false;
        private Collider2D containerBoundaries;
        private Random random;

        public Vector3 PointInArea(int index, Layout zoneView)
        {
            containerBoundaries = zoneView.GetComponent<Collider2D>();
            Assert.IsNotNull(containerBoundaries);
            var boundingBox = containerBoundaries.bounds.size; //meshObject.transform.GetComponent<Collider2D> ().bounds.size;

            //hack to keep synchro random values , between gizmos and elements 
            random = new Random(seed);
            UnityEngine.Random.InitState(seed + index);
            int attempt = 0;
            Vector2 position;
            Vector3 randomRelativePos;
            do
            {
                randomRelativePos = new Vector3(UnityEngine.Random.Range(-boundingBox.x / 2, boundingBox.x / 2), UnityEngine.Random.Range(-boundingBox.y / 2, boundingBox.y / 2),
                    UnityEngine.Random.Range(-boundingBox.z / 2, boundingBox.z / 2));
                attempt++;
                position = new Vector3(randomRelativePos.x, randomRelativePos.y, 0) + containerBoundaries.transform.position;
            } while (!containerBoundaries.OverlapPoint(position) && attempt <= 10);

            return randomRelativePos;
        }


        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {
            return PointInArea(index, zone) + index * zone.elementSpacing;
        }

        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {
            if (randomAngle)
            {
                return new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
            }

            return Vector3.zero;
        }
    }
}