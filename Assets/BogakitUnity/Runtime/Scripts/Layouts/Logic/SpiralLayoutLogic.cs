﻿using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    [CreateAssetMenu(fileName = "LL_SpiralLayout ", menuName = "BogaKit/LayoutLogics/Spiral Layout", order = 35)]
    public class SpiralLayoutLogic : LayoutLogic
    {


        [SerializeField] private int n = 10;
        [SerializeField] private float radius = 1f;
        [SerializeField] private float angleStep = 0.2f;  //in degrees
        [SerializeField] private bool useGoldenAngle = false;


        private float golden_angle = Mathf.PI * (3 - Mathf.Sqrt(5));

        public override Vector3 GetPositionAtIndex(int index, Layout zone, int size)
        {

            float theta = angleStep * index * Mathf.Deg2Rad;
            if (useGoldenAngle)
            {
                theta = golden_angle * index;
            }

            //float theta = index * golden_angle;
            float r = Mathf.Sqrt(index) / Mathf.Sqrt(n) * radius;
            return new Vector3(r * Mathf.Cos(theta), r * Mathf.Sin(theta), 0) + index * zone.elementSpacing ;
        }


        public override Vector3 GetRotationAtIndex(int index, Layout zoneView, int size)
        {

            //return Vector3.zero;
            float theta = angleStep * index * Mathf.Deg2Rad;
            if (useGoldenAngle)
            {
                theta = golden_angle * index;
            }
            return index * zoneView.angleIncrement + new Vector3(0, 0, Mathf.Rad2Deg * theta);
        }

    }
}
