﻿using UnityEngine;

namespace BogakitUnity.Layouts.Logic
{
    public class SplineLayoutLogic : LayoutLogic
    {
        public enum SplineStyle
        {
            Fit,
            RelativeSpacing,
            RelativeSpacingList,
        }

        //spacing between element center or spacing between element bounds
        [HideInInspector] public SplineStyle style = SplineStyle.RelativeSpacing;


//
//		public override Vector3 GetPositionAtIndex (int index, bool forGizmos = false)
//		{
//			Vector3 pos = Vector3.zero;
//			int nbEntities = forGizmos ? nbDummies : nbSlots;// + nbLockedIndexes;
//			//nbEntities += nbSpaces;
//			if (style == LinearLayoutStyle.RelativeSpacing) {
//				if (forGizmos && fitWhenFull && nbEntities * relativeLinearSpacing > 1.0f) {
//					return FitPositionAtIndex (index, forGizmos);
//
//				} else if (!forGizmos && fitWhenFull && nbEntities * relativeLinearSpacing > 1.0f) {
//					return FitPositionAtIndex (index, forGizmos);
//
//				} else {
//					
//					pos = (SpacingIncrement + linearSize * relativeLinearSpacing) * index + CalculateAnchor () - CalculateAroundAnchor (nbEntities);
//
//				}
//			} else if (style == LinearLayoutStyle.Fit) {
//				return FitPositionAtIndex (index, forGizmos);
//			} else if (style == LinearLayoutStyle.SpacingPresetList) {
//				Vector3 sumOfAbsSpacing = Vector3.zero;
//
//				if (index > RelativeSpacingList.Count) {
//					return Vector3.zero;
//				}
//				for (int i = 0; i < index; i++) {
//					sumOfAbsSpacing += linearSize * RelativeSpacingList [i];
//				}
//			
//				pos = sumOfAbsSpacing + SpacingIncrement * index + CalculateAnchor ();
//			}
//			return pos;
//		}
//
    }
}