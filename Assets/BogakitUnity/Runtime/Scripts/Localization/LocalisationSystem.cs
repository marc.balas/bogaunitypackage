using System;
using System.Collections.Generic;

namespace BogakitUnity.Localization
{
    public static class LocalisationSystem
    {
        public enum Language
        {
            English,
            French
        }

        public static Language currentLanguage = Language.English;
        private static Dictionary<string, string> localisedEN;
        private static Dictionary<string, string> localisedFR;

        private static bool IsInit;
        private static CSVLoader CsvLoader;
        public static void Init()
        {
            CsvLoader = new CSVLoader();
            CsvLoader.LoadCSV();
            
            UpdateDictionaries();

            IsInit = true;
        }

        private static void UpdateDictionaries()
        {
            localisedEN = CsvLoader.GetDictionaryValues("en");
            localisedFR = CsvLoader.GetDictionaryValues("fr");
        }

        public static string GetLocalisedValue(string key)
        {
            if (!IsInit)
            {
                Init();
            }

            string value = key;

            switch (currentLanguage)
            {
                case Language.English:
                    localisedEN.TryGetValue(key, out value);
                    break;
                case Language.French:
                    localisedFR.TryGetValue(key, out value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return value;
        }
#if UNITY_EDITOR
        public static void Add(string key, string value)
        {
            if (value.Contains("\""))
            {
                value = value.Replace('\"','"');
            }

            if (CsvLoader == null)
            {
                CsvLoader = new CSVLoader();
            }
            CsvLoader.LoadCSV();
            CsvLoader.Add(key,value);
            CsvLoader.LoadCSV();
            UpdateDictionaries();
        }
        
       
        public static void Edit(string key, string value)
        {
            if (value.Contains("\""))
            {
                value = value.Replace('\"','"');
            }

            if (CsvLoader == null)
            {
                CsvLoader = new CSVLoader();
            }
            CsvLoader.LoadCSV();
            CsvLoader.Edit(key,value);
            CsvLoader.LoadCSV();
            UpdateDictionaries();
        }
        
        public static void Remove(string key)
        {
            if (CsvLoader == null)
            {
                CsvLoader = new CSVLoader();
            }
            CsvLoader.LoadCSV();
            CsvLoader.Remove(key);
            CsvLoader.LoadCSV();
            UpdateDictionaries();
        }
#endif
        public static Dictionary<string, string> GetDictionaryForEditor()
        {
            if(!IsInit){Init();}

            return localisedEN;
        }
    }
}