using TMPro;
using UnityEngine;

namespace BogakitUnity.Localization
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextLocaliserUI : MonoBehaviour
    {
        private TextMeshProUGUI textField;

        
        [SerializeField]
        private LocalisedString localisedString;

        private void Start()
        {
            textField = GetComponent<TextMeshProUGUI>();
            //string value = LocalisationSystem.GetLocalisedValue(key);
            textField.text = localisedString.value;
        }

        public void UpdateKey(string newKey)
        { 
            localisedString = new LocalisedString(newKey);
        }
    }
}