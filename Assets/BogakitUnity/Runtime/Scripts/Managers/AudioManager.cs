﻿using System;
using System.Collections;
using NaughtyAttributes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BogakitUnity.Managers
{
    [Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
        [Range(0, 1f)] public float volume = 0.5f;
        [Range(0.1f, 3f)] public float pitch = 1f;

        [HideInInspector] public AudioSource source;

        public bool loop;
           
        [Button("Play Clip")]
        public void Play()
        {
            //Play the clip entirely.
            //AudioManager.PlayClip(clip);
            source.clip = clip;
            source.Play();
        }
    }


    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance;

        [SerializeField]private AudioSource efxSource; //Drag a reference to the audio source which will play the sound effects.
        [SerializeField]private AudioSource musicSource; //Drag a reference to the audio source which will play the music.
        [SerializeField]private SoundConfig config;


        public void ActivateVolume(bool volumeOn)
        {
            efxSource.mute = !volumeOn;
        }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                //	Debug.LogError("AudioManager already exist: destroy object " + gameObject.name);
                Destroy(gameObject);
                return;
            }

//            DontDestroyOnLoad(gameObject);
            if(config == null)
                return;
            foreach (Sound s in config.sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;
                s.source.volume = s.volume;
                s.source.pitch = s.pitch;
                s.source.loop = s.loop;
            }
        }

        //Used to play single sound clips.
        public static void PlayClip(AudioClip clip)
        {
            //Play the clip entirely.
            Instance.efxSource.PlayOneShot(clip);
        }
     


        //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
        public void RandomizeSfx(params AudioClip[] clips)
        {
            //Generate a random number between 0 and the length of our array of clips passed in.
            int randomIndex = Random.Range(0, clips.Length);

            //Choose a random pitch to play back our clip at between our high and low pitch ranges.
          // float randomPitch = Random.Range(soundSettings.lowPitchRange, soundSettings.highPitchRange);

            //Set the pitch of the audio source to the randomly chosen pitch.
         //  efxSource.pitch = randomPitch;

            //Set the clip to the clip at our randomly chosen index.
            efxSource.clip = clips[randomIndex];

            //Play the clip.
            efxSource.Play();
        }

        public IEnumerator PlayAfter(string soundName, float delay)
        {
            yield return new WaitForSeconds(delay);
            Play(soundName);
        }


        public static void Play(string soundName)
        {
            
            if (Instance == null)
            {
                Debug.LogWarning("No AudioManager to play '" + soundName + "'");
                return;
            }
            
            if (Instance.config == null)
            {
                Debug.LogWarning("No AudioManager config '" + soundName + "'");
                return;
            }
            if (soundName == "")
            {
                Debug.LogWarning("Empty sound name ");
                return;
            }
            Sound s = Instance.config.sounds.Find(sound => sound.name == soundName);
            if (s == null)
            {
                Debug.LogError("Could not find & sound with name '" + soundName + "'");
            }
            else
            {
                Instance.efxSource.volume = s.volume;
                PlayClip(s.clip);
            }
        }


        public void Stop(string soundName)
        {
            Sound s = config.sounds.Find(sound => sound.name == soundName);
            if (s == null)
            {
                Debug.LogError("Could not find & stop sound with name '" + soundName + "'");
            }
            else
            {
                s.source.Stop();
            }
        }
    }
}