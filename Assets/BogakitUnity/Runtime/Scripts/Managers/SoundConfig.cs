using System.Collections.Generic;
using UnityEngine;

namespace BogakitUnity.Managers
{
    [CreateAssetMenu(fileName = "SoundConfig", menuName = "BogaKit/SoundConfig", order = 0)]
    public class SoundConfig : ScriptableObject
    {
        
        public List<Sound> sounds;

    }
}