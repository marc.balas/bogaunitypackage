using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using NaughtyAttributes;
using TMPro;
using UnityEngine;

namespace BogakitUnity
{
    public class LoadingTips : MonoBehaviour
    {
      
        [SerializeField] private TextMeshProUGUI _tipsLabel;
        [SerializeField] private List<string> tips;
        [SerializeField] private float tipsDuration;
        [Tooltip("loop if < 0")]
        [SerializeField] private int nbTips;
        [SerializeField] private bool pauseGame;
    
        [SerializeField] private GameObject container;
    
        private Queue<string> tipsShuffled = new Queue<string>();
        private string currentTips ;
        // Start is called before the first frame update
        IEnumerator Start()
        {
            if(pauseGame)Time.timeScale = 0f;
            container.SetActive(true);
            if (tipsShuffled.Count == 0)
            {
          
                if(nbTips>0) tipsShuffled = new Queue<string>(tips.ShuffleIteratorNoPseudo().Take(nbTips));
                else   tipsShuffled = new Queue<string>(tips.ShuffleIteratorNoPseudo());

            }

            while(tipsShuffled.Count>0) {
            
                currentTips = tipsShuffled.Dequeue();
                _tipsLabel.text = currentTips;
                yield return new WaitForSecondsRealtime(tipsDuration);
            }
            _tipsLabel.text = "";
            container.SetActive(false);
            Time.timeScale = 1f;
        }

        [Button("Show tips")]
        void ShowTips()
        {
            StartCoroutine(Start());
        }
    
        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
