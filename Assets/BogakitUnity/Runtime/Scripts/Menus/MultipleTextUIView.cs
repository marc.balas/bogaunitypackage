using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace BogakitUnity.UI
{
    public class MultipleTextUIView : MonoBehaviour
    {
    
        [SerializeField]
        private List<TextMeshProUGUI> textList ; 
   
        public void Populate(List<string> texts)
        {
            if(texts.Count < textList.Count)
                Debug.LogWarning($"{name} too many texts to display: missing TextMeshProUGUI");
            
            // iterate on the smaller list
            for (int i = 0; i < Math.Min(texts.Count, textList.Count); i++)
            {
                TextMeshProUGUI textMeshProUGUI = textList[i];
                textMeshProUGUI.text = texts[i];
            }
        }
    }
}
