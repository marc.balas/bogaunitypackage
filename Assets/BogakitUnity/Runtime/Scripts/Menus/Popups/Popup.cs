﻿﻿using UnityEngine;

 namespace BogakitUnity.UI.Popups
{
    public class Popup : MonoBehaviour
    {
        [HideInInspector]
        public PopupManager parentScene;

        public void Close()
        {
            if (parentScene != null)
            {
                parentScene.OnPopupClosed();
            }
            Destroy(gameObject);
        }
    }
}