﻿using TMPro;
using UnityEngine.UI;

namespace BogakitUnity.UI.Popups
{
    public class PopupLoading : Popup
    {
        public TextMeshProUGUI text;
        public Slider progressBar;
        public TextMeshProUGUI progressText;
        
    }
}