﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace BogakitUnity.UI.Popups
{
    public class PopupManager : MonoBehaviour
    {
        public GameObject currentPopup { get; protected set; }

        [SerializeField]
        protected Canvas canvas;

        [SerializeField]
        [Tooltip("container for the popup")]
        protected CanvasGroup panelCanvasGroup;

        protected void Awake()
        {
            Assert.IsNotNull(canvas);
            Assert.IsNotNull(panelCanvasGroup);
        }

        protected void OnEnable()
        {
          //  OnPopupClosed();
        }

        public void OpenPopup<T>(string popUpName, Action<T> onOpened = null, bool darkenBackground = true) where T : Popup
        {
            if (currentPopup != null)
            {
                Debug.LogWarning("a popup is already opened");
             return;   
            }
            
            StartCoroutine(OpenPopupAsync(popUpName, onOpened, darkenBackground));
        }

        public void ClosePopup()
        {
            if (currentPopup != null)
            {
                Destroy(currentPopup);
                currentPopup = null;
                panelCanvasGroup.blocksRaycasts = false;
                panelCanvasGroup.GetComponent<Image>().DOKill();
                panelCanvasGroup.GetComponent<Image>().DOFade(0.0f, 0.2f);
            }
        }

        private IEnumerator OpenPopupAsync<T>(string namePopup, Action<T> onOpened, bool darkenBackground) where T : Popup
        {
            var request = Resources.LoadAsync<GameObject>(namePopup);
            while (!request.isDone)
            {
                yield return null;
            }

            if (request.asset == null)
            {
                Debug.LogError($"Popup {namePopup} not found in resources");
            }

            currentPopup = Instantiate(request.asset, canvas.transform, false) as GameObject;
            Assert.IsNotNull(currentPopup,$"Popup with name {namePopup} is null ");
            currentPopup.GetComponent<Popup>().parentScene = this;
            if (darkenBackground)
            {
                panelCanvasGroup.gameObject.SetActive( true);
                panelCanvasGroup.blocksRaycasts = true;
                panelCanvasGroup.GetComponent<Image>().DOFade(0.5f, 0.5f);
            }

            onOpened?.Invoke(currentPopup.GetComponent<T>());
        }

        public void OnPopupClosed()
        {
            panelCanvasGroup.blocksRaycasts = false;
            panelCanvasGroup.GetComponent<Image>().DOKill();
            panelCanvasGroup.GetComponent<Image>().DOFade(0.0f, 0.25f);
            ClosePopup();
        }
    }
}
