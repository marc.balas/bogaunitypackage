﻿using TMPro;

namespace BogakitUnity.UI.Popups
{
 

    public class PopupOneButton : Popup
    {
        public TextMeshProUGUI text;
        public MenuButton button;
        public TextMeshProUGUI buttonText;
    }
}