﻿
using TMPro;

namespace BogakitUnity.UI.Popups
{
    public class PopupPassword : Popup
    {
        public TMP_InputField inputField;
        public MenuButton button;
    }
}