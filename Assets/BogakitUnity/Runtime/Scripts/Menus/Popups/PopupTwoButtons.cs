﻿using TMPro;

namespace BogakitUnity.UI.Popups
{
    public class PopupTwoButtons : PopupOneButton
    {
        public MenuButton button2;
        public TextMeshProUGUI button2Text;
    }
}