using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace BogakitUnity.UI
{
    public class ProgressLoader : MonoBehaviour
    {
    
    
        [SerializeField]
        public TextMeshProUGUI loaderTitle;
        [SerializeField]
        public TextMeshProUGUI loaderText;
        [SerializeField]
        public Slider loadingSlider;

        private void Awake()
        {
            Assert.IsNotNull(loaderText);
            Assert.IsNotNull(loaderTitle);
            Assert.IsNotNull(loadingSlider);
            loadingSlider.value = 0.0f;
        }
    }
}
