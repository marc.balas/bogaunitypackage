﻿using BogakitUnity.Managers;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BogakitUnity.UI
{
    public class SimpleButton : MonoBehaviour
    {
      
        [Header("Sounds")]
        [SerializeField]
        private AudioClip MouseEnterSound;

        [SerializeField]
        private AudioClip MouseClickSound;

        [SerializeField]
        private AudioClip MouseExitSound;

        [SerializeField]
        private UnityEvent onClick;

        [SerializeField]
        private bool MaskedByUI;

        private void Awake()
        {
            Assert.IsTrue(GetComponent<Collider>() != null || GetComponent<Collider2D>() != null, name + "has no collider");
        }

        private void Update()
        {
            
        }

        public void OnMouseEnter()
        {
            if (MaskedByUI && EventSystem.current.IsPointerOverGameObject())
                return;

            if (enabled)
            {
                if (MouseEnterSound) AudioManager.PlayClip(MouseEnterSound);
            }
        }

        private void OnMouseExit()
        {
            if (enabled)
            {
                if (MouseExitSound) AudioManager.PlayClip(MouseExitSound);
            }
        }

        private void OnMouseDown()
        {
            if (MaskedByUI && EventSystem.current.IsPointerOverGameObject())
                return;

            if (enabled)
            {
                if (MouseClickSound) AudioManager.PlayClip(MouseClickSound);
                onClick?.Invoke();
            }
        }

    }
}