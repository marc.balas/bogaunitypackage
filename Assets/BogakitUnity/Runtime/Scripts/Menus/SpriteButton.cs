﻿using BogakitUnity.Extensions;
using BogakitUnity.Managers;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BogakitUnity.UI
{
    
    [RequireComponent(typeof(Collider2D))]
    public class SpriteButton : MonoBehaviour
    {
        [SerializeField] private bool active;
        [SerializeField] private SpriteRenderer hoverSprite;
        [SerializeField] private SpriteRenderer enableSprite;
        [SerializeField] private SpriteRenderer disabledSprite;
        [SerializeField] private SpriteRenderer clickSprite;

        [SerializeField] private float fadeInDuration = .3f;
        [SerializeField] private float fadeOutDuration = .3f;

        [SerializeField] private bool UseCoolDown;
        [SerializeField] private float coolDown;
        private bool HasCoolDown => UseCoolDown && coolDown > 0;

        [Header("Sounds")] [SerializeField] private AudioClip MouseEnterSound;
        [SerializeField] private AudioClip MouseClickSound;
        [SerializeField] private AudioClip MouseExitSound;
        [SerializeField] private UnityEvent onClick;
        [SerializeField] private bool MaskedByUI;

        private void Awake()
        {
            // Assert.IsNotNull(enableSprite);
            // Assert.IsNotNull(hoverSprite);
            // Assert.IsNotNull(disabledSprite);
            Assert.IsTrue(GetComponent<Collider>() != null || GetComponent<Collider2D>() != null,
                name + "has no collider");
        }

        private void Start()
        {
            SetActive(active);
        }

        [Button]
        public void ToggleActive()
        {
            SetActive(!active);
        }

        public void SetActive(bool isEnabled)
        {
          
            clickSprite.enabled = false;
            active = isEnabled;
            if (disabledSprite) disabledSprite.enabled = !isEnabled;
            enableSprite.enabled = isEnabled;
            if(hoverSprite)hoverSprite.enabled = isEnabled;
            if (isEnabled)
            {
                enableSprite.DOKill();
                enableSprite.DOFade(1.0f, 0.4f);
            }
            else
            {
                enableSprite.SetAlpha(0);
             
            }
            hoverSprite.DOKill();
            hoverSprite.DOFade(0.0f, 0.2f);

         
        }

        public void OnMouseEnter()
        {
            if (MaskedByUI && EventSystem.current.IsPointerOverGameObject())
                return;

            if (active)
            {
                if (MouseEnterSound) AudioManager.PlayClip(MouseEnterSound);

                //  hoverSprite.DOKill();
                hoverSprite.DOFade(1.0f, fadeInDuration);
            }
        }

        private void OnMouseExit()
        {
            if (active)
            {
                if (MouseExitSound) AudioManager.PlayClip(MouseExitSound);
                hoverSprite.DOKill();
                hoverSprite.DOFade(0.0f, fadeOutDuration);
            }
        }

        private void OnMouseDown()
        {
            if (MaskedByUI && EventSystem.current.IsPointerOverGameObject())
                return;

            if (active)
            {
                if (MouseClickSound) AudioManager.PlayClip(MouseClickSound);
                onClick?.Invoke();
                if (clickSprite)
                {
                    clickSprite.enabled = true;
                    clickSprite.DOKill();
                    clickSprite.DOFade(1.0f, fadeInDuration);
                    Invoke(nameof(DisableClickSprite), fadeOutDuration);
                }

                if (HasCoolDown)
                {
                    SetActive(false);
                    Invoke(nameof(EnableButton), coolDown);
                }

            }
        }


        private void EnableButton()
        {
            SetActive(true);
        }

        private void DisableClickSprite()
        {
            clickSprite.enabled = false;
            clickSprite.DOFade(0.0f, 0.0f);
        }
    }
}