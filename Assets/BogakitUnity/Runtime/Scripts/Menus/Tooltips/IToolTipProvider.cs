using System.Collections.Generic;

namespace BogakitUnity.Tooltips
{
    public interface IToolTipProvider
    {
        public string GetTooltipTitle();
        public string GetTooltipContent();
        public List<string> GetTooltipSections();
        public string GetTooltipFooter();
    }
}