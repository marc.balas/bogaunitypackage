﻿using System;
using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

//using AeLa.EasyFeedback.APIs;

namespace BogakitUnity.Tooltips
{

    public enum TooltipType
    {
        Fixed,
        Anchored,
        Floating
        
        
    }
    [RequireComponent(typeof(CanvasGroup))]
    public class Tooltip : MonoBehaviour
    {

        public TextMeshProUGUI headerField;
        [Required("missing textmesh ui")]
        public TextMeshProUGUI contentField;

       // [Required("missing section ")]
        public GameObject sectionPanel;

        
        public TextMeshProUGUI footerField;
        [Required("missing layout component")]
        public LayoutElement layoutElement;

        
        public int characterWrapLimit;
        [Tooltip("tooltip anchored to trigger")]
        public TooltipType type = TooltipType.Anchored;
        

        
        public List<GameObject> sectionPanels = new List<GameObject>();
        
        //[ShowIfAttributeBase("anchored")] 
        
        [Tooltip("offset to bottom left of the tooltip")]
        [SerializeField]
        private Vector2 offset;

        private RectTransform rectTransform;
        private GameObject anchor;
        [SerializeField] private Vector2 anchoredPosition;
        [ShowNativeProperty]
        public Vector2 position =>  GetComponent<RectTransform>().anchoredPosition;
       

        private void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            if (sectionPanel && sectionPanel.GetComponentInChildren<TextMeshProUGUI>() == null)
            {
                Debug.LogError( "missing textmesh ui in section");
            }
          
        }

        public void SetText(string content, List<string> sections, string header = "",string footer ="")
        {
            headerField.gameObject.SetActive(!string.IsNullOrEmpty(header));

            RemoveSections();
            
            if(!string.IsNullOrEmpty(header))
            {   headerField.text = header;
            }
            
            footerField.gameObject.SetActive(!string.IsNullOrEmpty(footer));
            
            if(!string.IsNullOrEmpty(footer))
            {   footerField.text = footer;
            }

            contentField.text = content;
            int headerLength = headerField.text.Length;
            int contentLength = contentField.text.Length;

            layoutElement.enabled = (headerLength > characterWrapLimit || contentLength > characterWrapLimit);
            foreach (var section in sections)
            {
             AddSection(section);
            }
         
        }

        [Button ("remove sections")]
        private void RemoveSections()
        {
            while (sectionPanels.FirstOrDefault())
            {
                var section = sectionPanels.FirstOrDefault();
                if (section == null) break; 
                DestroyImmediate(section);
                sectionPanels.Remove(section);
            }
        }

        [Button("add section")]
        public void AddSection()
        {
            AddSection("test new section");
        }
        
        public void AddSection(string text)
        {
            var  sectionGO = Instantiate(sectionPanel, transform );
            sectionGO.GetComponentInChildren<TextMeshProUGUI>().text = text;
            sectionPanels.Add(sectionGO);
            footerField.transform.SetAsLastSibling();
        }

        // Update is called once per frame
        private void Update()
        {
            if (Application.isEditor)
            {
            
                int headerLength = headerField.text.Length;
                int contentLength = contentField.text.Length;

                layoutElement.enabled = (headerLength > characterWrapLimit || contentLength > characterWrapLimit);

            }
            //required to have screen origin as ref position  
            if (type != TooltipType.Fixed)
            {
                rectTransform.anchorMax = Vector2.zero;
                rectTransform.anchorMin = Vector2.zero;    
            }

            Vector2 position = Vector2.zero;
            
            switch (type)
            {
                case TooltipType.Fixed:
                    break;
                case TooltipType.Anchored:
                    rectTransform.anchoredPosition = anchoredPosition+ offset;
                    break;
                case TooltipType.Floating:
                    position =Input.mousePosition;
                    rectTransform.anchoredPosition = position + offset;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
         
            //Vector2 pivot =  new Vector2(position.x/Screen.width, position.y/Screen.height);
            // rectTransform.pivot = pivot;
            // transform.position = position + offset;

        }

        // public void SetAnchor(GameObject go)
        // {
        //     anchor = go;
        // }

        public void SetPosition(GameObject go, Vector2 anchorOffset)
        {
            anchor = go;
            if (anchor?.GetComponent<RectTransform>())
            {
                anchoredPosition = anchor?.GetComponent<RectTransform>().anchoredPosition ?? Input.mousePosition;
            }
            else
            {
                anchoredPosition =  Camera.main.WorldToScreenPoint(anchor.transform.position);
            }
            // overwrite offset with anchor offset from trigger
            if(type == TooltipType.Anchored) offset = anchorOffset;
        }
    }
}
