using System.Collections.Generic;
using UnityEngine;

namespace BogakitUnity.Tooltips
{
    [RequireComponent(typeof(TooltipTrigger))]
    public class TooltipProvider : MonoBehaviour, IToolTipProvider
    {
        [SerializeField] private string content;
        [SerializeField] private string header;
        [SerializeField] private string footer;
        [SerializeField] private List<string> sections = new List<string>();
      
        public string GetTooltipTitle()
        {
            return header;
        }

        public string GetTooltipContent()
        {
            return content;
        }
       

        public string GetTooltipFooter()
        {
            return footer;
        }

        public List<string> GetTooltipSections()
        {
            return sections;
        }
        
    }
}