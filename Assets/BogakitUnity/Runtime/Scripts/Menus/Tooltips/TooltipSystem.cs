﻿using System.Collections.Generic;
using BogakitUnity.Selections;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

namespace BogakitUnity.Tooltips
{
    [ExecuteInEditMode]
    public class TooltipSystem : MonoBehaviour
    {
        private static TooltipSystem current;

        [InfoBox("RaycastSelector is need for non UI tooltip triggers", EInfoBoxType.Warning)] 
        public Tooltip tooltip;

        [SerializeField][ReadOnly] private GameObject lockTo;
        public static bool isLocked => current.lockTo != null;
        
      //  public static GameObject LockTo => current.lockTo != null;

        private void Awake()
        {
            current = this;
            Hide();
            if(FindObjectOfType<RaycastSelector>()==null)Debug.LogError("Tooltip system:missing raycast selector. It is required to handle non UI tooltip triggers");
        }

        public static void Show(string content, GameObject go, string header, List<string> sections,string footer, Vector2 offset, bool lockToSelection)
        {
            if(current.lockTo!=null)
                return;
            current.tooltip.SetText(content,sections, header, footer);
            current.tooltip.gameObject.SetActive(true);
          
            current.tooltip.SetPosition(go, offset);
            LayoutRebuilder.ForceRebuildLayoutImmediate(current.tooltip.GetComponentInChildren<RectTransform>());
            if(lockToSelection)current.lockTo = go;

            // current.MoveTooltip(current.tooltip, go);
        }

        public  static void Hide()
        {
            if(!Application.isPlaying)return;
            
            
            if (current?.tooltip == null)
            {
                Debug.LogWarning("Tooltip system is null");
                return;
            }
            if (!Application.IsPlaying(current.tooltip)) return;
           
            current.tooltip?.gameObject.SetActive(false);
            current.lockTo = null;
        }

        private void OnEnable()
        {
            Selection.OnDeselect += SelectionOnOnDeselect;
        }

        private void SelectionOnOnDeselect(GameObject obj)
        {
            Hide();
        }

        private void OnDisable()
        {
            Selection.OnDeselect -= SelectionOnOnDeselect;
        }
        public static bool IsLockTo(GameObject o)
        {
            return current.lockTo == o;
        }

        public static void Unlock()
        {
            if (current == null)
            {
                Debug.LogWarning("Tooltip system is null");
                return;
            }
            current.lockTo = null;
        }
    }
}