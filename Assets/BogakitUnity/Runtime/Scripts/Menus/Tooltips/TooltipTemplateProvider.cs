using System.Collections.Generic;
using Boga.Assets;
using Boga.Core;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity.Tooltips
{
    [RequireComponent(typeof(TooltipTrigger))]
    public class TooltipTemplateProvider : MonoBehaviour, IToolTipProvider
    {
        private AssetTemplate template;
        // [InfoBox("which attribute to show")]
        // [SerializeField]private string attributeKey;

        public void PopulateWithAsset(AssetTemplate newAsset)
        {
            template = newAsset;
            Assert.IsNotNull(template, "template is null");

        }
        public string GetTooltipTitle()
        {
            return template.Name;
        }

        public string GetTooltipContent()
        {
            if (template == null)
            {
                Debug.LogError( $"template is null in {gameObject.name}");
                return "";
            }

            return template.textBoxString.Exist() ? template.textBoxString :template.Description;

        }
        private string GetGlossaryText(string infoText)
        {
            List<Keyword> keywordsFound = Glossary.instance.GetKeywords(infoText);
            string result = infoText;

            foreach (var keyword in keywordsFound)
            {
                result += $"\n{keyword}";
            }

            result = Glossary.instance.GetColoredText(result);
            return result;
        }

        public string GetTooltipFooter()
        {
            return "";
        }

        public List<string> GetTooltipSections()
        {
            // get keywords
            List<Keyword> keywordsFound = Glossary.instance.GetKeywords(template.textBoxString);

            List<string> result = new List<string>();
            foreach (var keyword in keywordsFound)
            {
                result.Add($"<b>{keyword.name}</b>:{Glossary.instance.GetColoredText(keyword.description)}");
            }


            return result;
        }    }
}