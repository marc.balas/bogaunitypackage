using System.Collections.Generic;
using Boga.Core;
using BogakitUnity.EntityViews.Abstract;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BogakitUnity.Tooltips
{
    public class TooltipTrigger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [Tooltip("Show the tooltip when selected ")] [SerializeField]
        internal bool lockWhenSelected;

        [SerializeField] private float delay;
        [ReadOnly] public float pointerEnterTime;
        [ReadOnly] public bool pointerIn;
        [SerializeField] internal Vector2 offset;

        [Tooltip("print debug text")] [SerializeField]
        private bool debug;


        public void OnPointerEnter(PointerEventData eventData)
        {
            pointerIn = true;
            if (delay <= 0) Show();
        }

        private void OnValidate()
        {
            if (GetComponent<IToolTipProvider>() is not { } p)
            {
                Debug.LogError($"No IToolTipProvider found on {gameObject.name}");
            }
        }

        private void Show()
        {
            if (debug) Debug.Log($"tooltip triggered by {gameObject.name}");
            if (TooltipSystem.isLocked) return;
            if (GetComponent<IToolTipProvider>() is not { } p) return;
            // do not show if there is no content
            // if (!p.GetTooltipContent().Exist())
            //     return;

            List<string> sections = p.GetTooltipSections();

#if DEBUG
            string debugInfo = "";
            if (GetComponent<ElementView>() is { } elementView)
            {
                debugInfo += elementView.element.MultiLineInfo();
            }

            if (debugInfo.Exist()) sections.Add(debugInfo);

#endif


            TooltipSystem.Show(p.GetTooltipContent(), gameObject, p.GetTooltipTitle(), sections, p.GetTooltipFooter(),
                offset,
                lockWhenSelected);
        }


        public void OnPointerExit(PointerEventData eventData)
        {
            if (lockWhenSelected && Selections.Selection.CurrentSelection == gameObject)
                return;
            //unlock if lock to this but not selected anymore
            if (TooltipSystem.IsLockTo(gameObject) && Selections.Selection.CurrentSelection != gameObject)
            {
                TooltipSystem.Unlock();
            }

            if (TooltipSystem.isLocked)
                return;

            pointerIn = false;
            pointerEnterTime = 0;
            TooltipSystem.Hide();
        }

        private void Update()
        {
            if (pointerIn)
            {
                pointerEnterTime += Time.deltaTime;
                if (delay > 0 && pointerEnterTime >= delay) Show();
            }
        }
    }
}