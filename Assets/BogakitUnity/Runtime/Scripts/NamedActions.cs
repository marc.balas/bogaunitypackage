using System;
using System.Collections.Generic;
using BogakitUnity.Extensions;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BogakitUnity
{
    public class NamedActions : MonoBehaviour
    {
        // bind a action name to a unity event
        [SerializeField] protected List<NamedEvent> namedActions = new List<NamedEvent>();

        [SerializeField] protected Button nameActionButtonPrefab;

        [SerializeField] protected Transform actionsParent;


        private void Awake()
        {
            actionsParent.Clear();
        }

        // Start is called before the first frame update
        void Start()
        {
            CreateButtons();
        }

        [Button("create buttons")]
        public void CreateButtons()
        {
            actionsParent.Clear();
            foreach (var namedEvent in namedActions)
            {
                //create a button
                var button = Instantiate(nameActionButtonPrefab, actionsParent);
                button.GetComponentInChildren<TextMeshProUGUI>().text = namedEvent.action;
                button.onClick.AddListener(namedEvent.UnityEvent.Invoke); //bind the action to the button();
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        [Serializable]
        public class NamedEvent
        {
            public string action;
            public UnityEvent UnityEvent;
        }
    }
}