using Boga.Assets;
using TMPro;
using UnityEngine;

public class RecipeInfoView : MonoBehaviour
{
    private Recipe recipe;
    
    
    [SerializeField]private TextMeshProUGUI title;
    [SerializeField]private TextMeshProUGUI formula;
    [SerializeField]private TextMeshProUGUI description;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetRecipe(Recipe _recipe)
    {
        recipe = _recipe;
        if (recipe != null)
        {
            title.text = recipe.Name;
            description.text = recipe.Description;
            formula.text = recipe.FormulaDescription;    
        }
        else
        {
            title.text = "unknown";
            description.text = "????";
            formula.text = "????";    
        }
        
    }
}
