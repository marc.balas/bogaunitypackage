using BogakitUnity.Selections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity
{
    [RequireComponent(typeof(TargetingAction))]
    public class RotateToTarget : MonoBehaviour
    {
        [SerializeField] private GameObject currentTarget;
        [SerializeField] private float duration = .5f;

        [SerializeField] private bool rotateTarget;
        TargetingAction targetingAction;
        TargetingManager targetingManager;
        private GameObject actionTarget => targetingAction.target;

        private GameObject previousTarget;

        //[SerializeField] private Vector3 lineAnchor;
        protected virtual void Awake()
        {
            targetingAction = GetComponent<TargetingAction>();
            Assert.IsNotNull(targetingAction, "missing targeting action");
            targetingManager = GetComponent<TargetingManager>();
            Assert.IsNotNull(targetingManager, "missing targeting manager");
        }

        // Update is called once per frame
        void Update()
        {
            UpdateTarget();
           
        }

        protected virtual void UpdateTarget()
        {
        
            if (previousTarget)
            {
                
                Debug.Log("ROTATE previous target " + previousTarget.name + " at time: " + Time.time);
                previousTarget.transform.DOKill();
                previousTarget.transform.DORotateQuaternion(Quaternion.Euler(0, 0, 0), 0.2f).SetEase(Ease.OutExpo);
                previousTarget = null;
            }
         
            if (actionTarget == currentTarget) return;
            previousTarget = currentTarget;
            currentTarget = actionTarget;
            if (Selection.CurrentSelection != gameObject) return;

            bool validTarget = targetingManager.IsValidTargeting(gameObject, currentTarget);
            // currentTarget && currentTarget.GetComponent<TargetingAction>() &&
            //                    currentTarget.GetComponent<TargetingAction>().validTarget;
            // DOTween.Kill(1);
            if (validTarget)
            {
                AlignAnimation(transform);
                if (!rotateTarget) return;
                //currentTarget.transform.DOKill();
                Vector3 direction = currentTarget.transform.position - transform.position;
                // Calculate the angle in radians from the X axis
                float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                Debug.Log("ROTATE  target:" + angle + "time: " + Time.time);
                Quaternion targetRotation = Quaternion.Euler(0, 0, -angle);
                currentTarget.transform.DORotateQuaternion(targetRotation, duration / 2).SetEase(Ease.OutExpo);
            }
            else
            {
                Quaternion targetRotation = Quaternion.Euler(0, 0, 0);
                transform.DORotateQuaternion(targetRotation, duration).SetEase(Ease.OutExpo).SetId(1);
            }
        }

        private void AlignAnimation(Transform t)
        {
            // t.DOKill();
            Vector3 direction = currentTarget.transform.position - transform.position;
            // Calculate the angle in radians from the X axis
            float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
            Debug.Log("angle source :" + angle);
            Quaternion targetRotation = Quaternion.Euler(0, 0, -angle);
            t.DORotateQuaternion(targetRotation, duration / 2).SetEase(Ease.OutExpo).SetId(3);
        }
    }
}