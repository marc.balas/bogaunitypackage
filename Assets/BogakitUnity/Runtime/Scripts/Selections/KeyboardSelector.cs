﻿using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using BogakitUnity.EntityViews.Abstract;
using UnityEngine;

namespace BogakitUnity.Selections
{
    //change focus and selection of active elements with keyboards 
    // update SO variable, containing GO with selection responder
    // logic : 

    //usecase : use keys to iterate over hand cards and change focus(simulate hovering)

    // valid = click
    // 

    public class KeyboardSelector : MonoBehaviour //, ISelector
    {
        public KeyCode next;
        public KeyCode previous;
        public KeyCode valid;

        public KeyCode cancelKey; //Deselect , defocus
        //public KeyCode switchKey;

        //private ISelectionResponse _selectionResponse;

        //at position of candidate
        public GameObject dummyFocus;
        public GameObject dummySelected;
        public GameObject objectWithFocus;
        
        private List<GameObject> selectableElements => EntityView.all.OfType<ElementView>().Select(el => el.gameObject).ToList();

        protected virtual void Update()
        {
            if (Input.GetKeyDown(next))
            {
                Selection.HoverChanged(Selection.CurrentFocus == null
                    ? selectableElements.FirstOrDefault()
                    :  selectableElements.Before(Selection.CurrentFocus));
                FocusOn(Selection.CurrentFocus);
            }

            else if (Input.GetKeyDown(previous))
            {
                Selection.HoverChanged(Selection.CurrentFocus == null ? selectableElements.FirstOrDefault() : selectableElements.After(Selection.CurrentFocus));
                FocusOn(Selection.CurrentFocus);
            }
            else if (Input.GetKeyDown(cancelKey))
            {
                Selection.Deselect();
            }

            else if (Input.GetKeyDown(valid))
            {
                if (Selection.CurrentSelection)
                {
                    Selection.Deselect();
                }
                else if (Selection.CurrentFocus)
                {
                    Selection.Select(Selection.CurrentFocus);
                }

                if (dummySelected)
                {
                    dummySelected.SetActive(Selection.CurrentSelection);
                    if (Selection.CurrentSelection) dummySelected.transform.position = Selection.CurrentSelection.transform.position;
                }
            }
        }

        public void FocusOn(GameObject go)
        {
            //deselect previous
            // if (objectWithFocus != null) objectWithFocus.GetComponent<EntityView>()?.OnFocus(false);
            // objectWithFocus = go;
            // if (objectWithFocus != null) objectWithFocus.GetComponent<EntityView>()?.OnFocus(true);
            if (dummyFocus)
            {
                dummyFocus.SetActive(Selection.CurrentFocus);
                if (Selection.CurrentFocus) dummyFocus.transform.position = Selection.CurrentFocus.transform.position;
            }
        }
    }
}