using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using GameObject = UnityEngine.GameObject;

namespace BogakitUnity.Selections
{
    public class RaycastSelector : MonoBehaviour
    {
        [SerializeField]
        protected bool debug;

        
        protected static List<GameObject> hoverGOs = new List<GameObject>();
        
        
        [SerializeField]
        protected bool BlockedByUI;

        [SerializeField]
        protected RaycastMode mode;

        [SerializeField]
        protected TextMeshProUGUI debugInfo;

        // protected BaseUIManager ui;

        private Camera mainCamera;

        protected void Awake()
        {
            mainCamera = Camera.main;
        }

        public static bool IsPointerOverObject(GameObject obj)
        {
            return hoverGOs.Contains(obj);
        }

        /// <summary>
        /// ignore current selection
        /// </summary>
        /// <returns></returns>
        private List<GameObject> GetGameObjectsWith2DRaycast()
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D[] hits2D = Physics2D.GetRayIntersectionAll(ray, Mathf.Infinity);
            List<GameObject> gos = hits2D.Select(h => h.transform.gameObject).Where(g=> g != Selection.CurrentSelection).GroupBy(h=>h).Select(g=>g.FirstOrDefault()).ToList();
            return gos;
        }

        private T GetComponentWith2DRaycast<T>(List<T> ignoredElements = null) where T : MonoBehaviour
        {
            if (BlockedByUI && EventSystem.current.IsPointerOverGameObject())
                return null;

            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            RaycastHit2D[] hits2D = Physics2D.GetRayIntersectionAll(ray, Mathf.Infinity);

            if (hits2D.Length > 0)
            {
                foreach (var hit in hits2D)
                {
                    if (hit.transform)
                    {
                        if (hit.transform.gameObject != Selection.CurrentSelection)
                        {
                            T component = hit.transform.gameObject.GetComponent<T>();
                            if (component != null)
                            {
                                if (ignoredElements == null || !ignoredElements.Contains(component))
                                    return component;
                            }
                        }
                    }
                }
            }

            return null;
        }

        private void LateUpdate()
        {
            
            UpdateDebugInfo();
            
        }

        private void UpdateDebugInfo()
        {
            try
            {
                if (debugInfo != null)
                {
                    debugInfo.text = $"focus: {Selection.CurrentFocus?.name ?? "-"} ";
                    debugInfo.text += $"\nselection: { Selection.CurrentSelection?.name ?? "-" } ";
                    debugInfo.text += $"\nevent current: { EventSystem.current.currentSelectedGameObject?.name ?? "-" } ";
                }
            }
            catch (MissingReferenceException e)
            {
                Console.WriteLine(e);
              
            }
          
        }

        private void Update()
        {
            if (BlockedByUI && EventSystem.current.IsPointerOverGameObject())
                return;

            hoverGOs = mode == RaycastMode.mode2D
                    ? GetGameObjectsWith2DRaycast(): GetGameObjectsWith3DRaycast(); 
            
            GameObject hoverGO = hoverGOs.FirstOrDefault();
            if (debug && hoverGOs.Count>0)
            {
                Debug.Log("hover on objects :" + hoverGOs.Count);
            }
            //new hovered obj
            if (Selection.CurrentFocus != hoverGO)
            {
                if (debug) Debug.Log($"Focus changed from {Selection.CurrentFocus?.name ?? "nothing"} to {hoverGO?.name ?? "nothing"}");
                Selection.HoverChanged(hoverGO);
            }

            // TODO : remove drag drop 
            // when there is an active interaction  , ignore all clicks ?
            // delegate to interaction 
            // if (Input.GetMouseButtonDown(0) && DragDropAction.CurrentDragDropAction == null)
            // {
            //     if (Selection.CurrentSelection != hoverGO)
            //     {
            //         if (debug) Debug.Log("new click on :" + hoverGO);
            //         Selection.Select(hoverGO);
            //     }
            //
            //     //release current after, to avoid same frame conflict with multiple interactions
            // }
        }

        private GameObject GetGameObjectWith3DRaycast()
        {
          
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity);
            foreach (var hit in hits)
            {
                if (hit.transform)
                {
                    if (hit.transform.gameObject != Selection.CurrentSelection)
                    {
                        return hit.transform.gameObject;
                    }
                }
            }
            return null;
        }
        
        private List<GameObject> GetGameObjectsWith3DRaycast()
        {
    
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity);

            return hits.Select(h => h.transform.gameObject).Where(h=>h.transform.gameObject != Selection.CurrentSelection).ToList();
        }
    }
    
  

    public enum RaycastMode
    {
        mode2D,
        mode3D
    }
}