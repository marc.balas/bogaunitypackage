using System;
using BogakitUnity.EntityViews.Abstract;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BogakitUnity.Selections
{
    public static class Selection
    {
        public static event Action<GameObject,GameObject> OnFocusChanged;
        public static event Action<GameObject> OnSelect;
        public static event Action<GameObject> OnDeselect;

        public static GameObject CurrentSelection { get; private set; }
        public static GameObject CurrentFocus = null;

        public static GameObject PreviousFocus;
        //public static Zone zoneWithFocus => currentFocus ? currentFocus.GetComponent<ZoneView>()?.zone  : null;
      //  public static IEntity elementWithFocus => currentFocus ? currentFocus.GetComponent<ElementView>()?.Entity  : null;

        public static void HoverChanged(GameObject go)
        {
            PreviousFocus = CurrentFocus;
            CurrentFocus = go;
            
            OnFocusChanged?.Invoke(CurrentFocus, PreviousFocus);
        }
        private static void SelectionChanged(GameObject go)
        {
            Debug.Log($"selection changed to {go}");
           
            OnSelect?.Invoke(go);
            if (go ) go.GetComponent<EntityView>()?.Select();
            
        }
        public static void Deselect()
        {
            if (EventSystem.current) EventSystem.current.SetSelectedGameObject(null);
            Debug.Log($" deselection of {CurrentSelection}");
            OnDeselect?.Invoke(CurrentSelection);
            CurrentSelection = null;
        }
        
        public static void Select(GameObject newSelection)
        {
           
            if (CurrentSelection == newSelection) return;
            Deselect();
            CurrentSelection = newSelection;
            SelectionChanged(newSelection);
        }

    }

}