using TMPro;
using UnityEngine;

namespace BogakitUnity
{
    [RequireComponent(typeof( TextMeshProUGUI))]
    public class TextSelection : MonoBehaviour
    {
    
        private TextMeshProUGUI label;
        void Awake()
        {
            label = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            Selections.Selection.OnSelect += SelectionOnOnSelect ;
            Selections.Selection.OnDeselect += SelectionOnOnDeselect ;
        }

        private void SelectionOnOnDeselect(GameObject obj)
        {
            label.text =  "selection: -"; 
        }

        private void SelectionOnOnSelect(GameObject newSelection)
        {
            label.text =  "selection: " +   newSelection?.name ?? "-"; 
        }

        private void OnDisable()
        {
            Selections.Selection.OnSelect -= SelectionOnOnSelect;
            Selections.Selection.OnDeselect -= SelectionOnOnDeselect;
        }


    }
}
