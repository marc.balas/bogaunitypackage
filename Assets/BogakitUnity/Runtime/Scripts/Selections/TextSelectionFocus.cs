using TMPro;
using UnityEngine;

namespace BogakitUnity
{
    [RequireComponent(typeof( TextMeshProUGUI))]
    public class TextSelectionFocus : MonoBehaviour
    {
    
        private TextMeshProUGUI label;
        void Awake()
        {
            label = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            Selections.Selection.OnFocusChanged += OnFocusChanged;
        }

        private void OnDisable()
        {
            Selections.Selection.OnFocusChanged -= OnFocusChanged;
        }

        private void OnFocusChanged(GameObject newFocus, GameObject arg2)
        {
            label.text =   "focus: " +  newFocus?.name ?? "-"; 
        }

    }
}
