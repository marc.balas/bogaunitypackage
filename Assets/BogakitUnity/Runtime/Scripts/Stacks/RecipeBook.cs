using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Boga.Assets;
using Boga.Core.Exceptions;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity
{
    public class RecipeBook : MonoBehaviour
    {
        [Tooltip("load/save recipes from account")] [SerializeField]
        private bool useAccount;

        [ShowIf("useAccount")] [SerializeField]
        TextAsset accountFile;

        [Required("prefab missing")] [SerializeField]
        private GameObject recipeItemPrefab;

        [Required("parent transform missing")] [SerializeField]
        private Transform recipeContent;

        [Required("recipe info view")] [SerializeField]
        private RecipeInfoView recipeInfoView;

        public List<string> recipeNames = new List<string>();

        public List<Recipe> recipes = new List<Recipe>();

        private void Awake()
        {
            if (useAccount)
            {
                try
                {
                    AccountInfo.path = Path.Combine(Application.persistentDataPath, "account.json");
                    string content = "";
                    // Check if the file exists
                    if (File.Exists(AccountInfo.path))
                    {
                        // Read the content of the file
                        content = File.ReadAllText(AccountInfo.path);
                        Debug.Log("File content: " + content);
                    }
                    else
                    {
                        Debug.LogError("File not found: " + AccountInfo.path);
                    }
                    AccountInfo.CreateFromData(accountFile != null ? accountFile.text : content);
                    recipeNames = AccountInfo.shared?.recipes;
                }
                catch (BogaException e)
                {
                    Debug.LogError("cannot load account: " + e);
                }
            }

            recipeItemPrefab.SetActive(false);
        }


        // Start is called before the first frame update
        void Start()
        {
            Assert.IsNotNull(recipeItemPrefab);
            Assert.IsNotNull(recipeContent);
            Assert.IsNotNull(recipeInfoView);
            gameObject.SetActive(false);
            foreach (var recipe in AccountInfo.shared?.recipes)
            {
                AddRecipe(recipe);
            }

            if (AccountInfo.shared?.recipes.Count == 0)
            {
                GameObject recipeGo = Instantiate(recipeItemPrefab, recipeContent);
                recipeGo.GetComponentInChildren<TextMeshProUGUI>().text = "no recipe yet. Try harder ";
                recipeGo.SetActive(true);
            }
        }

        private void AddRecipe(string recipe)
        {
            Recipe r = ResourceManager.GetRecipes()
                .FirstOrDefault(r => r.Name.Equals(recipe, StringComparison.InvariantCultureIgnoreCase));

            if (r == null) throw new ResourceException($"did not find recipe {recipe} in resources");
            GameObject recipeGo = Instantiate(recipeItemPrefab, recipeContent) ?? throw new ArgumentNullException("Instantiate(recipeItemPrefab, recipeContent)");
            recipeGo.GetComponentInChildren<TextMeshProUGUI>().text = Decorate(r);
            recipeGo.SetActive(true);
            recipes.Add(r);
        }

        public string Decorate(Recipe r)
        {
            return
                $"{r.Name}"; // {String.Join("+",r.formula).(System.Drawing.Color.Beige )} => {String.Join("+",r.product)}";
        }

        public void UpdateRecipeInfo(TextMeshProUGUI recipeitem)
        {
            Recipe recipe = recipes.FirstOrDefault(r =>
                r.Name.Equals(recipeitem.text, StringComparison.InvariantCultureIgnoreCase));
            recipeInfoView.SetRecipe(recipe);
        }

        private void OnEnable()
        {
            AccountInfo.shared.RecipeLearn += OnRecipeLearn;
        }

        private void OnDisable()
        {
            AccountInfo.shared.RecipeLearn -= OnRecipeLearn;
        }

        private void OnRecipeLearn(string newRecipe)
        {
            AddRecipe(newRecipe);
        }
    
    }
}