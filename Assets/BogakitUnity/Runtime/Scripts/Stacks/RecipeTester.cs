using System.Collections.Generic;
using Boga.Assets;
using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity
{
    public class RecipeTester : MonoBehaviour
    {

        [SerializeField] private List<string> ingredients;
    
    
        [SerializeField] private List<string> products = new List<string>();
        [SerializeField] private List<string> poolProducts = new List<string>();
        private List<AssetTemplate> ingredientAssets = new List<AssetTemplate>();
        // Start is called before the first frame update
        void Start()
        {
        
        }

        private void OnValidate()
        {
            ingredientAssets.Clear();
            foreach (var ingredient in ingredients)
            {
                ingredientAssets.Add(ResourceManager.GetAssetTemplateWithName<AssetTemplate>(ingredient));
            }
        }

        [Button("try recipe")]
        void TestRecipe()
        {
            Recipe recipe = ResourceManager.GetRecipeWithItems(ingredients);
            Debug.Log($"RECIPE: {recipe}");
            products.AddRange(recipe.product);
            var asset = recipe.GetPoolProduction<AssetTemplate>(ingredientAssets);
            if(asset)poolProducts.Add(asset.Name);
        }
    
        [Button("clear products")]
        public void ClearProducts()
        {
            products.Clear();
            poolProducts.Clear();
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
