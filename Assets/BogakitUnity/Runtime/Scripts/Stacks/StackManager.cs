using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Boga.Assets;
using Boga.Core;
using Boga.Core.Exceptions;
using BogakitUnity.Boga.Catalog;
using BogakitUnity.Extensions;
using BogakitUnity.Selections;
using BogakitUnity.Utils;
using DG.Tweening;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Serialization;

namespace BogakitUnity
{
    [RequireComponent(typeof(DragDropAction))]
    public class StackManager : DragDropManager
    {
        [InfoBox("override this class to customize drag drop logic")]
        [Required("missing stack recipes")]
        [FormerlySerializedAs("combinationConfig")]
        [SerializeField]
        private TextAsset recipes;
        //
        // [Required("missing stack catalog")]
        // [SerializeField]
        // private TextAsset catalog;
        //
        // [Required("missing pack catalog")] [SerializeField]
        // private TextAsset packs;
        //
        // [Required("missing pools catalog")] [SerializeField]
        // private TextAsset pools;

        [HorizontalLine(color: EColor.Green)] [SerializeField]
        private List<string> startingAssets = new List<string>();

        [SerializeField] private TextMeshProUGUI errorText;
        [SerializeField] private TextMeshProUGUI infoText;

        [Header("on spawn, auto stack similar stack")] [SerializeField]
        private bool autoStackSimilar = true;

        [Header("on spawn, auto stack on closest similar stack within range")] [SerializeField]
        private float similarStackingDistance = 5f;

        [SerializeField] private int maxStackSize = 10;
        [SerializeField] private Stackable prefab;
    
        [SerializeField] private List<StackablePrefabType> prefabByType = new List<StackablePrefabType>();
        private readonly HashSet<string> assetsSeen = new HashSet<string>();

        [Header("Stacks can only move inside these boundaries")] [Required("missing boundaries")] [SerializeField]
        private Collider2D boundaries;

        [SerializeField] private bool debug;

        [ShowIfAttributeBase("debug")] [SerializeField] [Unity.Collections.ReadOnly]
        private List<Stackable> stackablesOverlapping;

        private Stackable stackableWithFocus;
        [SerializeField] private AssetView assetView;

        // [ShowIfAttributeBase("debug")] [FormerlySerializedAs("dialog")] [SerializeField]
        // private GameObject debugDialog;

        protected virtual void Awake()
        {
            PseudoRandomGenerator.Setup(1);
            if (errorText) errorText.gameObject.SetActive(false);
            if (assetView) assetView.gameObject.SetActive(false);
            // LoadAssetCatalog();
        }

        private void Start()
        {
            foreach (var startingAsset in startingAssets)
            {
                AssetTemplate t = ResourceManager.GetAssetTemplateWithName<AssetTemplate>(startingAsset);
                SpawnStackAround(Vector3.zero, $"{t.Name}");
            }
        }


        private void OnEnable()
        {
            Stackable.Created += StackableOnCreated;
            Stackable.Destroyed += stackable => stackablesOverlapping.Remove(stackable);

            Stackable.DidProduced += StackableDidProduced;
            Stackable.DoubleClick += StackableOnDoubleClick;
            AssetPackView.OpenAssetPack += OnOpenAssetPack;
        }

        private void StackableOnCreated(Stackable stackable)
        {
            stackable.GetComponent<LimitedPosition>()?.SetLimits(boundaries);

            //check if unique so far
            if (!assetsSeen.Contains(stackable.assetName))
            {
                stackable.ShowBadgeIsNew(true);
            }

            // add it to account
            if (!AccountInfo.shared.HasAsset(stackable.asset))
            {
                AccountInfo.shared.AddAsset(stackable.asset);
            }

            assetsSeen.Add(stackable.assetName);
        }

        

        public List<Recipe> GetHints()
        {
            //TODO:
            //return recipes that you could do
            return new List<Recipe>();
        }

        private void OnDisable()
        {
            Stackable.Created -= OnStackableCreated;
            Stackable.Destroyed -= StackableOnDestroyed;

            Stackable.DidProduced -= StackableDidProduced;
            Stackable.DoubleClick -= StackableOnDoubleClick;
            AssetPackView.OpenAssetPack -= OnOpenAssetPack;
        }

        private void StackableOnDestroyed(Stackable stackable)
        {
            stackablesOverlapping.Remove(stackable);
        }

        private void OnStackableCreated(Stackable stackable)
        {
            stackable.GetComponent<LimitedPosition>()?.SetLimits(boundaries);
        }

        public void TrashStack(GameObject go)
        {
            Stackable stackable = go.GetComponent<Stackable>();
            if (stackable)
            {
                List<Stackable> trashables = stackable.allStackItems;
                trashables.Reverse();
                foreach (var toTrash in trashables)
                {
                    toTrash.Destroy();
                    // toTrash.transform.DOScale(0, 0.5f).OnComplete(()=>Destroy(toTrash.gameObject));
                }
            }
        }


        private void OnOpenAssetPack(AssetPackView pack)
        {
            List<AssetTemplate> assets = ResourceManager.GetAssetsFromPack<AssetTemplate>(pack.packName);
            StartCoroutine(SpawnAssets(assets, pack.transform.position));

            pack.transform.DOScale(0, 0.5f).OnComplete(() => Destroy(pack.gameObject));
        }

        private IEnumerator SpawnAssets(List<AssetTemplate> assets, Vector3 transformPosition)
        {
            foreach (var asset in assets)
            {
                SpawnStackAround(transformPosition, asset.Name);
                yield return new WaitForSeconds(1);
            }
        }

        public override string GetHelpText()
        {
            if (Selection.CurrentSelection != null)
            {
                return $"Stack {Selection.CurrentSelection.name} on {Selection.CurrentFocus?.name}";
            }
            else return "";
        }

        private void StackableOnDoubleClick(Stackable obj)
        {
            ShowAssetView(obj.asset);
            Time.timeScale = 0f;
        }

        private void ShowAssetView(AssetTemplate objAsset)
        {
            if (assetView)
            {
                assetView.gameObject.SetActive(true);
                assetView.PopulateWithAsset(objAsset);
            }
        }

        // [Button]
        // private void LoadAssetCatalog()
        // {
        //     ResourceManager.RegisterAssets<AssetTemplate>(catalog.text);
        //     ResourceManager.RegisterPools(pools.text);
        //     ResourceManager.RegisterRecipes(recipes.text);
        //     ResourceManager.RegisterAssetPacks(packs.text);
        // }

        private void StackableDidProduced(Stackable stackProducer)
        {
            List<Stackable> ingredients = stackProducer.allStackItems;
            //stackProducer.recipe.ProduceWith(stackProducer.allStackItems.Select(s => s.asset).ToList());
            Debug.Log($"Recipe ready: {stackProducer.recipe}");
            // specific recipe logic


            // 1. apply recipe modifiers
            // wood : quality +2
            // $1 : quality +2 give +2 quality to the 1st ingredient matching the 1st ingredient of the formula


            foreach (var kvp in stackProducer.recipe.modifiers)
            {
                List<Stackable> modifiedIngredients = Select(kvp.Key, from: ingredients, stackProducer.recipe);
                foreach (var stack in modifiedIngredients)
                {
                    stack.ApplyModifier(kvp.Value);
                }
            }

            //2. Produce from pool
            //by product is a pool, select which will be spawned
            AssetTemplate assetTemplateFromPool = stackProducer.GetPoolProduction<AssetTemplate>();
            if (assetTemplateFromPool)
            {
                SpawnStackAround(stackProducer, assetTemplateFromPool.Name);
            }

            // 3. create product
            foreach (var item in stackProducer.recipe.GetProducts(stackProducer.allStackItems.Select(s => s.asset).ToList()))
            {
                SpawnStackAround(stackProducer, item);
            }

            // END: destroy cost stacks
            // we destroy at the end, the cost can be used as production parameter 
            foreach (var item in stackProducer.recipe.GetConsumedAssets(stackProducer.allStackItems.Select(s => s.asset)
                         .ToList()))
            {
                Stackable toPay = stackProducer.allStackItems.Last(s =>
                    s.asset.Name.Equals(item.Name, StringComparison.InvariantCultureIgnoreCase));
                toPay.Consume();
            }
        }

        private List<Stackable> Select(string selector, List<Stackable> from, Recipe recipe)
        {
            // check reserved words 
            //  reserved: &&all, $$any,
            //  TODO value based: $$max quality (min, max) + stat key
            if (selector.StartsWith("$$"))
            {
                string reservedWord = selector.Replace("$$", "");
                switch (reservedWord)
                {
                    case "any":
                        return new List<Stackable>() { from.RandomSample() };
                    case "all":
                        return from;
                    default:
                        throw new BogaException($"invalid selector with reserved word {selector}");
                }
            }

            //  positional: $1  , relative to formula
            if (selector.StartsWith("$"))
            {
                string position = selector.Replace("$", "");
                if (!int.TryParse(position, out int index))
                    throw new BogaException($"invalid position {position}, not a valid number");

                //  if (index > recipe.formula.Count) throw new BogaException($"invalid position {index}");
                string ingredientName = recipe.formula[index - 1];
                Stackable stackable = from.FirstOrDefault(s =>
                    Equals(s.asset, AssetTemplate.GetAssetMatching(from.Select(item => item.asset), ingredientName)));
                return new List<Stackable>() { stackable };
            }


            //  named asset: wood   
            Stackable stack = from.FirstOrDefault(s =>
                s.asset.Name.Equals(selector, StringComparison.InvariantCultureIgnoreCase));
            return new List<Stackable>() { stack };

            //TODO: add random range: [1-3] 
        }

        // Update is called once per frame
        void Update()
        {
            infoText.text = $"{Stackable.stacks.Count()} stacks {Stackable.all.Count} stackables";

            UpdateOverlappingStacks();
            stackableWithFocus = Selection.CurrentFocus?.GetComponent<Stackable>();


            CheckInputs();

        }

        private void CheckInputs()
        {
#if UNITY_EDITOR
            if (stackableWithFocus != null)
            {
                if (Input.GetKeyDown(KeyCode.D))
                {
                    stackableWithFocus.Destroy();
                }
                
                if (Input.GetKeyDown(KeyCode.I))
                {
                    stackableWithFocus.root.FinishRecipe();
                }

                if (Input.GetKeyDown(KeyCode.C))
                {
                    SpawnStackAround(stackableWithFocus, stackableWithFocus.name);
                }

                if (Input.GetKeyDown(KeyCode.U))
                {
                    stackableWithFocus.UnstackAll();
                }

            }
#endif

        }

        [Button("Group SimilarToHover")]
        public void GroupSimilarToHover()
        {
            if (stackableWithFocus) stackableWithFocus.StackSimilarOnMe();
        }

        [Button("split stack with focus")]
        public void Split()
        {
            if (stackableWithFocus) stackableWithFocus.Split();
        }

        // private void ShowCreateAssetDialog()
        // {
        //     // list of buttons with all asset from catalog
        //     debugDialog.gameObject.SetActive(true);
        //     List<(string, Action)> items = new List<(string, Action)>();
        //
        //     foreach (var template in ResourceManager.GetTemplates<AssetTemplate>())
        //     {
        //         items.Add((template.Name, () => SpawnStackAround(null, template.Name)));
        //     }
        //
        //     debugDialog.PopulateWithActions(items);
        // }

        private void GroupAll()
        {
            //List<IGrouping<string,Stackable>> s = Stackable.all.GroupBy(c => c.GetHashCode()).ToList();
            List<IGrouping<int, Stackable>> s = Stackable.all.GroupBy(c => c.GetHashCode(), c => c).ToList();

            s.ForEach(g => { g.First().StackListOnMe(g.ToList()); });
        }

        private void UnstackAll()
        {
            foreach (var root in Stackable.stacks)
            {
                root.UnstackAll();
            }
        }

        private void UpdateOverlappingStacks()
        {
            if (Selection.CurrentSelection != null)
            {
                Stackable stackable = Selection.CurrentSelection.GetComponent<Stackable>();
                if (stackable != null)
                {
                    Stackable.all.ForEach(s => s.dropRectangle.color = Color.red);
                    stackablesOverlapping.Clear();
                    foreach (var s in Stackable.all)
                    {
                        if (s != stackable && Physics2D.IsTouching(stackable.GetComponent<BoxCollider2D>(),
                                s.GetComponent<BoxCollider2D>()))
                        {
                            stackablesOverlapping.Add(s);
                        }
                    }

                    stackablesOverlapping.ForEach(s => s.leaf.dropRectangle.color = Color.green);
                }
            }
        }

        public override bool CanDropOn(GameObject dragDropAction, GameObject go)
        {
            if (go == null)
            {
                return true;
            }

            Stackable stackable = Selection.CurrentSelection.GetComponent<Stackable>();

            return stackable.CanStackOn(go.GetComponent<Stackable>());
        }

        public override void BeginDrag(GameObject element)
        {
            element.GetComponent<Stackable>()?.WillUnstack();
        }

        // move to StackManager delegate
        public override bool CanDrag(GameObject element)
        {
            if (element.GetComponentInParent<Stackable>() != null)
                return element.GetComponentInParent<Stackable>().CanBeDragged();
            else return true;
        }

        public override void DidDropOn(GameObject source,GameObject go)
        {
            Stackable stackable = Selection.CurrentSelection.GetComponent<Stackable>();

            if (!stackable)
            {
                PutSourceDown(source);
              //  dropAction.OnEndDrag();
                return;
            }

            Stackable stackableTarget = go?.GetComponent<Stackable>()?.leaf;


            // try to find a valid one, first no in same stack leaf 
            // fallback: no valid found, force cancel by choosing first one which is not a descendant
            if (stackableTarget == null)
            {
                UpdateOverlappingStacks();
                stackableTarget = stackablesOverlapping.FirstOrDefault(c => stackable.CanStackOn(c)) ??
                                  stackablesOverlapping.FirstOrDefault(c => !c.IsDescendantOf(stackable));
            }

            if (!CanDropOn(source, stackableTarget?.gameObject))
            {
                this.ShowText(errorText.gameObject, errorText, $"Cannot stack on {stackableTarget}", 1f);
                //dropAction.OnCancelDrag();
                return;
            }

            // do not stop prod if drop on nothing,
            // stop prod if not valid 
            //TODO : allow prod to continue if new stack has same recipe else stop
            // if root moves do not stop
            //if()
            if (stackable.parent == null)
            {
            }

            else if (stackableTarget != null)
            {
                stackable.root.StopProduction();
            }
            else
            {
                stackable.root.StopProduction();
            }

            stackable.StackOn(stackableTarget);
            if (stackablesOverlapping.Count == 0)
            {
                PutSourceDown(source);
            }

            //dropAction.OnEndDrag();
        }

        public void PutSourceDown(GameObject dropAction)
        {
            if (dropAction == null)
                return;

            dropAction.transform.position = dropAction.transform.position.WithZ(0);
        }

        [Button("spawn random card from catalog")]
        public void SpawnStackAround()
        {
            AssetTemplate assetTemplate = ResourceManager.GetRandomAssetTemplate<AssetTemplate>();
            SpawnStackAround(Vector3.zero, FindPlacementForCircle(Vector3.zero, 0.5f, 100), assetTemplate.Name);
        }

        public void SpawnStackAround(Vector3 spawnPosition, string newStack, bool forceGrouping = false)
        {
            SpawnStackAround(spawnPosition, FindPlacementForCircle(spawnPosition, 0.5f, 100), newStack, forceGrouping);
        }

        public void SpawnStack(Vector3 atPosition, string newStack,bool forceGrouping = false)
        {
            SpawnStackAround(atPosition, atPosition, newStack, forceGrouping );
        }

        public void SpawnAssetPackAround(Vector3 position, string packName)
        {
        }

        Vector2 FindPlacementForCircle(Vector3 center, float radius, float maxDistance)
        {
            float checkIncrement = 0.5f; // Increment distance for each step
            float angleIncrement = 30f; // Angle increment for the spiral

            for (float distance = checkIncrement; distance < maxDistance; distance += checkIncrement)
            {
                for (float angle = 0; angle < 360; angle += angleIncrement)
                {
                    Vector2 checkPosition = center + Quaternion.Euler(0, 0, angle) * Vector2.right * distance;
                    Collider2D hit = Physics2D.OverlapCircle(checkPosition, radius);
                    if (hit == null) // No collision found at this position
                    {
                        return checkPosition; // This position is open for placement
                    }
                }
            }

            // No suitable position found within maxDistance
            throw new Exception("no space available");
        }

        public void SpawnStackAround(Vector3 spawnPosition, Vector3 targetPosition, string assetName,bool forceGrouping = false)
        {
            if (!IsStackNameValid(assetName))
            {
                this.ShowText(errorText.gameObject, errorText, $"invalid stack name: {assetName}", 1f);
                return;
            }

            var newStack = CreateStack(spawnPosition.WithZ(0), assetName);

            if (autoStackSimilar || forceGrouping)
            {
                // similar stacks ordered by distance
                // get stacks containing stackables similar to the new stackable

                // exclude ?   s != stackableTarget
                List<Stackable> similars =
                    Stackable.stacks
                        .Where(s => s.allStackItems.All(c => c.assetName == s.assetName) && s.assetName == assetName)
                        .OrderBy(s =>
                            Vector2.Distance(s.transform.position, newStack.transform.position)).ToList();
                // stack on similar if within range
                //excluded self
                similars.Remove(newStack);
                Debug.Log($"{similars.Count} similar assets found");
                Stackable stackSimilar = similars.FirstOrDefault();
                if (stackSimilar != null && (forceGrouping || similarStackingDistance >
                    Vector2.Distance(stackSimilar.transform.position, newStack.transform.position)))
                {
                  
                    newStack.StackOn(stackSimilar.leaf);
                    newStack.Unfreeze();
                
                    // newStack.transform.DOMove(stackSimilar.leaf.transform.position, .5f).OnComplete(() =>
                    //     newStack.Unfreeze());
                }
            }
            else
            {
                newStack.transform.DOPunchRotation(new Vector3(-60, 00, 0), .5f, 0);
                newStack.transform.DOMove(targetPosition.WithZ(0), .5f).OnComplete(() =>
                    newStack.Unfreeze());
            }
        }

        private Stackable CreateStack(Vector3 position, string newStack)
        {
           AssetTemplate asset = ResourceManager.GetAssetTemplateWithName<AssetTemplate>(newStack.CleanString());


           Stackable p = prefabByType.FirstOrDefault(p => p.typeName.Equals(asset.Type,
               StringComparison.InvariantCultureIgnoreCase))?.prefab;

            GameObject newGo = p == null ? Instantiate(prefab.gameObject, null):Instantiate(p.gameObject, null);
            
            newGo.GetComponent<Stackable>().PopulateWithName(newStack);
            newGo.GetComponent<Rigidbody2D>().simulated = false;
            newGo.GetComponent<SortingGroup>().sortingLayerName = "Top";
            //debugDialog?.gameObject.SetActive(false);
            newGo.transform.position = position;
            newGo.name = newStack;
            return newGo.GetComponent<Stackable>();
        }

        private bool IsStackNameValid(string newStack)
        {
            return true;
        }

        private void SpawnStackAround(Stackable stackableTarget, string newStack)
        {
            SpawnStackAround(stackableTarget.transform.position, stackableTarget.FuturePosition, newStack);
        }

        [Button("Snap to grid")]
        public void SnapToGrid()
        {
            // snap each stack to closest free cell 
            foreach (var stack in Stackable.stacks)
            {
                stack.transform.DOMove(stack.transform.GetClosestGridPosition(2), .5f);
            }
        }

        public Recipe GetClosestRecipe(Stackable stackable)
        {
            //TODO: if not producing , find the closest valid recipe for this stack
            return null;
        }

        // public List<Recipe> GetRecipeWithMissingElements(int nbMissing)
        // {
        //     var recipes = new List<Recipe>();
        //     foreach (var recipe in ResourceManager.GetRecipes())
        //     {
        //         var missing = recipe.GetMissingItems(Stackable.all.Select(s => s.asset));
        //         if (missing.missingItems.Count == nbMissing)
        //         {
        //             recipes.Add(recipe);
        //         }
        //     }
        //
        //     return recipes;
        // }


        public override void CancelDragDropOnAction(DragDropAction dropAction)
        {
            Stackable stackable = dropAction.sourceView.GetComponent<Stackable>();
            stackable?.DidStack();

            //stackable?.StackOn(stackable.parent);

            if (stackable?.parent == null) dropAction.PutSourceDown();
        }

        public void SpawnStacksAround(Vector3 position, AssetTemplate asset, int nb)
        {
            for (int i = 0; i < nb; i++)
            {
               SpawnStackAround(position, asset.Name);
            }
        }

        public void DestroyAllStacksWithName(string nameToDestroy)
        {
            foreach (var stack in Stackable.all.Where(s =>
                         s.assetName.Equals(nameToDestroy, StringComparison.InvariantCultureIgnoreCase)))
            {
                stack.Destroy();
            }
        }

        public void DestroyStacksWithName(string nameToDestroy, int nb)
        {
            var stacks = Stackable.all.Where(s =>
                         s.assetName.Equals(nameToDestroy, StringComparison.InvariantCultureIgnoreCase)).ToList();
            
                if(stacks.Count() < nb) return;

                for (int i = 0; i < nb; i++)
                {
                    stacks.First().Destroy();  
                }
            
        }

        public void ShowError(string canTBuyThisCard)
        {
            this.ShowText(errorText.gameObject, errorText, canTBuyThisCard, 1f);

        }
    }

    [Serializable]
    internal class StackablePrefabType
    {
        public string typeName;
        public Stackable prefab;
    }
}