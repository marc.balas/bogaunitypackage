using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Boga.Assets;
using Boga.Core;
using Boga.Core.Exceptions;
using BogakitUnity.Anim;
using BogakitUnity.Boga.Catalog;
using BogakitUnity.Extensions;
using BogakitUnity.Managers;
using BogakitUnity.Tooltips;
using DG.Tweening;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;
using Selection = BogakitUnity.Selections.Selection;

namespace BogakitUnity
{
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(SortingGroup))]
    [RequireComponent(typeof(LerpFollow))]
    public class Stackable : MonoBehaviour, IPointerClickHandler, IToolTipProvider //, IBuffable
    {
        public static readonly List<Stackable> all = new List<Stackable>();
        [SerializeField] private UnityEvent actionOnUpdate;
        [SerializeField] private UnityEvent<bool> OnStack;
        [field: SerializeField] public string assetName; // => asset ? asset.Name : "";

        [Tooltip("does not accept any other stack")] [SerializeField]
        private bool locked;

        [Tooltip("cannot unstack")] [SerializeField]
        private bool sticky;

        [Tooltip("can be moved")] [SerializeField]
        private bool draggable = true;

        public bool isRoot => root == this;


        [ShowNativeProperty] public bool isSingle => child == null && parent == null;

        //all stackable are identical
        [ShowNativeProperty]
        public bool isPure =>
            allStackItems.All(s => s.assetName.Equals(assetName, StringComparison.InvariantCultureIgnoreCase));

        [ShowNativeProperty]
        public bool isPureType => allStackItems.All(s => s.asset && s.asset.Type.Equals(asset.Type));

        [SerializeField] protected int durability = 1;
        [SerializeField] protected int cost = 1;
        [SerializeField] public string assetType { get; protected set; } = "";
        [SerializeField] private float productionDuration;
        public float productionTimeLeft => (100 * productionDuration * (1 - slider.value)) / (float)productionSpeed;
        [field: SerializeField] public Vector2 FuturePosition { get; private set; }

        [ShowNativeProperty]
        public Stackable leaf
        {
            get
            {
                Stackable other = this;

                while (other?.child != null)
                {
                    other = other.child;
                }

                return other;
            }
        }


        [Required("missing visual transform")] [Foldout("Views")] [SerializeField]
        protected Transform visual;

        [Required("missing production slider")] [Foldout("Views")] [SerializeField]
        private Slider slider;

        [Required("missing pause toggle")] [Foldout("Views")] [SerializeField]
        private Toggle pauseToggle;

        [Foldout("Views")] [SerializeField] public SpriteRenderer lockSprite;
        [Foldout("Views")] [SerializeField] private TextMeshPro stackText;
        [Foldout("Views")] [SerializeField] public SpriteRenderer dropDummy;
        [Foldout("Views")] [SerializeField] public SpriteRenderer stickySprite;
        [Foldout("Views")] [SerializeField] public SpriteRenderer notDraggableSprite;
       // [Foldout("Views")] [SerializeField] public Rectangle dropRectangle;
       //TODO: add sprite
       [Foldout("Views")] [SerializeField] public SpriteRenderer dropRectangle;
        [Foldout("Views")] [SerializeField] public GameObject badgeIsNew;
        [Foldout("Views")] [SerializeField] public GameObject badgeIsUnique;
        [Foldout("Views")] [SerializeField] public GameObject validDropIndicator;
        [Foldout("Views")] [SerializeField] public GameObject invalidDropIndicator;
        [Foldout("Views")] [SerializeField] private string stackSound;
        [Foldout("Views")] [SerializeField] private string unstackSound;

        [HorizontalLine(color: EColor.Green)] [SerializeField]
        private bool debug;

        [ShowIf("debug")] [SerializeField] [ReadOnly]
        protected internal Stackable parent;

        [ShowIf("debug")] [SerializeField] [ReadOnly]
        protected Stackable child;

        [ShowIf("debug")] [SerializeField] public Bounds bounds;

        [ShowIf("debug")] [SerializeField] [ReadOnly]
        protected Recipe _recipe;

        AssetView view;
        [SerializeField] private int productionSpeed;

        [SerializeField]
        private Dictionary<string, int> stats = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

        private Dictionary<string, string>
            attributes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        private List<Stackable> stackItems
        {
            get
            {
                var list = new List<Stackable>(root.descendants)
                {
                    root
                };
                return list;
            }
        }

        public AssetTemplate asset { get; protected set; }

        public Recipe recipe => _recipe;

        protected virtual string info => $"{assetName} D:{durability} V:{cost}";


        public Stackable root
        {
            get
            {
                Stackable other = this;

                while (other != null && other.parent != null)
                {
                    other = other.parent;
                }

                return other;
            }
        }


        private Vector3 offset => GetComponent<LerpFollow>().GetOffset();


        // there is a production, it is not paused, 
        private bool productionActive => (pauseToggle == null || !pauseToggle.isOn) && slider.isActiveAndEnabled &&
                                         (Selection.CurrentSelection == null ||
                                          !Selection.CurrentSelection
                                              .GetComponent<Stackable>().SameStack(this));

        private bool stackHasRecipe => root.recipe != null;
        public static IEnumerable<Stackable> stacks => all.Where(s => s.parent == null);

        public static IEnumerable<Stackable> leaves => all.Where(s => s.child == null);

        public static event Action<Stackable> Created;
        public static event Action<Stackable> Destroyed;
        public static event Action<Stackable> DidProduced;
        public static event Action<Stackable> DoubleClick;
        
        public UnityEvent<Stackable,Recipe> startProduction;

        public List<Stackable> descendants
        {
            get
            {
                List<Stackable> list = new List<Stackable>();
                Stackable other = child;
                while (other != null)
                {
                    list.Add(other);
                    other = other.child;
                }

                return list;
            }
        }

        public List<Stackable> allStackItems
        {
            get
            {
                var list = new List<Stackable> { root };
                list.AddRange(root.descendants);
                return list;
            }
        }

        public void OnDrawGizmos()
        {
            if (recipe != null && isRoot)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawCube(FuturePosition, bounds.size);
            }
        }

        public List<Stackable> ascendants
        {
            get
            {
                List<Stackable> list = new List<Stackable>();
                Stackable other = parent;
                while (other != null)
                {
                    list.Add(other);
                    other = other.parent;
                }

                return list;
            }
        }

        private void Awake()
        {
            Assert.IsNotNull(visual);
            view = GetComponent<AssetView>();
            bounds = GetComponent<BoxCollider2D>().bounds;
            slider.gameObject.SetActive(false);
            dropDummy.enabled = false;
            badgeIsNew.SetActive(false);
            slider.value = 0;
            all.Add(this);
            
            //GetComponent<Rigidbody2D>().simulated = false;
            //GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            //Invoke("unfreeze",.75f);
            if (pauseToggle)
            {
                pauseToggle.gameObject.SetActive(false);
                pauseToggle.isOn = false;
            }


            //if(GetComponent<Rigidbody2D>())GetComponent<Rigidbody2D>().freezeRotation = true;
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            if (debug) Debug.Log($"collision stay {name} with {other.gameObject.name} ");
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (debug) Debug.Log($"collision enter {name} with {other.gameObject.name} ");
        }

        // Start is called before the first frame update
        void Start()
        {
            LoadAsset();
            Unfreeze();
            Created?.Invoke(this);
        }

        [Button("Load asset")]
        private void LoadAsset()
        {
            asset = ResourceManager.GetAssetTemplateWithName<AssetTemplate>(assetName);
            if (asset == null)
            {
                throw new ResourceException($"Asset {assetName} not found ");
            }
            else
            {
                view.PopulateWithAsset(asset);
                stats = asset.Stats.Clone();
                attributes = asset.Attributes.Clone();
                // foreach (var kvp in asset.Stats)
                // {
                //     attributes.Add(kvp.Key, kvp.Value);
                // }
            }
        }

        public void ShowBadgeIsNew(bool show)
        {
            if (badgeIsNew) badgeIsNew.SetActive(show);
        }

        public override int GetHashCode()
        {
            return tag.GetHashCode();
        }

        private void OnEnable()
        {
            Selection.OnSelect += SelectionSelect;
            Selection.OnDeselect += SelectionDeselect;
        }

        private void SelectionDeselect(GameObject deselected)
        {
            dropDummy.enabled = false;
            dropRectangle.enabled = false;
        }

        private void OnDisable()
        {
            Selection.OnSelect -= SelectionSelect;
            Selection.OnDeselect -= SelectionDeselect;
            Destroyed?.Invoke(this);
        }

        private void OnDestroy()
        {
            all.Remove(this);
        }

        public void Destroy()
        {
            // unstack
            if (parent)
            {
                var oldParent = parent;
                Unstack();
                oldParent.UpdateProduction();
                
            }
            if (child) child.Unstack();

            transform.DOScale(Vector3.zero, .5f).OnComplete(() => GameObject.Destroy(gameObject));
        }

        //check if recipe changed
        private void UpdateProduction()
        {
            var newRecipe = ResourceManager.GetRecipeWithItems(allStackItems.Select(s => s.asset));

            if (recipe != newRecipe)
            {
                _recipe = newRecipe;
                StopProduction();
                if(recipe != null)StartRecipe();
            }
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (badgeIsNew.activeSelf)
                badgeIsNew.transform.DOScale(Vector3.zero, .5f).OnComplete(() => badgeIsNew.SetActive(false));
            if (eventData.clickCount == 2)
            {
                Debug.Log("double click");
                DoubleClick?.Invoke(this);
            }
        }

        public void Unstack()
        {
            StackOn(null);
        }

        private void SelectionSelect(GameObject obj)
        {
            Stackable stackable = obj.GetComponent<Stackable>();
            if (stackable && stackable.CanStackOn(this))
            {
                dropDummy.enabled = true;
                dropRectangle.enabled = true;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (stackText)
            {
                if (debug)
                    stackText.text =
                        $"{name} P: {parent} C:{child} D:{descendants.Count} O:{GetComponent<SortingGroup>().sortingOrder} {stats.ToDebugString()}";
                else
                {
                    stackText.text = $"{name}";
                }
            }

            if (debug) stackText.color = locked ? Color.red : Color.green;
            if (lockSprite) lockSprite.enabled = locked;
            if (notDraggableSprite) notDraggableSprite.enabled = !draggable;
            if (stickySprite) stickySprite.enabled = sticky;
            // if selection is same stack : we pause production
            if (productionActive)
            {
                slider.value += Time.deltaTime * (productionSpeed / 100f) / productionDuration;
                if (slider.value >= 1)
                {
                    //leaf.transform.DOShakeRotation(.5f, new Vector3(0, 0, 20));
                    if (debug) Debug.Log("Production ready !");
                    GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
                    Invoke("Unfreeze", .5f);
                    FuturePosition = FindPlacementForCircle(0.5f, 100);
                    DidProduced?.Invoke(this);
                    recipe.CompleteProduction();

                    StopProduction();
                    // start again 
                    StartProduction();
                }
            }

            // // animate dash
            // if (dropRectangle)
            // {
            //     AnimateDropRectangle();
            // }
            if (Selection.CurrentSelection && Selection.CurrentSelection.GetComponent<Stackable>() == this)
            {
                // HACK:fix issue with collider.isTouching when Z is not 0 while lerping with small values
                if (transform.position.z < Single.Epsilon & transform.position.z < 0)
                {
                    transform.position = transform.position.WithZ(0);
                }
            }
        }

        public void Unfreeze()
        {
            GetComponent<SortingGroup>().sortingLayerName = "Default";
            GetComponent<Rigidbody2D>().simulated = true;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        // private void AnimateDropRectangle()
        // {
        //     if (dropRectangle.enabled) dropRectangle.DashOffset += dashSpeed / 100;
        //    
        // }

        public override string ToString()
        {
            return $"{name}";
        }

        // move to StackManager delegate
        //TODO: throw 
        public bool CanStackOn(Stackable stackableTarget)
        {
            // can stack on nothing
            if (stackableTarget == null)
                return true;
            if (stackableTarget.locked)
                return false;
            // cannot stack on self
            if (stackableTarget == this)
                return false;
            if (SameStack(stackableTarget)) return false;

            // cannot stack on target with a child which is not null, ie: only stackable on leaf
            if (stackableTarget.child != null)
                return false;

            // can be re-stacked on same parent
            if (parent == stackableTarget)
                return false;
            if (assetName.Equals(stackableTarget.assetName, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            if (assetType.Equals(stackableTarget.assetType, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            return CanBeStackedOnStackable(stackableTarget); //!SameStack(stackableTarget) &&
        }

        // only allow if recipe possible
        protected virtual bool CanBeStackedOnStackable(Stackable stackableTarget)
        {
            //check recipe
            // List<string> items = new List<string>()
            // {
            //     assetName
            // };
            //
            // items.AddRange(descendants.Select(s => s.assetName).ToList());
            // items.Add(stackableTarget.assetName);
            // Recipe recipeWithItems = ResourceManager.GetRecipeWithItems(allStackItems.Select(s=>s.asset));
            // return recipeWithItems != null;
            // only pack are not stackable
            return true;
        }

        public void StackListOnMe(List<Stackable> stackables)
        {
            foreach (var stackable in stackables)
            {
                stackable.StackOn(leaf);
            }
        }


        [Button("stack similar here")]
        public void StackSimilarOnMe()
        {
            IEnumerable<Stackable> stackables = all.Where(s =>
                s.isSingle && s.assetName.Equals(assetName, StringComparison.InvariantCultureIgnoreCase));
            StackListOnMe(stackables.ToList());
        }

        public void StackOn(Stackable stackableTarget)
        {
            if (!CanStackOn(stackableTarget))
                return;

            if (stackableTarget)
            {
                // change parent collider
                if (parent)
                {
                    parent.child = null;
                    parent.UpdateCollider();
                    UpdateCollider();
                    stackableTarget.UpdateCollider();
                }

                parent = stackableTarget;
                stackableTarget.child = this;

                DidStackOn(stackableTarget);
            }

            //stack on nothing: update collider
            else
            {
                if (parent)
                {
                    parent.child = null;
                    parent.UpdateCollider();
                    UpdateCollider();
                    parent = null;
                }
            }

            if (debug) Debug.Log($"stack {name} on {stackableTarget}");
            DidStack();
        }

        protected virtual void DidStackOn(Stackable stackableTarget)
        {
            // do specific thing
            root.StartProduction();
        }

        private void StartProduction()
        {
            //check if already production
            List<string> items = new List<string>()
            {
                assetName
            };

            //  items.AddRange(descendants.Select(s => s.assetName).ToList());
            _recipe = ResourceManager.GetRecipeWithItems(allStackItems.Select(s => s.asset));

            if (recipe != null)
            {
                StartRecipe();
               
            }

            else
            {
                Debug.Log("no recipe here");
                StopProduction();
            }
        }

        private void StartRecipe()
        {
            if (recipe == null)
            {
                return;
            }
            AccountInfo.shared.LearnRecipe(recipe.Name);
                
            startProduction?.Invoke(this,recipe);
            productionDuration = _recipe.duration;
            productionSpeed = _recipe.GetSpeedFactor(allStackItems.Select(s => s.asset.Name).ToList());
            slider.gameObject.SetActive(true);
            FuturePosition = FindPlacementForCircle(0.5f, 100);

            // if (recipe.CanBePaused)
            // {
            //     pauseToggle.gameObject.SetActive(true);
            //     pauseToggle.isOn = false;
            // }

            //stop current production in descendant
            child?.StopProduction();
        }

        public void StopProduction()
        {
            slider.gameObject.SetActive(false);
            slider.value = 0;
            FuturePosition = Vector2.zero;
            if (pauseToggle) pauseToggle.gameObject.SetActive(false);
        }

        public bool IsDescendantOf(Stackable stackable)
        {
            Stackable other = stackable;
            while (other != null)
            {
                if (other == this)
                    return true;
                other = other.child;
            }

            return false;
        }

        private bool IsAscendantOf(Stackable stackable)
        {
            Stackable other = stackable;
            while (other != null)
            {
                if (other == this)
                    return true;
                other = other.parent;
            }

            return false;
        }

        private bool SameStack(Stackable otherStackable)
        {
            // check Stackable are not ascendant or descendant 
            return IsAscendantOf(otherStackable) || IsDescendantOf(otherStackable);
        }

        // not really unstack, prepare dragging
        public void WillUnstack()
        {
            EnterFocus();
            GetComponent<LerpFollow>().Detach();
            //descendants.ForEach(s=>s.GetComponent<Collider2D>().enabled = false);
            int sortingOrder = 9999;
            PreparePhysicsForDragging(sortingOrder);

            if (parent)
            {
                parent.UpdateCollider();
            }

            UpdateCollider();
            AudioManager.Play(unstackSound);
        }

        private void PreparePhysicsForDragging(int sortingOrder)
        {
            GetComponent<SortingGroup>().sortingOrder = sortingOrder;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            GetComponent<PointEffector2D>().enabled = false;
            if (child)
            {
                child.PreparePhysicsForDragging(++sortingOrder);
            }
        }

        private void OnMouseEnter()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log($"Clicked on the UI {EventSystem.current.currentSelectedGameObject}");
                return;
            }

            // only root stackable can get focus
            if (Selection.CurrentSelection == null)
            {
                // if (parent == null)

                if (!stackHasRecipe) EnterFocus();
                if (debug) Debug.Log($"hover stackable {info}");
            }
        }

        private void EnterFocus()
        {
            if (!CanBeDragged())
            {
                return;
            }

            visual.transform.DOKill();

            visual.DOLocalMoveZ(-0.5f, .1f).OnComplete(() =>
            {
                // if (debug) Debug.Log($"mouse enter {name} z: {visual.localPosition.z}");
            });
            if (child) child.EnterFocus();
        }

        private void ExitFocus()
        {
            visual.DOKill();
            visual.localPosition = visual.localPosition.WithZ(0);
            //    transform.FindDeepChild("Visual").DOLocalMoveZ(0.0f, .12f);
            if (child) child.ExitFocus();
        }

        private void OnMouseExit()
        {
            // always keep focus if we have selection. avoid that fast mouse moves trigger exit
            if (Selection.CurrentSelection != null)
                return;
            ExitFocus();
            if (debug) Debug.Log($"mouse exit {name} z: {visual.localPosition.z}");
        }


        Vector2 FindPlacementForCircle(float radius, float maxDistance)
        {
            float checkIncrement = 0.5f; // Increment distance for each step
            float angleIncrement = 30f; // Angle increment for the spiral

            for (float distance = checkIncrement; distance < maxDistance; distance += checkIncrement)
            {
                for (float angle = 0; angle < 360; angle += angleIncrement)
                {
                    Vector2 checkPosition =
                        transform.position + Quaternion.Euler(0, 0, angle) * Vector2.right * distance;
                    Collider2D hit = Physics2D.OverlapCircle(checkPosition, radius);
                    if (hit == null) // No collision found at this position
                    {
                        return checkPosition; // This position is open for placement
                    }
                }
            }

            // No suitable position found within maxDistance
            throw new Exception("no space available");
        }

        public void DidStack()
        {
            // do specific thing
            root.StartProduction();
            ExitFocus();
            // must handle collision 
            if (parent == null)
            {
                GetComponent<LerpFollow>().Detach();
                GetComponent<SortingGroup>().sortingOrder = 1;
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

                GetComponent<PointEffector2D>().enabled = true;
            }
            // disable physics
            else
            {
                parent.UpdateCollider();
                GetComponent<PointEffector2D>().enabled = false;
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                GetComponent<LerpFollow>().Follow(parent.transform);
                GetComponent<SortingGroup>().sortingOrder = parent.GetComponent<SortingGroup>().sortingOrder + 1;
            }

            int sortingOrder = GetComponent<SortingGroup>().sortingOrder;

            foreach (var s in descendants)
            {
                s.GetComponent<SortingGroup>().sortingOrder = ++sortingOrder;
                s.GetComponent<PointEffector2D>().enabled = false;
            }

            
            if(parent && stackHasRecipe)OnStack?.Invoke(stackHasRecipe);
            
            AudioManager.Play(stackSound);
        }

        private void UpdateCollider()
        {
            GetComponent<BoxCollider2D>().offset =
                new Vector2(offset.x * 0.5f * descendants.Count, offset.y * 0.5f * descendants.Count);
            GetComponent<BoxCollider2D>().size =
                new Vector2(bounds.size.x, bounds.size.y + (Math.Abs(offset.y) * descendants.Count));
            if (debug) Debug.Log($"new bound {GetComponent<BoxCollider2D>().size}");
        }

        public virtual bool CanUnstack()
        {
            return !sticky;
        }


        public virtual bool CanBeDragged()
        {
            return draggable && (parent == null || CanUnstack());
        }

        [Button("unstack descendants")]
        public void UnstackAll()
        {
            descendants.ForEach(d => d.Unstack());
        }

        [Button("split stackables ")]
        public void Split()
        {
            List<Stackable> toCheck = allStackItems;
            Stackable previous = root;
            foreach (var s in toCheck)
            {
                if (previous.name != s.name)
                {
                    s.Unstack();
                    s.Split();
                    return;
                }
            }
        }

        // public override void PopulateWithAsset(AssetTemplate asset)
        // {
        //     this.asset = asset;
        //     durability = asset.GetStat("durability");
        //     if (title) title.text = $"{asset.Name}\n{asset.GetStat("value")}";
        //
        //     if (art == null) return;
        //     Sprite sprite = Resources.LoadAll<Sprite>(asset.GetAttribute("art")).First();
        //     if (sprite == null)
        //     {
        //         Debug.LogWarning($"missing sprite in resources {asset.GetAttribute("art")}");
        //     }
        //
        //     art.sprite = sprite;
        // }

        public void Consume()
        {
            durability--;
            if (durability <= 0) Destroy();
        }

        public virtual void PopulateWithName(string newStack)
        {
            assetName = newStack.CleanString();
            asset = ResourceManager.GetAssetTemplateWithName<AssetTemplate>(assetName);
        }

        public void RenameTo(string newName)
        {
            name = newName;
        }

        public string GetStackListDescription()
        {
            string desc = "";
            // var groupedAndCounted = objects
            //     .GroupBy(obj => obj.Value)
            //     .Select(group => new { Value = group.Key, Count = group.Count() });
            //
            // foreach (var item in groupedAndCounted)
            // {
            //     Console.WriteLine($"{item.Value}({item.Count})");
            // }
            var list = stackItems.GroupBy(s => s.assetName); //.Select(g=>g.Key);
            foreach (var group in list)
            {
                desc += $"{group.Count()}x {group.Key.FirstCharToUpper()}\n";
            }

            //desc = string.Join("\n", list);
            return desc;
        }


        public virtual string GetTooltipTitle()
        {
            return stackHasRecipe ? "producing " + root.recipe.Name : assetName;
        }

        public virtual string GetTooltipContent()
        {
            if (stackHasRecipe)
            {
                return root.recipe?.FormulaDescription;
            }

            if (isSingle) return asset ? asset?.Type + "\n" + asset?.Description : "none";
            return GetStackListDescription();
        }

        public List<string> GetTooltipSections()
        {
            return new List<string>();
        }

        public string GetTooltipFooter()
        {
            return stackHasRecipe
                ? $"{root.recipe.GetProductDescription<AssetTemplate>(allStackItems.Select(s => s.asset).ToList())} \n {root.recipe.Description.ColorTo(Color.white)}  \n {root.productionTimeLeft:F1} s left"
                : asset?.Description;
        }

        public T GetPoolProduction<T>() where T : AssetTemplate
        {
            try
            {
                return recipe.GetPoolProduction<T>(allStackItems.Select(s => s.asset).ToList());
            }
            catch (BogaException e)
            {
                Debug.LogError(e.Message);
                return null;
            }
         
        }


        public void ApplyModifier(string expression)
        {
            actionOnUpdate?.Invoke();
            //string expression = "key +123";
            //string pattern = @"(\w+)\s*([\+\-\*\/\=])\s*(\d+)"; 
            string pattern = Regexp.BUFF_OPERATION; 
            Match match = Regex.Match(expression, pattern);

            if (match.Success)
            {
                string key = match.Groups[1].Value;
                MathOperation op = match.Groups[2].Value.AsMathOperation();
                string number = match.Groups[3].Value;
                if (!int.TryParse(number, out int amount))
                {
                    throw new BogaException($"invalid number format in {expression}");
                }

                Debug.Log($"Key: {key}, Operator: {op}, Number: {amount}");
                int oldValue = stats.GetValueOrDefault(key, 0);
                stats[key] = MathEvaluator.Evaluate(stats[key], amount, op);
                

                Debug.Log($"Stack {name}: stat of {key} set to {stats[key]}.");
                return;
            }

            //format: attribute newValue
            string patternAttribute = @"(\w+)\s*(\w+)";
            Match matchAttribute = Regex.Match(expression, patternAttribute);

            if (matchAttribute.Success)
            {
                string key = match.Groups[1].Value;
                string value = match.Groups[2].Value;
                attributes[key] = value;
                Debug.Log($"Stack {name}: Attribute of  {key} set to {attributes[key]}.");
            }
            else
            {
                Debug.LogWarning("No match found.");
            }
        }
        
        public void BoostRecipe(int percent)
        {
            if (recipe != null)
            {
                slider.value += percent * 0.01f;
            }
        }

#if UNITY_EDITOR
        public void FinishRecipe()
        {
            if (recipe != null)
            {
                slider.value = 1;
            }
        }
#endif
        
    }
}
