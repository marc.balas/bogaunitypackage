using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity
{
    public class StackableCreator : MonoBehaviour
    {
        private StackManager stackManager;
    
        [SerializeField]private string assetName;
        // Start is called before the first frame update
        void Start()
        {

            stackManager = FindObjectOfType<StackManager>();
        }
    
        [Button("Create")]
        public void Create()
        {
            stackManager.SpawnStack(Vector3.zero, assetName);
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
