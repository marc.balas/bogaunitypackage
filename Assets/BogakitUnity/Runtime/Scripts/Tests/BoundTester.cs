using UnityEngine;

namespace BogakitUnity.Tests
{
    public class BoundTester : MonoBehaviour
    {
//Attach this script to an empty GameObject. Create 2 more GameObjects and attach a Collider component on each. Choose these as the "My Object" and "New Object" in the Inspector.
//This script allows you to move your main GameObject left to right. If it intersects with the other, it outputs the message to the Console.
        public GameObject m_MyObject, m_NewObject;
        Collider2D m_Collider, m_Collider2;

        void Start()
        {
            //Check that the first GameObject exists in the Inspector and fetch the Collider
            if (m_MyObject != null)
                m_Collider = m_MyObject.GetComponent<Collider2D>();

            //Check that the second GameObject exists in the Inspector and fetch the Collider
            if (m_NewObject != null)
                m_Collider2 = m_NewObject.GetComponent<Collider2D>();
        }

        void Update()
        {
            //If the first GameObject's Bounds enters the second GameObject's Bounds, output the message
            if ( Physics2D.IsTouching(m_Collider , m_Collider2))
            {
                Debug.Log("Bounds intersecting");
            }
        }
    }
}

