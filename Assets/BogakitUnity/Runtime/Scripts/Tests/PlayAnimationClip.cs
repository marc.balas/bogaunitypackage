using System.Collections.Generic;
using BogakitUnity.Selections;
using UnityEngine;

namespace BogakitUnity.Tests
{
    public class PlayAnimationClip : MonoBehaviour
    {

        [System.Serializable]
        class KeyAndString
        {
            public KeyCode keyCode;
            public string stringShortcut;
        }
        [SerializeField]
        private List<KeyAndString> shortcuts = new List<KeyAndString>();

        // Update is called once per frame
        void Update()
        {
            foreach (var kvp in shortcuts)
            {
                if (kvp != null && Input.GetKeyDown(kvp.keyCode))
                {
                    if (Selection.CurrentFocus != null)
                    {
                        Selection.CurrentFocus.GetComponent<Animator>()?.Play(kvp.stringShortcut);
                    }
                }
            }
        }
    }
}