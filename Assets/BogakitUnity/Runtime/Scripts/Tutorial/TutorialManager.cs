using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Boga.Assets;
using Boga.Core;
using Boga.Core.Exceptions;
using Boga.Kit;
using BogakitUnity.Extensions;
using DG.Tweening;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace BogakitUnity
{
    public class TutorialManager : MonoBehaviour
    {
        private List<TutorialStep> tutorialSteps;

        [SerializeField] private int stepIndex = 0;

        [SerializeField] private TextAsset tutorialFile;
        [SerializeField] private CanvasGroup tutorialCanvas;

        [SerializeField] TextMeshProUGUI tutorialTexts;

        [SerializeField] Button tutorialNextButton;

        Dictionary<string, int> eventCounters = new Dictionary<string, int>();
        [SerializeField] List<Image> tutorialImages;

        [SerializeField] float timeCountdown = 0;

        [SerializeField] Color keywordColor;

        private TutorialStep currentStep;

        //[SerializeField] private bool forceTutorial;
        public bool isComplete;

        public bool isBlocked;

        private void Awake()
        {
            tutorialCanvas.alpha = 0;
            tutorialCanvas.gameObject.SetActive(false);

            try
            {
                var deserializer = new DeserializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build();
                try
                {
                    tutorialSteps = deserializer.Deserialize<List<TutorialStep>>(tutorialFile.text);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    throw e;
                }
                // JsonSerializerSettings settings = new JsonSerializerSettings
                // {
                //     MissingMemberHandling = MissingMemberHandling.Ignore // Explicitly ignore missing members
                // };
                // tutorialSteps = JsonConvert.DeserializeObject<List<TutorialStep>>(tutorialFile.text, settings);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new BogaException($"invalid tutorial data {e.Message}");
            }
        }

        [Button("Start Tutorial")]

        public void StartTutorial()
        {
            StopCoroutine(RunTutorial());
            isBlocked = false;
            ResetTutorialSteps();
            StartCoroutine(RunTutorial());
        }

        private void ResetTutorialSteps()
        {
            foreach (var step in tutorialSteps)
            {
                step.completed = false;
            }
        }

        [Button("next step")]
        public void NextStep()
        {
            currentStep.completed = true;
        }


        IEnumerator Start()
        {
            isComplete = false;
            if (tutorialNextButton)
            {
                tutorialNextButton.onClick.AddListener(() => currentStep.completed = true);
                tutorialNextButton.gameObject.SetActive(false);
            }

            bool tutorialActive = BogaGame.shared.settings.IsFeatureDisabled("tutorial");
            if (!tutorialActive) yield return null;
            else
            {
                yield return StartCoroutine(RunTutorial());

            }

            tutorialCanvas.DOFade(0, .5f).OnComplete(() => tutorialCanvas.gameObject.SetActive(false));
            isComplete = true;
        }

        private IEnumerator RunTutorial()
        {
            isComplete = false;
            stepIndex = 0;
            yield return new WaitUntil(() => !isBlocked);
            yield return new WaitForSeconds(.5f);
            tutorialCanvas.gameObject.SetActive(true);
            tutorialCanvas.transform.localScale = Vector3.one * 0.5f;
            tutorialCanvas.GetComponent<CanvasGroup>().alpha = 0;
            //  tutorialCanvas.DOFade(1, .5f);

            foreach (var step in tutorialSteps)
            {
                currentStep = step;
                if (currentStep.completed) continue;
                if (tutorialNextButton)
                {
                    tutorialNextButton.gameObject.SetActive(!step.waitForEvent.Exist());
                }

                // After X seconds show new tuto
                //  if (step.delay > 0) yield return new WaitForSeconds(step.delay);
                yield return new WaitForSeconds(.5f);
                tutorialCanvas.transform.DOScale(1, .25f);
                tutorialCanvas.DOFade(1, .5f);
                if (step.animation.Exist())
                {
                    GetComponent<Animator>().Play(step.animation);
                }

                if (step.focus.Exist())
                {
                    GameObject obj = GameObject.Find(step.focus);
                    obj.transform.DOShakeRotation(1);
                }

                if (step.Line.Exist())
                {
                    tutorialTexts.gameObject.SetActive(true);
                    tutorialTexts.text = step.Line.ColorMarkdown("%%", keywordColor);
                }

                yield return new WaitUntil(() => step.completed);
                // if (step.Url.Exist() && tutorialImages.Count > stepIndex)
                // {
                //     tutorialImages[stepIndex].sprite = Resources.Load<Sprite>(step.Url);
                // }
                // tutorialTexts[stepIndex].gameObject.SetActive(false);
                stepIndex++;
                tutorialCanvas.DOFade(0, .25f);
                tutorialCanvas.transform.DOScale(0f, .5f);
                yield return new WaitForSeconds(2.5f);

            }

            isComplete = true;
            if (tutorialNextButton)
            {
                tutorialNextButton.gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            Stackable.Created += OnCreated;
            Stackable.Destroyed += OnDestroyed;
            Recipe.DidComplete += OnRecipeDone;
        }

        private void OnDisable()
        {
            Stackable.Created -= OnCreated;
            Stackable.Destroyed -= OnDestroyed;
            Recipe.DidComplete -= OnRecipeDone;
        }

        private void OnRecipeDone(Recipe obj)
        {
            if (!eventCounters.TryAdd("recipeCompleted", 1))
                eventCounters["recipeCompleted"]++;

            if (eventCounters.ContainsKey("recipe." + obj.Name))
                eventCounters["recipe." + obj.Name]++;
            else eventCounters["recipe." + obj.Name] = 1;


            UpdateTutorial();
        }

        private void UpdateTutorial()
        {
            foreach (var step in tutorialSteps)
            {
                if (step.completed) continue;
                if (step.canBeSkipped || step == currentStep) UpdateSkipTutorial(step);
            }
        }

        private void OnDestroyed(Stackable obj)
        {
            return;
        }

        private void OnCreated(Stackable obj)
        {
            // deactivate some tutorial,  
            UpdateTutorial();
        }

        private void UpdateSkipTutorial(TutorialStep step)
        {
            if (step.completed) return;
            //use regexp

            string patternAsset = @"^asset\.(\w+)\.(\w+)\s*(>=?|<=?|!=|==)\s*(\d+)$";

            // wait_for_event: asset.type.Plant > 0
            // wait_for_event: asset.attributeName.Plant > 0

            Match match = Regex.Match(step.waitForEvent, patternAsset);
            if (match.Success)
            {
                string assetCompare = match.Groups[1].Value;
                string assetTypeValue = match.Groups[2].Value;

                string operatorString = match.Groups[3].Value;
                int rightValue = int.Parse(match.Groups[4].Value);

                int count = 0;
                if (assetCompare == "type")
                {
                    count = Stackable.all.Count(s =>
                        s.asset.Type.Equals(assetTypeValue, StringComparison.InvariantCultureIgnoreCase));
                }

                else
                {
                    count = Stackable.all.Count(s =>
                        s.asset.GetAttribute(assetCompare)
                            .Equals(assetTypeValue, StringComparison.InvariantCultureIgnoreCase));
                }

                if (operatorString.Compare(count, rightValue))
                {
                    step.completed = true;
                    return;
                }
            }

            string patternEvent = @"^event\.(\w+)\s*(>=?|<=?|!=|==)\s*(\d+)$";


            //   wait_for_event: recipeCompleted.count >= 3
            match = Regex.Match(step.waitForEvent, patternEvent);
            if (match.Success)
            {
                string eventName = match.Groups[1].Value;
                string operatorString = match.Groups[2].Value;
                int rightValue = int.Parse(match.Groups[3].Value);

                if (eventCounters.ContainsKey(eventName))
                {
                    int count = eventCounters[eventName];
                    if (operatorString.Compare(count, rightValue))
                    {
                        step.completed = true;
                    }
                }
            }
        }

    }
}