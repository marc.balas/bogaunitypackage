
using YamlDotNet.Serialization;

namespace BogakitUnity
{
    [System.Serializable]
    public class TutorialStep
    {
        public string Line { get; set; }
        public string Url { get; set; }

        [YamlMember(Alias = "wait_for_event", ApplyNamingConventions = false)]

        public string waitForEvent { get; set; } = "";
        public string animation { get; set; }
        [YamlMember(Alias = "can_be_skipped", ApplyNamingConventions = false)]

        public bool canBeSkipped { get; set; } = true;
        public int delay { get; set; }
       // public bool next { get; set; } = false;
        public string focus { get; set; } = "";
        
        //not in yaml
        
        public bool completed { get; set; } = false;
       

    }
    
    
}