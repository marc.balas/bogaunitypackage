using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BogakitUnity.Extensions;
using BogakitUnity.Managers;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace BogakitUnity.Utils
{
    public class DebugMessageManager: MonoBehaviour
    {
        
        
        [SerializeField]
        protected GameObject messagePanel;

        private float messageLifetime => Mathf.Clamp(10.0f / (messages.Count + 1), .5f, 3);
        private readonly Queue<GameObject> messages = new Queue<GameObject>();

        [SerializeField]
        private GameObject messagePrefab;

        #region LIFECYCLE
        protected void Awake()
        {
            Assert.IsNotNull(messagePrefab);
            Assert.IsNotNull(messagePanel);
            
            
            messagePanel.transform.Clear();
        }
        #endregion
        
        protected virtual void Show(GameObject go, TextMeshProUGUI textUI, string message, float duration = -1)
        {
            textUI.text = message;
            go.SetActive(true);
            if (duration > 0) StartCoroutine(HideCoroutine(go, duration));
        }
        
        private IEnumerator HideCoroutine(GameObject go, float duration = 1)
        {
            yield return new WaitForSeconds(duration);
            go.SetActive(false);
        }
        
        #region Messages

        
        public void QueueMessageAnimation(TextMeshProUGUI textUI, string message, float duration)
        {
            AnimationManager.AppendAnimation(message, duration, () => Show(textUI.gameObject, textUI, message), () => textUI.gameObject.SetActive(false));
        }
        
        [Button("queue message")]
        public void QueueMessage()
        {
            QueueMessage("test message");
        }

        public void QueueMessage(string message)
        {
            GameObject messageGO = Instantiate(messagePrefab, messagePanel.transform, false);
            messageGO.SetActive(true);
            messageGO.GetComponentInChildren<TextMeshProUGUI>().text = message;
            messages.Enqueue(messageGO);
            CancelInvoke();
            InvokeRepeating(nameof(DestroyMessages), 1, messageLifetime);
        }

        protected void QueueMessage(string message, Color color)
        {
            QueueMessage(message);
            GameObject lastMessage = messages.Last();
            lastMessage.GetComponentInChildren<Image>().color = color;
        }

        private void DestroyMessages()
        {
            if (messages.Count > 0)
            {
                GameObject message = messages.Dequeue();

                Destroy(message);
            }
        }

        #endregion
    }
}