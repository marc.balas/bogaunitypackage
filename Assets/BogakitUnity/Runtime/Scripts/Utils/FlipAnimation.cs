﻿// Copyright (C) 2022 Boga. All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement,
// a copy of which is available at http://unity3d.com/company/legal/as_terms.

using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

namespace BogakitUnity.Utils
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class FlipAnimation : MonoBehaviour
    {
        [SerializeField] private Vector3 flipAxis = Vector3.up;
        [SerializeField] private float rotationDuration = .5f;
        [SerializeField] private float translationDuration = 1f;
        [SerializeField] private Vector3 flippingTranslation;

        [SerializeField] private bool Flipped;


        public void Flip(bool visible,bool animated)
        {
            if(Flipped !=visible)
                return;
            Flip(animated);
        }

        public void Flip(bool animated)
        {
            if (animated)
            {
                DoRotationAnimation();
            }
            else
            {
                transform.Rotate(flipAxis, 180, Space.Self);
            }

            Flipped = !Flipped;
        }

        [Button]
        public void Flip()
        {
               Flip(true);//Application.isPlaying
           
        }

        private void DoRotationAnimation()
        {
            Transform transform1;
            (transform1 = transform).DOKill(true);
            Vector3 anchorPosition = transform1.localPosition;
            Quaternion newRot = transform1.localRotation * Quaternion.AngleAxis(180, Vector3.up);
            Sequence mySequence = DOTween.Sequence();
            mySequence.Append(transform.DOLocalMove(anchorPosition + flippingTranslation, translationDuration / 2));
            mySequence.Append(transform.DOLocalRotateQuaternion(newRot, rotationDuration));
            mySequence.Append(transform.DOLocalMove(anchorPosition, translationDuration / 2));
        }
    }
}