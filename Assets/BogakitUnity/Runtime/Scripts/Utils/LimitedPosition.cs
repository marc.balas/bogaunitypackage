using UnityEngine;

namespace BogakitUnity.Utils
{
    public class LimitedPosition : MonoBehaviour
    {

        [SerializeField]
        private Collider2D _collider;
    
        // Start is called before the first frame update
        void Start()
        {
            // Assert.IsNotNull(collider, $"missing collider for limited position : {name}");
        }

        public void SetLimits(Collider2D limits)
        {
            _collider = limits;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            //  
            if (_collider && !_collider.OverlapPoint(transform.position))
            {
                transform.position = _collider.ClosestPoint(transform.position);
            }
        }
    
        public void OnDrawGizmos()
        {
     

            if (!_collider)
            {
                return; // nothing to do without a collider
            }

            Vector3 closestPoint = _collider.ClosestPoint(transform.position);

            Gizmos.DrawSphere(transform.position, .5f);
            Gizmos.DrawWireSphere(closestPoint, .3f);
        }
    }
}