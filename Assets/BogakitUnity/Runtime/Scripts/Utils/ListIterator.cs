using System.Collections.Generic;
using System.Linq;
using Boga.Core;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;

namespace BogakitUnity.Utils
{


    public class ListIterator : MonoBehaviour
    {
        [SerializeField] private List<GameObject> gameObjects = new List<GameObject>();

        GameObject currentObject;

        // Start is called before the first frame update
        void Start()
        {

            Assert.IsTrue(gameObjects.Count > 0, " listIterator has no objects to iterate");
            currentObject = gameObjects.First();
            foreach (var go in gameObjects)
            {
                go.SetActive(false);
            }

            currentObject.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {
        }

        [Button("iterate")]
        public void Iterate()
        {
            if (currentObject == null) currentObject = gameObjects.FirstOrDefault();
            currentObject.SetActive(false);
            currentObject = gameObjects.After(currentObject);
            currentObject.SetActive(true);
        }
    }
}