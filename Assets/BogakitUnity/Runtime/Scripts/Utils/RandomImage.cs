using System.Collections.Generic;
using Boga.Core;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace BogakitUnity.Utils
{
    
    [RequireComponent(typeof(Image))]
    public class RandomImage : MonoBehaviour
    {
        [SerializeField] private List<Sprite> images;
        
        [SerializeField] private Vector3 positionRange;
        [SerializeField] public UnityEngine.UI.Image image;

        private void Start()
        {
            image = GetComponent<UnityEngine.UI.Image>();
            Spawn();
        }

        private void OnValidate()
        {
            
        }

        [Button("spawn")]
        public void Spawn()
        {
            image.sprite = images.RandomSampleNoPseudo();
            GetComponent<RectTransform>().anchoredPosition = new Vector3(Random.Range(-positionRange.x, positionRange.x), Random.Range(-positionRange.y, positionRange.y), 0);
        }
    }
}