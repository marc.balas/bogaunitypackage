using System.Collections.Generic;
using Boga.Core;
using NaughtyAttributes;
using TMPro;
using UnityEngine;

namespace BogakitUnity.Utils
{
    
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class RandomText : MonoBehaviour
    {
        [SerializeField] private List<string> texts;
        private TextMeshProUGUI label;

        private void Start()
        {
            label = GetComponent<TextMeshProUGUI>();
            Spawn();
        }

        private void OnValidate()
        {
            
        }

        [Button("spawn")]
        public void Spawn()
        {
            label.text = texts.RandomSampleNoPseudo();
        }
    }
}