using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BogakitUnity.Utils
{
    public class SpriteSheetAnimation : MonoBehaviour
    {
        [SerializeField] private float duration;
        [SerializeField] private int loops;
        [SerializeField] private UnityEvent onComplete;

    
        [SerializeField] private bool stopOnComplete;
        [SerializeField] private Sprite[] sprites;
        [SerializeField] private Sprite completeSprite;
        [SerializeField] private Image image;
        private SpriteRenderer sr;
        private int index = 0;
        private float timer = 0;
        private bool playing;
        private int nbLoops;

        void Start()
        {
            image = GetComponent<Image>();
            sr = GetComponent<SpriteRenderer>();
            Assert.IsTrue(sprites.Length > 0, "SpriteSheetAnimation needs sprites");
        }

        [Button("Play")]
        public void Play()
        {
            playing = true;
            image.enabled = true;
            Debug.Log($"sprite animation started at {Time.time}");
        }
    
        [Button("PlayOnce")]
        public void PlayOnce()
        {
            playing = true;
            stopOnComplete = true;
            image.enabled = true;
            //   Debug.Log($"sprite animation started at {Time.time}");
        }
    
        [Button("Stop")]
        public void Stop()
        {
            playing = false;
            index = 0;
            //image.enabled = false;
            timer = 0;
            nbLoops = 0;
            stopOnComplete = false;
            image.sprite = completeSprite;
            if (completeSprite == null) image.enabled = false;
//        Debug.Log($"sprite animation stopped at {Time.time}");
        }

        public void PlayAnim(string animationName, float duration)
        {
            this.duration = duration;
            sprites = Resources.LoadAll<Sprite>(animationName);
            playing = true;
        
        }

        private void Update()
        {
            if (!playing)
                return;
            if ((timer += Time.deltaTime) < duration / sprites.Length) return;
            //show next sprite and restart timer
            timer = 0;
            image.sprite = sprites[index];
            index++;
            if (index < sprites.Length) return;
            nbLoops++;
            index = 0;
            if (!stopOnComplete && (loops <= 0 || nbLoops < loops)) return;
            Stop();
            // playing = false;
            // stopOnComplete = false;
            // image.sprite = completeSprite;
            onComplete?.Invoke();
        }
    }
}