using UnityEngine;
using UnityEngine.Events;

namespace BogakitUnity.Utils
{
    public class ToggleAnimation : MonoBehaviour
    {
        [SerializeField]private UnityEvent<bool> onToggle;
    
        [SerializeField]private UnityEvent onShow;
        [SerializeField]private UnityEvent onHide;
        public bool active = true;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void Toggle()
        {
            if (active)
            {
                Hide();
            }
            else
            {
                Show();
            }
            onToggle?.Invoke(active);
        }

        public void Hide()
        {
            active = false;
            onHide?.Invoke();
        }

        public void Show()
        {
            active = true;
            onShow?.Invoke();
        }
    }
}
