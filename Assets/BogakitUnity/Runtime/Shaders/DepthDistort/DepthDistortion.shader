﻿Shader "BogaKit/DepthDistortion"
{
    Properties
    {
		_MainTex		("Main Texture", 2D) = "white" {}
		_DepthTex		("Depth Texture", 2D) = "black" {}
		_DistortionTex	("Distortion Texture", 2D) = "black" {}
		_DepthFactorX	("Depth Factor X", Range(-0.07,0.07)) = 0
    	_DepthFactorY	("Depth Factor Y", Range(-0.07,0.07)) = 0
	}
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			sampler2D _DepthTex;
            float4 _DepthTex_ST;
			
			sampler2D _DistortionTex;
			float4 _DistortionTex_ST;

			float _DepthFactorX;
			float _DepthFactorY;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed distortion = tex2D(_DistortionTex, i.uv).a;
				float2 uv = i.uv;
				uv.x += distortion * sin(_Time.y * 2 + i.uv.y * 30) * 0.003;
				uv.y += distortion * sin(_Time.y * 2 + i.uv.x * 60) * 0.006;

				fixed depth = (1 - tex2D(_DepthTex, uv).a);
				
				fixed4 col = tex2D(_MainTex, float2(uv.x + depth * _DepthFactorX, uv.y + depth * _DepthFactorY));
				return col;
            }
            ENDCG
        }
    }
}
