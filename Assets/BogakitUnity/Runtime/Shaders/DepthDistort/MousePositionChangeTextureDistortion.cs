﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BogakitUnity.Shaders.DepthDistort
{
    public class MousePositionChangeTextureDistortion : MonoBehaviour
    {
        private static readonly int Shader_Depth_X          = Shader.PropertyToID("_DepthFactorX");
        private static readonly int Shader_Depth_Y          = Shader.PropertyToID("_DepthFactorY");

        private const float MIN_DEPTH = -0.05f;
        private const float MAX_DEPTH = 0.05f;

        [FormerlySerializedAs("m_NaturalAmplitude")] [SerializeField] private float      mNaturalAmplitude = 0.2f;
        [FormerlySerializedAs("m_NaturalPeriod")] [SerializeField] private float      mNaturalPeriod = 5;
        [FormerlySerializedAs("m_DampingFactor")] [SerializeField] private float      mDampingFactor = 5;

        private float mCurrentX;
        private float mCurrentY;

        // Update is called once per frame
        void Update()
        {
            float x = Input.mousePosition.x / Screen.width;// + Mathf.Cos(Time.time * Mathf.PI * 2 / m_NaturalPeriod) * m_NaturalAmplitude;
            float y = Input.mousePosition.y / Screen.height;// + Mathf.Sin(Time.time * Mathf.PI * 2 / m_NaturalPeriod) * m_NaturalAmplitude;

            mCurrentX = Mathf.Lerp(mCurrentX, 1 - x, Time.deltaTime * mDampingFactor);
            mCurrentY = Mathf.Lerp(mCurrentY, y, Time.deltaTime * mDampingFactor);

            Material m = GetComponent<Image>().material;
            m.SetFloat(Shader_Depth_X, Mathf.Lerp(MIN_DEPTH, MAX_DEPTH, mCurrentX));
            m.SetFloat(Shader_Depth_Y, Mathf.Lerp(MIN_DEPTH, MAX_DEPTH, mCurrentY));
        }

        private Dictionary<string, GameObject> loadedResources;

        private void LoadAll()
        {
            loadedResources = new Dictionary<string, GameObject>();
            loadedResources.Add("Path", Resources.Load<GameObject>("path"));
        }
    }
}
