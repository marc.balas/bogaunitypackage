﻿using System.Collections;
using UnityEngine;

namespace BogakitUnity.Shaders.SceneTransitions
{
    [ExecuteInEditMode]
    public class CutoffTransition : MonoBehaviour
    {
        public Material TransitionMaterial;

        public bool fadeIn = false;
        public float duration = 1;

        public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);
        private static readonly int Cutoff = Shader.PropertyToID("_Cutoff");

        void OnRenderImage(RenderTexture src, RenderTexture dst)
        {
            if (TransitionMaterial != null)
                Graphics.Blit(src, dst, TransitionMaterial);
        }



        [ContextMenu("Animate cutoff")]
        public void AnimateCutOff()
        {
            StopAllCoroutines();
            fadeIn = !fadeIn;
            StartCoroutine(CutOffCoroutine());
        }


        [ContextMenu("Animate Show")]
        public void AnimateShow()
        {
            TransitionMaterial.SetFloat(Cutoff, 1);
            fadeIn = true;
            AnimateCutOff();
        }

        [ContextMenu("Animate Hide")]
        public void AnimateHide()
        {
            TransitionMaterial.SetFloat(Cutoff, 0);
            fadeIn = false;
            AnimateCutOff();
        }


        private IEnumerator CutOffCoroutine()
        {
        //    var startTime = Time.time;
           var previousCutoff= TransitionMaterial.GetFloat(Cutoff);
            // var previousColor= spriteGlow.GlowColor;

            // spriteGlow.GlowColor = glowColor;

            float journey = 0f;
            while (journey <= duration)
            {
                journey = journey + Time.deltaTime;
                float percent = Mathf.Clamp01(journey / duration);
                float curvePercent = curve.Evaluate(percent);
                TransitionMaterial.SetFloat(Cutoff, Mathf.LerpUnclamped(previousCutoff, fadeIn? 1 : 0, curvePercent));
                //print("glow anim " + spriteGlow.GlowBrightness+ " % "+percent);

                yield return new WaitForEndOfFrame();
            }

      

        }

    }
}
