using BogakitUnity;
using BogakitUnity.Layouts;
using DG.Tweening;
using UnityEngine;

public class DemoTargetingManager : TargetingManager
{
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override bool IsValidTargeting(GameObject source, GameObject target)
    {
        
        if(target == null) return false;
        if (target == source) return false;
        if (target.transform.parent == source.transform.parent) return false;
        return (target.CompareTag("Trash") || target.CompareTag("Card"));
        
    }

    public override bool CanStartTargetingWith(GameObject source)
    {
        return true;
    }

    public override void BeginTargeting(GameObject source)
    {
        Debug.Log("begin targeting");
    }

    public override void EndTargeting(GameObject source, GameObject target)
    {
      
        Debug.Log($"Manager : end targeting {source} on {target} ");
        if(!IsValidTargeting(source, target))return;
        
        
        if (target.CompareTag("Trash")) {
            
            // 
        
            Layout layout = source.GetComponentInParent<Layout>();
            //remove card from hand  and destroy 
            source.transform.SetParent(null);
            source.GetComponent<Collider2D>().enabled = false;
            source.transform.DOMove( target.transform.position, 0.5f).OnComplete( () =>
            {
                
                Destroy(source);
                layout?.Organize();
                
            });
            
        }
        else if (target.CompareTag("Card"))
        {
            
            Layout layout = target.GetComponentInParent<Layout>();
            target.transform.SetParent(null);
            Destroy(target.GetComponent<PunchHover>());
            target.transform.DOScale( Vector3.zero, 1.2f).OnComplete( () =>
            {
                
                DestroyImmediate(target);
                layout?.Organize();
            });
        }

        
      
    }
}
