using BogakitUnity.Extensions;
using BogakitUnity.Layouts;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Assertions;
using Selection = BogakitUnity.Selections.Selection;

namespace BogakitUnity.Samples
{
    public class HandDragDrop : DragDropManager
    {
        [Required] [SerializeField] private Layout dropLayout;
        [Required] [SerializeField] private Layout dropLayoutTop;
        [Required] [SerializeField] private LayoutPlaceholder placeholder;
        [Required] [SerializeField] private LayoutPlaceholder placeholder2;
        [Required] [SerializeField] private CardHandController handController;
        [Required] [SerializeField] private GameObject cardPrefab;

        [Required] [SerializeField] private Transform drawAnchor;
        [Required] [SerializeField] private Transform spawnAnchor;

        [Required] [SerializeField] private TargetingArrow targetingArrow;
        [SerializeField] private bool isDragging;

        private void OnEnable()
        {
            Assert.IsNotNull(dropLayout);
            Selection.OnFocusChanged += OnFocusChanged;
        }

        private void OnDisable()
        {
            Selection.OnFocusChanged -= OnFocusChanged;
        }

        private void OnFocusChanged(GameObject current, GameObject previous)
        {
            //      Debug.Log($"OnFocusChanged: to {current} previous: {previous}");


            // selection but have no active targeting action 
            // or dragged card
            if (handController.IsDraggingCard)
            {
                placeholder2.UpdateTarget();

                placeholder.UpdateTarget();
            }


            // if (current == null)
            // {
            //     placeholder.Stop();
            //     placeholder2.Stop();
            //     return;
            // }
            //
            // if (EventSystem.current.currentSelectedGameObject != null)
            // {
            //     placeholder2.StartFollowing(EventSystem.current.currentSelectedGameObject);
            //     placeholder.StartFollowing(EventSystem.current.currentSelectedGameObject);
            //
            // }
            // if (previous == dropLayout.gameObject)
            // {
            //     placeholder.Stop();
            // }
            //
            // if (previous == dropLayoutTop.gameObject)
            // {
            //     placeholder2.Stop();
            // }
        }

        public void DrawCard()
        {
            var card = Instantiate(cardPrefab);
            card.transform.position = spawnAnchor.position;
            card.UpdateSortingOrder(999, "Zone");
            Sequence s = DOTween.Sequence();
            s.Append(card.transform.DOMove(drawAnchor.transform.position, 0.5f));
            s.AppendInterval(1);
            s.OnComplete(() => { handController.AddCard(card); });
        }

        public override void BeginDrag(GameObject arg2)
        {
            isDragging = true;
        }

        // private void UpdatePlaceHolders()
        // {
        //     if (EventSystem.current.currentSelectedGameObject == null) return;
        //     if (Selection.CurrentFocus == null) return;
        //
        //     if (Selection.CurrentFocus == placeholder.Layout.gameObject)
        //     {
        //         placeholder.StartFollowing(EventSystem.current.currentSelectedGameObject);
        //     }
        //
        //     if (Selection.CurrentFocus == placeholder2.Layout.gameObject)
        //     {
        //         placeholder2.StartFollowing(EventSystem.current.currentSelectedGameObject);
        //     }
        // }

        public override void DidDropOn(GameObject source, GameObject go)
        {
            
            Debug.Log( $"DidDropOn:  did drop {source} on {go} :");
            Layout dropZone = go.GetComponent<Layout>();
            if (dropZone)
            {
                int index = dropZone.ClosestIndexFromPosition(source
                    .transform.position);
                Vector3 position = dropZone.ClosestPosition(source.transform.position);
                dropZone.AddAtIndex(source, index);

                // AFTER we get the index
                placeholder2.Stop();
                placeholder.Stop();
                
                source.transform.DOMove(position + 2 * Vector3.back, 1f).OnComplete( ()=> source.transform.DOMove(position, .5f));
                
            }
          //  Selection.Deselect();
           // EventSystem.current.SetSelectedGameObject(null);
            
        }

        public override void EndDrag(DragDropAction obj)
        {
            isDragging = false;
          //  Selection.Deselect();
        }

        public override bool CanDropOn(GameObject source, GameObject target)
        {
            return target && target.GetComponent<Layout>() &&
                   target.GetComponent<Layout>();
        }

        public override bool CanDrag(GameObject element)
        {
            if (element.GetComponentInChildren<TargetingAction>()) return false;
            return true;
        }

        private void Update()
        {
            // UpdatePlaceHolders();
        }
    }
}